<?php

$loader = include __DIR__.'/vendor/autoload.php';
$loader->addPsr4(false, __DIR__.'/models');
$loader->addPsr4(false, __DIR__.'/services');

//Initialing Service Container
$container = new League\Container\Container;

//Include helper Function
include BASE_DIR.'/services/helpers.php';

define('AJAX', Request::isAjax());

//Setting Error Logger
$log = new Monolog\Logger('LOGS');
$log_path = LOGS_DIR.'/'.date('Y-m-d').'.log';
$log->pushHandler(new Monolog\Handler\StreamHandler($log_path, Monolog\Logger::DEBUG));

//Setting Error Logger
$whoops = new \Whoops\Run;

if(DEBUG && !AJAX) {
	$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
}elseif(DEBUG){
	$whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler);
}

$whoops->pushHandler(function($exception, $exceptionInspector)use($log){

	$logInfo = [];

	$frames = $exceptionInspector->getFrames();
	foreach ($frames as $frame) {
		$filePath = $frame->getFile();
		if (strpos($filePath, '/vendor/') !== false) {
		    continue;
		}
		$logInfo = [
			'file' => $filePath,
			'line' => $frame->getLine()
		];
		break;
	}

	$logInfo['session'] = Session::get('logs');
	Session::set('logs', null);

	$log->error($exception->getMessage(), $logInfo);

	$frames->filter(function($frame) {
		$filePath = $frame->getFile();
		return !strpos($filePath, '/vendor/');
	});

	if(DEBUG){
		return \Whoops\Handler\Handler::DONE;
	}

	return Response::error(500);

});

$whoops->register();

//Configuring Database
$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
	'driver'    => 'mysql',
	'host'      => DB_HOST,
	'database'  => DB_NAME,
	'username'  => DB_USERNAME,
	'password'  => DB_PASSWORD,
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => DB_PREFIX,
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$schema =  $capsule->schema();
