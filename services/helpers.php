<?php

function Load($module){
    global $container;
    $key = strtolower($module);
    $key = str_replace('\\', '.', $key);

    if(!$container->has($key)){
        $container->add($key, new $module);
    }
    return $container->get($key);
};
function LogInfo($message){
  global $log;
  $log->info($message);
};
function d($var, $option='json'){
  debug($var, $option);
}
function debug($var, $option='json'){
    echo "<pre>";
    $die = true;
    if($option=='php|continue'){
        $die = false;
        $response = var_export($var);
    }elseif($option=='json|continue'){
        $die = false;
        $response = json_encode($var);
    }elseif($option=='continue'){
        $die = false;
        $response = json_encode($var);
    }elseif($option=='php'){
        $response = var_export($var);
    }else{
        $response = json_encode($var);
    }
    RESPONSE:
    echo $response; if($die) die;

};
