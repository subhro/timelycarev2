<?php

namespace Roster;

class Main {

    private $options = [];
    private $population = false;

    public function init($population=false, $options=[])
    {
        $this->options = $options;
        $this->population = $population;
    }

    public function generate()
    {
        $count = $this->options['size'] ?? 100;
        $module = Load('Roster\Population');
        for ($i=0; $i < $count; $i++) {
          $this->population = $module->get($this->population, $this->options);
        }
        return $this->population;
    }


}
