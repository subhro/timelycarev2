<?php

namespace Roster;

class Fitness {

  public function calculate($chromosome, $options=[]){

      $cost = 0;

      $rows = $options['days'];
      $columns = $options['staffs'];
      $shifts = $options['shifts'];

      $staffPerMonth = $options['staffPerMonth'];
      $staffPerShift = $options['staffPerShift'];
      $consecutiveDay = $options['consecutiveDay'];

      $rowsCost = array_fill(0, $rows, 0);
      $columnCost = array_fill(0, $columns, 0);
      $rowsShiftCost = array_fill(0, $rows, 0);

      $columnWeekCost = array_fill(0, $columns, 0);

      for ($i=0; $i < $rows; $i++) {

        $rowsShiftCost[$i] = array_fill(0, $shifts+1, 0);

        for ($j=0; $j < $columns; $j++) {
          $rowsCost[$i] += $chromosome[$i][$j]?1:0;
          $columnCost[$j] += $chromosome[$i][$j]?1:0;


          if(!$columnWeekCost[$j]){
            $columnWeekCost[$j] =  array_fill(0, intval($rows/7)+1, 0);
          }

          $columnWeekCost[$j][intval($i/7)] += $chromosome[$i][$j]?1:0;
          $rowsShiftCost[$i][$chromosome[$i][$j]] += $chromosome[$i][$j]?1:0;
        }
      }

      foreach ($rowsCost as $value) {
        $cost += 3*abs($value - $staffPerMonth);
      }

      foreach ($columnWeekCost as $weekCost) {
        foreach ($weekCost as $load) {
          if($load > 4){
            $cost +=1;
          }
        }
      }


      foreach ($columnCost as $value) {
        //  $cost += abs($value - 2);
      }

      foreach ($rowsShiftCost as $value) {
        for ($i=0; $i < $shifts+1; $i++) {
          $cost += abs($value[$i] - $staffPerShift[$i]);
        }
      }


      return 1/(1+$cost);
  }

}
