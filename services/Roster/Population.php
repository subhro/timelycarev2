<?php

namespace Roster;

class Population {


  public function get($population = false, $options=[]){
    
    if(!$population){
      return $this->init($options);
    }else{
      return $this->next($population, $options);
    }

  }

  public function init($options=[]){
    $population = [];
    $size = $options['size'];

    for ($i=0; $i < $size; $i++) {
      $chromosome = $this->chromosome($options);
      $fitness = $this->fitness($chromosome, $options);
      $population[] = [
        'fitness' => $fitness,
        'chromosome' => $chromosome
      ];
    }
    return $this->sort($population);
  }


  function next($prevPopulation, $options=[]){


    $nextPoulation = [];
    $populationCout = count($prevPopulation);
    $populationCoutHalf = (Int)$populationCout/2;

    for ($i=0; $i < $populationCoutHalf; $i++) {

      $crossover = $this->crossover(
        $prevPopulation[$i]['chromosome'],
        $prevPopulation[$i+1]['chromosome'],
        $options
      );

      foreach ($crossover as $chromosome) {
        $chromosome = $this->mutate($chromosome, $options);
        $fitness = $this->fitness($chromosome, $options);
        $nextPoulation[] = [
          'fitness' => $fitness,
          'chromosome' => $chromosome
        ];
      }
    }

    return $this->sort($nextPoulation);
  }


  public function sort($population){
    $fitness = [];
    foreach ($population as $p) {
      $fitness[] = $p['fitness'];
    }
    array_multisort($fitness, SORT_DESC, $population);
    return $population;
  }





  public function chromosome($options=[]){
    $module = Load('Roster\GA');
    return $module->chromosome($options);
  }

  public function mutate($chromosome, $options=[]){
    $module = Load('Roster\GA');
    return $module->mutate($chromosome, $options);
  }

  public function fitness($chromosome, $options=[]){
    $module = Load('Roster\GA');
    return $module->fitness($chromosome, $options);
  }

  public function crossover($chromosome1, $chromosome2, $options=[]){
    $module = Load('Roster\GA');
    return $module->crossover($chromosome1, $chromosome2, $options);
  }




}
