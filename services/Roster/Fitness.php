<?php

namespace Roster;

class Fitness {

  public function calculate($chromosome, $options=[]){

      $cost = 0;

      $rows = $options['days'];
      $columns = $options['staffs'];
      $shifts = $options['shifts'];

      $staffPerMonth = $options['staffPerMonth'];
      $staffPerShift = $options['staffPerShift'];
      $consecutiveDay = $options['consecutiveDay'];

      $rowsCost = array_fill(0, $rows, 0);
      $columnCost = array_fill(0, $columns, 0);
      $rowsShiftCost = array_fill(0, $rows, 0);
      $consecutiveDayCost = array_fill(0, $columns, 0);

      $consecutiveDayCount = 0;

      for ($i=0; $i < $rows; $i++) {

        $rowsShiftCost[$i] = array_fill(0, $shifts+1, 0);

        for ($j=0; $j < $columns; $j++) {
          $rowsCost[$i] += $chromosome[$i][$j]?1:0;
          $columnCost[$j] += $chromosome[$i][$j]?1:0;

          if($chromosome[$i][$j] == 0){
            $consecutiveDayCount = 0;
          }else{
            $consecutiveDayCount++;
          }

          if($consecutiveDayCount == $consecutiveDay){
              $consecutiveDayCost[$j] = 0;
          }else{
            $consecutiveDayCost[$j] += abs($consecutiveDayCount - $consecutiveDay);
          }

          $rowsShiftCost[$i][$chromosome[$i][$j]] += $chromosome[$i][$j]?1:0;
        }

      }


      //d($consecutiveDayCost);
      //staff per day
      foreach ($consecutiveDayCost as $value) {
        $cost += $value;
      }


      foreach ($columnCost as $value) {
        if($staffPerMonth > $value){
          $cost += 2*abs($value - $staffPerMonth);
        }else{
          $cost += abs($value - $staffPerMonth);
        }
      }

      foreach ($rowsShiftCost as $value) {
        for ($i=0; $i < $shifts+1; $i++) {
          $cost += abs($value[$i] - $staffPerShift[$i]);
        }
      }


      return 1/(1+$cost);
  }

}
