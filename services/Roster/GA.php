<?php

namespace Roster;

class GA {

  public function chromosome($options=[]){
    $rows = $options['days'];
    $columns = $options['staffs'];
    $values = $options['shifts'];

    $chromosome = [];
    for ($i=0; $i < $rows; $i++) {
      for ($j=0; $j < $columns; $j++) {
        $chromosome[$i][$j] = rand(0, $values);
      }
    }
    return $chromosome;
  }

  public function fitness($chromosome, $options=[]){
    $module = Load('Roster\Fitness');
    return $module->calculate($chromosome, $options);
  }

  public function crossover($chromosome1, $chromosome2, $options=[]){

    $newChromosome1 = [];
    $newChromosome2 = [];

    $rows = $options['days'];
    $columns = $options['staffs'];
    $shifts = $options['shifts'];

    $crossover = $options['crossover'];

    if($crossover=='x'){
      $cutRows = $rows;
      $cutColumns = rand(1, $columns-2);
    }elseif($crossover=='y'){
      $cutRows = rand(1, $rows-2);
      $cutColumns = $columns;
    }else{
      $cutRows = rand(1, $rows-2);
      $cutColumns = rand(1, $columns-2);
    }

    for ($i=0; $i < $rows; $i++) {
      for ($j=0; $j < $columns; $j++) {
        if($i<$cutRows && $j<$cutColumns){
          $newChromosome1[$i][$j] = $chromosome1[$i][$j];
          $newChromosome2[$i][$j] = $chromosome2[$i][$j];
        }else{
          $newChromosome1[$i][$j] = $chromosome2[$i][$j];
          $newChromosome2[$i][$j] = $chromosome1[$i][$j];
        }
      }
    }
    return [$newChromosome1, $newChromosome2];
  }



  public function mutate($chromosome, $options=[]){
    $newChromosome = [];
    $rows = $options['days'];
    $columns = $options['staffs'];
    $shifts = $options['shifts'];

    $count = $rows*$columns;
    $count1 = (int)($count*0.10);

    for ($i=0; $i < $rows; $i++) {
      for ($j=0; $j < $columns; $j++) {
        if(rand($count1,$count) == $count){
          $newChromosome[$i][$j] = rand(0, $shifts);
        }else{
          $newChromosome[$i][$j] = $chromosome[$i][$j];
        }
      }
    }
    return $newChromosome;
  }


}
