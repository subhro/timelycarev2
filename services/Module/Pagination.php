<?php

namespace Module;

class Pagination {


  public function get($page, $limit, $count)
  {
    $page_left = $page-1;
    $page_right = $page+1;

    if($page == 1):
      $page_left = 0;
    endif;

    if($count < $limit):
      $page_right = 0;
    endif;

    $pagination = (object)[
      'page' => $page,
      'left' => $page_left,
      'right' => $page_right,
    ];

    return $pagination;
  }


}
