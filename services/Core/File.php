<?php

namespace Core;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;


class File {

  private $filesystem;


  public function read($path){
    $filesystem = $this->getFileSystem();
    return $filesystem->read($path);
  }

  public function has($path){
    $filesystem = $this->getFileSystem();
    return $filesystem->has($path);
  }

  public function delete($path){
    $filesystem = $this->getFileSystem();
    return $filesystem->delete($path);
  }

  public function rename($from, $to){
    $filesystem = $this->getFileSystem();
    return $filesystem->rename($from, $to);
  }


  public function copy($from, $to){
    $filesystem = $this->getFileSystem();
    return $filesystem->copy($from, $to);
  }


  public function write($path, $contents){
    $filesystem = $this->getFileSystem();
    return $filesystem->put($path, $contents);
  }

  public function mime($path){
    $filesystem = $this->getFileSystem();
    return $filesystem->getMimetype($path);
  }

  public function time($path){
    $filesystem = $this->getFileSystem();
    return $filesystem->getTimestamp($path);
  }

  public function size($path){
    $filesystem = $this->getFileSystem();
    return $filesystem->getSize($path);
  }

  public function store($temp, $to){
    return move_uploaded_file($temp, STORAGE_DIR.$to);
  }


  private function getFileSystem(){

    if(!$this->filesystem){
      $adapter = new Local(STORAGE_DIR);
      $this->filesystem = new Filesystem($adapter);
    }
    return  $this->filesystem;

  }




  // public function write($path, $contents){
  //   $filesystem = $this->getFileSystem();
  //   return $filesystem->write($path, $contents);
  // }
  //
  // public function update($path, $contents){
  //   $filesystem = $this->getFileSystem();
  //   return $filesystem->update($path, $contents);
  // }




}
