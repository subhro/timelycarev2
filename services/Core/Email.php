<?php

namespace Core;

use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_SendmailTransport;



class Email {

    public $mailer;

    public function send($to, $subject, $message){

        if(DEBUG){
          return LogInfo($message);
        }

        $mailer = $this->getInstance();
        $message = $this->setMessage($to, $subject, $message);
        $mailer->send($message);

    }

    public function message($template, $data){
        $view = Load('Core\View');
        return $view->get('emails.'.$template, $data);
    }

    private function setMessage($to, $subject, $message){

        $message = (new Swift_Message($subject))
        ->setFrom(['info@timelycare.org' => 'Timely Care'])
        ->setTo([$to])
        ->setBody($message, 'text/html');

        return $message;
    }


    private function getInstance(){

        if($this->mailer)
            return $this->mailer;

        $transport = new Swift_SendmailTransport(EMAIL_TRANSPORT);
        // $transport = Swift_SmtpTransport::newInstance('smtp.mailgun.org');
        $this->mailer = new Swift_Mailer($transport);

        return $this->mailer;
    }



}
