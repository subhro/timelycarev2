<?php

namespace Core;

class Session {

  public function __construct()
  {
    session_start();
    $_SESSION['_start_time'] = time();
  }

  // public function __destruct()
  // {
  //   unset($this);
  // }

  public function get($key)
  {

    if($this->isExpired()){
      $this->destroy();
      return false;
    }

    if(!isset($_SESSION[$key])){
      return false;
    }

    $this->renew();

    return $_SESSION[$key];
  }




  public function has($key){

    if($this->isExpired()){
      $this->destroy();
      return false;
    }

    $this->renew();

    return isset($_SESSION[$key]) ? true : false;
  }


  public function set($key, $value)
  {
    $this->renew();
    $_SESSION[$key] = $value;
  }


  public function isExpired()
  {
    $startTime = $_SESSION['_start_time'];
    $startTime += 60;

    if($startTime < time()) {
      return true;
    } else {
      return false;
    }

  }

  public function renew()
  {
    $_SESSION['_start_time'] = time();
  }

  public function destroy()
  {
    $_SESSION = [];
    $_SESSION['_start_time'] = time();
  }



}
