<?php

namespace Core;

use Windwalker\Edge\Edge;
use Windwalker\Edge\Cache\EdgeFileCache;
use Windwalker\Edge\Loader\EdgeFileLoader;
use Windwalker\Edge\Compiler\EdgeCompiler;


class View {

    public $view;

    public function get($page, $data=[]){
        return $this->render($page, $data);
    }

    public function render($page, $data=[]){

      $viewFiles = [
          SECTION.'/pages',
          SECTION.'/partials',
          SECTION.'/widgets'
      ];

      $pagePath = str_replace('.', '/', $page);
      $isExists = false;
      foreach($viewFiles as $folder) {
        if(file_exists($folder.'/'.$pagePath.'.php')){
          $isExists = true;
        }
      }

      if(!$isExists) {
        $page = '404';
        $viewFiles = [
            APP_DIR.'/errors'
        ];
      }

      $view = $this->getInstance($viewFiles);
      return $view->render($page, $data);

    }


    public function getInstance($viewFiles){

        if($this->view)
            return $this->view;

        $fileLoader = new EdgeFileLoader($viewFiles);
        $fileLoader->addFileExtension('.php');

        $compiler = new EdgeCompiler;
        $compiler->setContentTags('<%', '%>');
        $compiler->setEscapedContentTags('<%%', '%%>');

        $cacheLoader = new EdgeFileCache(STORAGE_DIR.'/view');
        $this->view = new Edge($fileLoader, $compiler, $cacheLoader);

        return $this->view;

    }



}
