<?php

namespace Core;

use Request;


class Response {

  public function html($q){
    $view = Load('Core\View');
    header('Content-Type: text/html');
    echo $view->render($q);
  }

  public function json($file){
    $data = include SECTION.'/'.$file.'.php';
    $code = isset($data['code']) ? $data['code'] : 200;
    $this->setHeaders();
    http_response_code($code);
    echo json_encode($data);
  }

  public function filter(){

    $filter = [];
    $filterFile = SECTION.'/filter.php';
    if(file_exists($filterFile)){
      $filter = include $filterFile;
    }

    if(!isset($filter['denied'])){
      $filter['denied'] = false;
    }
    if(!isset($filter['redirect'])){
      $filter['redirect'] = '/';
    }
    if(Request::is($filter['redirect'])){
      $filter['denied'] = false;
    }
    $filter = (object)$filter;
    return $filter;
  }

  public function redirect($url){
    if(Request::isAjax()){
      echo json_encode([
        'data' => false,
        'events' => ['page.redirect' => $url]
      ]); die;
    }
    header('Location: '.$url); die;
  }

  public function options(){
    $this->setHeaders();
    http_response_code(200);
    echo '{}'; die;
  }

  private function setHeaders(){
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: Content-Type,Auth-Id,Auth-Token');
    header('Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT');
    header('Content-Type: application/json');
  }


  public function error($code){
    if($code == 404){
      return $this->notFoundError();
    }else{
      return $this->unknownServerError();
    }
  }

  private function notFoundError(){
    http_response_code(400);
    if(Request::isAjax()){
      echo json_encode([
        'code' => 400,
        'type' => 'error',
        'message' => 'Not Found, Please report us'
      ]); die;
    }
    include BASE_DIR.'/apps/errors/404.php'; die;
  }

  private function unknownServerError(){
    http_response_code(400);
    if(Request::isAjax()){
      echo json_encode([
        'code' => 400,
        'type' => 'error',
        'message' => 'Server Error, Please report us'
      ]); die;
    }
    include BASE_DIR.'/apps/errors/500.php'; die;
  }




}
