<?php

$method = Request::method();

if($method == 'OPTIONS'){
	Response::options();
}

if(!isset($_SERVER['REDIRECT_URL'])){
	$uri = '/';
}else{
	$uri = $_SERVER['REDIRECT_URL'];
}

$uri = str_replace(SITE_URL, '/', $uri);

$uri = ltrim($uri, '/');
$uri = rtrim($uri, '/');

$params = explode('/', $uri);
if(!isset($params[0]) || !$params[0]){
	$params = ['index'];
}


$module = '';
switch($params[0]){

	case '-api':
		$section = 'mobile';
	break;
	case '-dev':
		$section = '-tools';
	break;
	case '-test':
		$section = '-test';
	break;


	case 'cron':
		$section = 'crons';
	break;

	case 'admin':
		$module = '/pages';
		$section = 'webmin';
	break;

	default:
		$module = '/pages';
		$section = 'website';
	break;

}

if($section != 'website'){
	array_shift($params);
}
if(!isset($params[0]) || !$params[0]){
	$params = ['index'];
}

$page = 'index';

switch($params[0]){
	case 'api':
		array_shift($params);
		$module = '/apis';
	break;
}

$id = 0;

if(count($params)){
	$path = '';
}else{
	$path = '/index';
}


foreach ($params as $i => $param) {
	$path .= '/'.$param;
	$file = BASE_DIR.'/apps/'.$section.$module.$path;
	if(file_exists($file.'.php')){
		$page = $param;
		break;
	}
}

$file = BASE_DIR.'/apps/'.$section.$module.$path;

if(!file_exists($file.'.php')){
	$page = '404';
	$module = '/pages';
	$section = 'errors';
}

//
// echo '$page:  '.$page;
// echo '<br>';
// echo '$module:  '.$module;
// echo '<br>';
// echo '$section:  '.$section;
//
// die;

define('PAGE', $page);
define('SECTION', BASE_DIR.'/apps/'.$section);

$filter = Response::filter();

if($filter->denied){
	Response::redirect($filter->redirect);
}


if($module == '/pages'){
	Response::html($path);
}elseif($module == '/apis'){
	Response::json($module.$path);
}elseif($section == 'mobile'){
	if(!Input::has('uid')){
		Input::set('uid', Request::header('Auth-Id'));
		Input::set('token', Request::header('Auth-Token'));
	}
	Response::json($module.$path);
}else{
	include SECTION.$module.$path.'.php';
}





//
