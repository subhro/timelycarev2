<?php

use Illuminate\Database\Eloquent\Model;


class Schedule extends Model{

  public $table = 'schedules';
  public $timestamps = false;


  protected static function boot(){
    parent::boot();
    static::addGlobalScope(new WithoutTrash);
  }

}
