<?php

use Illuminate\Database\Eloquent\Model;


class Analytics extends Model{

  public $table = 'analytics';
  public $timestamps = false;

}
