<?php

use Illuminate\Database\Eloquent\Model;


class Leave extends Model{

  public $table = 'leave_request';
  public $timestamps = false;

}
