<?php

use Illuminate\Database\Eloquent\Model;


class Favorite extends Model{

  public $table = 'favorites';
  public $timestamps = false;

}
