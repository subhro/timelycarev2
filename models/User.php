<?php

use Illuminate\Database\Eloquent\Model;


class User extends Model{

  public $table = 'users';
  public $timestamps = false;

  protected $appends = [
    'first_name',
    'last_name',
    'favorite',
    'message',
    'availability'
  ];
  protected $hidden = [
    'password'
  ];

  public function getFirstNameAttribute()
  {
    $names = explode(' ', $this->attributes['name']);
    return $names[0] ?? '';
  }

  public function getLastNameAttribute()
  {
    $names = explode(' ', $this->attributes['name']);
    return $names[1] ?? '';
  }

  public function description()
  {
    return $this->belongsTo('Description');
  }

  public function getDescriptionTextAttribute()
  {
    return $this->description->name ?? 'Unknown';
  }

  public function getShiftIdsAttribute($value)
  {
    return json_decode($value);
  }

  public function setShiftIdsAttribute($value)
  {
    $this->attributes['shift_ids'] = json_encode($value);
  }

  public function getImageAttribute($value)
  {
    return $value;
  }

  public function getFavoriteAttribute()
  {
    $id = Input::get('uid');
    $favId = $this->attributes['id'];
    if($id == $favId){
      return 0;
    }
    $favorite = Favorite::where('user_id', $id)->where('favorite_id', $favId)->first();
    if(!$favorite){
      return 0;
    }
    return 1 - $favorite->trash;
  }

  public function getMessageAttribute()
  {
    $uid = Input::get('uid');
    $sender_id = $this->attributes['id'];
    $sender = User::find($sender_id);
    if($sender->type != 2){
      $chats = Chat::where('sender_id', $sender_id)
      ->where('receiver_id', $uid)
      ->where('status', 0)
      ->where('type', '!=', 2)
      ->count();
      return $chats;
    }
  }

  public function getAvailabilityAttribute()
  {
    $id = $this->attributes['id'];
    $availability = Availability::where('user_id', $id)->first();
    if(!$availability){
      $availability = new Availability;
      $availability->user_id  = $id;
      $availability->voice_call = 1;
      $availability->text_message = 1;
      $availability->custom_message = '';
      $availability->available_again = date('Y-m-d H:i:s');
      $availability->save();
    }

    return $availability;
  }

}
