<?php
// user_shift	text
// swap_shift

use Illuminate\Database\Eloquent\Model;


class ShiftRequest extends Model{

  public $table = 'shifts_requests';
  public $timestamps = false;


  public function getUserShiftAttribute($value)
  {
      return json_decode($value);
  }

  public function setUserShiftAttribute($value)
  {
      $this->attributes['user_shift'] = json_encode($value);
  }



  public function getSwapShiftAttribute($value)
  {
      return json_decode($value);
  }

  public function setSwapShiftAttribute($value)
  {
      $this->attributes['swap_shift'] = json_encode($value);
  }


}
