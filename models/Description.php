<?php

use Illuminate\Database\Eloquent\Model;


class Description extends Model{

  public $table = 'users_descriptions';
  public $timestamps = false;

}
