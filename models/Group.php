<?php

use Illuminate\Database\Eloquent\Model;


class Group extends Model{

  public $table = 'groups';
  public $timestamps = false;

  protected $appends = ['image', 'favorite'];

  public function getMemberIdsAttribute($value)
  {
      return json_decode($value);
  }

  public function setMemberIdsAttribute($value)
  {
      $value = array_map('strval', $value);
      $this->attributes['member_ids'] = json_encode($value);
  }

  public function getImageAttribute()
  {
    $id = $this->attributes['id'];
    $path = STORAGE_DIR . '/groups';
    $image = $path.'/'.$id.'.jpg';
    if(file_exists($image)){
      return FULL_URL . '/storage/groups/'.$id.'.jpg';
    }
    return 'assets/imgs/avatar-group.png';
  }

  public function getFavoriteAttribute()
  {
      return -1;
  }

}
