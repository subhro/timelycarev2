<?php

use Illuminate\Database\Eloquent\Model;


class Chat extends Model{

  public $table = 'chats';
  public $timestamps = false;
  protected $hidden = ['sender', 'receiver'];
  protected $appends = ['sender_name', 'receiver_name'];

  public function sender()
  {
      return $this->belongsTo('User', 'sender_id', 'id');
  }

  public function receiver()
  {
      return $this->belongsTo('User', 'receiver_id', 'id');
  }

  public function getSenderNameAttribute()
  {
      return $this->sender->name;
  }

  public function getReceiverNameAttribute()
  {
      return $this->receiver->name;
  }

  public function getTimestampAttribute($value)
  {
      return date('H:i, d-M', strtotime($value));
  }

}
