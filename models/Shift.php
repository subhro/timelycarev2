<?php

use Illuminate\Database\Eloquent\Model;


class Shift extends Model{

  public $table = 'shifts';
  public $timestamps = false;

}
