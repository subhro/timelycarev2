<?php

use Illuminate\Database\Eloquent\Model;


class Availability extends Model{

  public $table = 'availabilities';
  public $timestamps = false;

}
