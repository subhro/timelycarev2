<?php

if(Input::has('path')){
  $path = Input::get('path');

  if(@unlink($path)) {
    $status['code'] = 0;
  } else {
    $status['code'] = 1;
  }

  echo json_encode($status);
  exit();
}

function get_log_files($dir, &$results = array()) {

  $files = scandir($dir);
  $dirs_list = [];
  $files_list = [];

  if ( $files ) {
    foreach ( $files as $key => $value ) {
      $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
      if ( !is_dir($path) ) {
        $files_list[] = $path;
      } elseif ( $value != "." && $value != ".." ) {
        $dirs_list[] = $path;
      }
    }
    rsort($files_list);
    foreach ( $files_list as $path ) {
      preg_match("/^.*\/(\S+)$/", $path, $matches);
      $name = str_replace($dir.DIRECTORY_SEPARATOR,'',$path);
      $results[$dir][$name] = array('name' => $name, 'path' => $path);
    }
    if ( count($dirs_list) > 0 ) {
      foreach ( $dirs_list as $path ) {
        get_log_files($path, $results);
      }
    }
    return $results;
  }
  return false;

}

function tail($filename, $lines = 50, $buffer = 4096) {
  if ( !is_file($filename) ) {
    return false;
  }
  $f = fopen($filename, "rb");
  if ( !$f ) {
    return false;
  }

  fseek($f, -1, SEEK_END);

  if ( fread($f, 1) != "\n" ) $lines -= 1;

  $output = '';
  $chunk = '';

  while ( ftell($f) > 0 && $lines >= 0 ) {
    $seek = min(ftell($f), $buffer);
    fseek($f, -$seek, SEEK_CUR);
    $output = ($chunk = fread($f, $seek)).$output;
    fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
    $lines -= substr_count($chunk, "\n");
  }

  while ( $lines++ < 0 ) {
    $output = substr($output, strpos($output, "\n") + 1);
  }

  fclose($f);
  return $output;
}

function show_list_of_files($files, $lines = 50) {
  global $log;

  if ( empty($files) ) {
    return false;
  }
  if ( !isset($log) )
  $log = 0;

  foreach ( $files as $dir => $files_array ) {
    foreach ( $files_array as $k => $f ) {
      if ( !is_file($f['path']) ) {
        unset($files_array[$k]);
        continue;
      }

      $page = str_replace(LOGS_DIR . '/', '', $log);

      $active = ($f['name'] == $page) ? 'list-group-item-warning' : '';
      echo '<li class="list-group-item justify-content-between ' . $active . '">';
      echo '<a href="?p=' . urlencode($f['name']) . '&lines=' . $lines . '">' . $f['name'] . '</a>';
      echo '<span style="cursor:pointer" onClick="trashLogFile(\'' . $f['path'] . '\')" class="badge badge-danger badge-pill"><i class="fa fa-times" aria-hidden="true"></i></span>';
      echo '</li>';
    }
  }
}

define('DISPLAY_REVERSE', true);

$log   = (!isset($_GET['p'])) ? 0 : LOGS_DIR . '/' . urldecode($_GET['p']);
$lines = (!isset($_GET['lines'])) ? 50 : $_GET['lines'];
$files = get_log_files(LOGS_DIR);

$filename = $log;
$title = substr($log, (strrpos($log, '/') + 1));

$page_name = 'Error Logs';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="/assets/css/framework.css">
  <link rel="stylesheet" href="/assets/css/app.css">
  <title>Error Logs</title>
  <style media="screen">
  body, *{
    font-family: sans-serif;
  }
  #layout{
    padding: 40px;
  }
  #clockUTC{
    width: 30%;
    padding: 4px 9px;
    border-radius: 4px;
    box-shadow: 0 2px 4px 0px rgba(35,47,97,0.4);
    color: #ffffff;
    background: #0275d8;
    text-align: center;
  }
  .left-header{
    font-size: 40px;
    font-family: Constantia;
    font-weight: normal;
  }
  .jumbotron{
    padding: 4rem 2rem;
    background-color: #eceeef;
  }
  .container{
    width: 1140px;
    max-width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    margin-left: auto;
    margin-right: auto;
  }
  .container h1{
    font-family: Constantia;
    font-size: 4.5rem;
    font-weight: 300;
    line-height: 1.1;
  }
  .container .lead{
    font-family: Constantia;
    font-size: 1.25rem;
    font-weight: 300;
  }
  .list-group{
    list-style: none;
    display: flex;
    flex-direction: column;
    padding-left: 0;
    margin-bottom: 0;
    margin-top: 15px;
  }
  .list-group-item.active{
    z-index: 2;
    color: #fff;
    background-color: #0275d8;
    border-color: #0275d8;
  }
  .list-group-item:first-child{
    border-top-right-radius: .25rem;
    border-top-left-radius: .25rem;
  }
  .justify-content-between{
    justify-content: space-between !important;
  }
  .list-group-item{
    position: relative;
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    padding: .75rem 1.25rem;
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
  }
  .badge-danger{
    background-color: #d9534f;
  }
  .badge-pill{
    padding-right: .6em;
    padding-left: .6em;
    border-radius: 10rem;
  }
  .badge{
    display: inline-block;
    padding: .25em .4em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25rem;
  }
  code{
    padding: .2rem .4rem;
    font-size: 90%;
    color: #bd4147;
    background-color: #f7f7f9;
    border-radius: .25rem;
  }
  </style>
</head>

<body>

  <div id="layout">

    <div id="main">
      <div class="header">
        <div class="row">
          <div class="col-sm-3">
            <h1 class="left-header"><?php echo !empty($title) ? $title : $page_name; ?></h1>
          </div>
          <div class="col-sm-5">
            <div id="clockUTC"></div>
          </div>
          <div class="col-sm-4 text-right">
            <form action="" method="get" class="pure-form pure-form-aligned">
              <input type="hidden" name="p" value="<?php echo str_replace(LOGS_DIR . '/', '', $log); ?>">
              <label>
                How many lines to display?
                <select name="lines" onchange="this.form.submit()">
                  <option value="10" <?php echo ($lines == '10') ? 'selected' : ''; ?>>10</option>
                  <option value="50" <?php echo ($lines == '50') ? 'selected' : ''; ?>>50</option>
                  <option value="100" <?php echo ($lines == '100') ? 'selected' : ''; ?>>100</option>
                  <option value="500" <?php echo ($lines == '500') ? 'selected' : ''; ?>>500</option>
                  <option value="1000" <?php echo ($lines == '1000') ? 'selected' : ''; ?>>1000</option>
                </select>
              </label>
            </form>
          </div>
        </div>
      </div>

      <div class="content">
        <div class="row">
          <div class="col-sm-3" id="log_menu">
            <div class="pure-menu pure-menu-open">
              <ul class="list-group">
                <a href="#" class="list-group-item active">Log Dates</a>
                <?php show_list_of_files($files, $lines); ?>
              </ul>
            </div>
          </div>

          <div class="col-sm-9">

            <?php
            $output = tail($filename, $lines);
            if ( $output ) {
              $output = explode("\n", $output);
              if ( DISPLAY_REVERSE ) {
                $output = array_reverse($output);
              }
              $output = implode('</code><li class="list-group-item"><code>', $output);
              ?>

              <ul class="list-group"><?php echo $output; ?></ul>

            <?php } else { ?>

              <div class="jumbotron jumbotron-fluid">
                <div class="container">
                  <h1 class="display-3">Error Logs</h1>
                  <p class="lead">Choose Log Date from the list to view error logs.</p>
                </div>
              </div>

            <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">

  function renderTime() {
    var currentTime = new Date();
    var diem = "AM";
    var y = currentTime.getUTCFullYear();
    var n = currentTime.getUTCMonth() + 1;
    var d = currentTime.getUTCDate();
    var h = currentTime.getUTCHours();
    var m = currentTime.getUTCMinutes();
    var s = currentTime.getUTCSeconds();
    if(h == 0) {
        h = 12;
    }
    if(h < 10) {
        h = "0" + h;
    }
    if(m < 10) {
        m = "0" + m;
    }
    if(s < 10) {
        s = "0" + s;
    }

    n = (n < 10) ? '0' + n : n;
    d = (d < 10) ? '0' + d : d;

    var myclock = document.getElementById('clockUTC');
    myclock.innerHTML = y + "-" + n + "-" + d + " " + h + ":" + m + ":" + s;
    setTimeout('renderTime()', 1000);
  }

  renderTime();

  setInterval(renderTime, 1000);

  var $devUrl = "/<?=DEV_URL?>";

  function trashLogFile(path) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', $devUrl + '/logs');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
      if (xhr.status === 200) {
        window.location.reload();
      }
    };
    xhr.send(encodeURI('path=' + path));
  }
  </script>

</body>
</html>
