<?php

$data   = false;
$events = false;

$rules = [
  'type'    => 'required',
  'message' => 'required'
];
$msg = [
  'type:required'    => 'Please select user type',
  'message:required' => 'Please enter some message'
];
$errors = Input::validate($rules, $msg);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$message = Input::get('message');
$type    = Input::get('type');
if($type == 'error') {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Please select user type'
    ]
  ];
  goto RESPONSE;
}

$userIds = Input::get('userIds');
if(!count($userIds)) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Please select members'
    ]
  ];
  goto RESPONSE;
}


foreach($userIds as $member) {
  $chat = new Chat;
  $chat->type         = 8;  
  $chat->sender_id    = 1;
  $chat->receiver_id  = $member;
  $chat->message      = $message;
  $chat->message_type = 'text';
  $chat->timestamp    = date('Y-m-d H:i:s');
  $chat->save();
}


$events = [
  'page.refresh' => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Chat Notification send successfully.'
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
