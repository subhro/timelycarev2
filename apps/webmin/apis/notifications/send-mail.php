<?php

$data   = false;
$events = false;

$rules = [
  'type'    => 'required',
  'subject' => 'required',
  'message' => 'required'
];
$msg = [
  'type:required'    => 'Please select user type',
  'subject:required' => 'Please enter subject',
  'message:required' => 'Please enter some message'
];
$errors = Input::validate($rules, $msg);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$subject = Input::get('subject');
$message = Input::get('message');
$type    = Input::get('type');
if($type == 'error') {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Please select user type'
    ]
  ];
  goto RESPONSE;
}

$userIds = Input::get('userIds');
if(!count($userIds)) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Please select members'
    ]
  ];
  goto RESPONSE;
}


foreach($userIds as $member) {
  $to = $member;
  $body = Email::message('notification', ['message' => $message]);
  // Email::send($to, $subject, $body);
}


$events = [
  'page.refresh' => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Mail Notification send successfully.'
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
