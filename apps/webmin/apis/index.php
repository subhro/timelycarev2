<?php

$data   = false;
$events = false;

$rules = [
  'email' => 'required|email',
  'password' => 'required'
];
$errors = Input::validate($rules);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Invalid Username or Password'
    ]
  ];
  goto RESPONSE;
}


$email    = Input::get('email');
$password = Input::get('password');

$user = User::where('email', $email)
->where('password', Crypto::hash($password))
->where('type', 8)
->first();

if(!$user) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Invalid Username or Password'
    ]
  ];
  goto RESPONSE;
}

Session::set('uid', $user->id);

$events = [
  'delay.redirect' => '/admin/dashboard'
];


RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
