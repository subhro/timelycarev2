<?php

$data   = false;
$events = false;
$prev_year_month = false;
$next_year_month = false;
$user = [];

$type = Input::get('type');
$year_month = Input::get('year_month');

if(!$year_month) {
  $year_month = date('Y-m', strtotime('+1 month'));
}

if($year_month != date('Y-m')) {
  $prev_year_month = date('Y-m', strtotime($year_month.' -1 month'));
}

if($year_month != date('Y-m', strtotime('+2 month'))) {
  $next_year_month = date('Y-m', strtotime($year_month.' +1 month'));
}
$year_month_text = date('F, Y', strtotime($year_month));

$year_month = explode('-', $year_month);
$year = $year_month[0];
$month = $year_month[1];


$offset = 1 - date('w', mktime(12, 0, 0, $month, 1, $year));
if($offset == '-5' && $month != '2'):
  $cells = 42;
elseif($offset == '1' && $year%4 != 0 && $month == '2'):
  $cells = 28;
endif;


$count = 42;
$class = '';
for($day=0; $day<$count; $day++):
  $time = mktime(12, 0, 0, $month, $day + $offset, $year);
  $week = date('w', $time);
  $index = (Int)$day/7;
  $date = date('Y-m-d', $time);
  $userCount = Schedule::where('type', $type)->where('date', $date)->get();
  $todayselect = ($date == date('Y-m-d')) ? 'today' : '';

  if (date('m', $time)!=$month):
    $calendar[$index][$week] = [];
  else:
    $calendar[$index][$week] = [
      'text'         => $day + $offset,
      'formatdate'   => date('jS F, Y', $time),
      'exactdate'    => date('Y-m-d', $time),
      'todayselect'  => $todayselect,
      'userCount'    => ($userCount->count()!=0) ? $userCount->count() : ''
    ];
  endif;
endfor;

$currentYear = date('Y');
$currentMonth = $month;
$firstdate = '01';
$desireDate = $currentYear.'-'.$currentMonth.'-'.$firstdate;


$schUsers = Schedule::where('trash', 0)
->where('type', $type)
->where('date', $desireDate)
->get();

$i = 0;
foreach($schUsers as $schUser){
  $user[$i]['id'] = $schUser->id;
  $user[$i]['date'] = $schUser->date;
  $user[$i]['shift_name'] = $schUser->shift_name;
  $user[$i]['shift_start'] = date("g:i A", strtotime($schUser->shift_start));
  $user[$i]['shift_end'] = date("g:i A", strtotime($schUser->shift_end));
  $user[$i]['shift_color_ft'] = $schUser->shift_color_ft;
  $user[$i]['users'] = User::find($schUser->user_id);
  $i++;
}

$data = [
  'calendar'        => $calendar,
  'prev_year_month' => $prev_year_month,
  'next_year_month' => $next_year_month,
  'year_month_text' => $year_month_text,
  'schedule'        => $user,
  'fulldate'        => date('jS F, Y', strtotime($desireDate)),
  'formateddate'    => $desireDate
];

return [
  'data'   => $data,
  'events' => $events
];
