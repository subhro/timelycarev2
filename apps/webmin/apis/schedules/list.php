<?php

$data   = false;
$events = false;
$prev_year_month = false;
$next_year_month = false;
$users = [];

$type = Input::get('type', 0);
$descId = Input::get('desc_id', 0);
$year_month = Input::get('year_month');

if(!$year_month) {
  $year_month = date('Y-m', strtotime('+1 month'));
}

if($year_month != date('Y-m')) {
  $prev_year_month = date('Y-m', strtotime($year_month.' -1 month'));
}

if($year_month != date('Y-m', strtotime('+2 month'))) {
  $next_year_month = date('Y-m', strtotime($year_month.' +1 month'));
}

$year_month_text = date('F, Y', strtotime($year_month));

$year_month_array = explode('-', $year_month);
$year = $year_month_array[0];
$month = $year_month_array[1];

$daysInMonth = date('t', strtotime($year_month.'-01'));
$daysInMonth = intval($daysInMonth);


for($day=0; $day<$daysInMonth; $day++):

  $weekends = false;
  $time = mktime(12, 0, 0, $month, $day+1, $year);
  $date = date('Y-m-d', $time);
  $week = date('D', $time);

  if($week === 'Sat' || $week === 'Sun'){
    $weekends = true;
  }

  $calendar[] = [
    'text'  => $day+1,
    'week'  => $week,
    'weekends'  => $weekends
  ];
endfor;

if($descId){
  $description = Description::find($descId);
}else{
  $description = Description::where('type', $type)->first();
}


$users = User::where('type', $type)
    ->where(function($query) use(&$description){
        if(isset($description)){
          $query->where('description_id', $description->id);
        }
    })
    ->where('trash', 0)
    ->get();

foreach($users as $user) :

  $schedule = [];
  for($day=0; $day<$daysInMonth; $day++):

    $time = mktime(12, 0, 0, $month, $day+1, $year);
    $date = date('Y-m-d', $time);

    $userSchedule = Schedule::where('trash', 0)
    ->where('type', $type)
    ->where('user_id', $user->id)
    ->where('date', $date)
    ->first();

    if(!$userSchedule) {
      $schedule[] = [
        'id'             => 0,
        'date'           => '',
        'shift_name'     => '',
        'shift_start'    => '',
        'shift_end'      => '',
        'shift_color_ft' => '000000',
        'shift_color_bg' => 'ffffff',
        'user_id' => $user->id,
      ];
      $user->schedule = $schedule;
      continue;
    }

      $shift_start = date("g:i A", strtotime($userSchedule->shift_start));
      $shift_end = date("g:i A", strtotime($userSchedule->shift_end));

      $schedule[] = [
        'id'             => $userSchedule->id,
        'date'           => $userSchedule->date,
        'shift_name'     => $userSchedule->shift_name,
        'shift_start'    => $shift_start,
        'shift_end'      => $shift_end,
        'shift_color_ft' => $userSchedule->shift_color_ft,
        'shift_color_bg' => $userSchedule->shift_color_bg,
        'user_id' => $user->id
      ];

      $user->schedule = $schedule;

  endfor;
endforeach;


$shifts = Shift::where('type', $type)
->where(function($query) use(&$description){
      if(isset($description)){
        $query->where('description_id', $description->id);
      }
  })
  ->get();


$data = [
  'calendar'        => $calendar,
  'prev_year_month' => $prev_year_month,
  'next_year_month' => $next_year_month,
  'year_month_text' => $year_month_text,
  'year_month'      => $year_month,
  'daycount'        => $daysInMonth,
  'users'           => $users,
  'shifts'          => $shifts
];

return [
  'data'   => $data,
  'events' => $events
];
