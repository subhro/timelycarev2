<?php

$data   = false;
$events = false;

$rules = [
  'user_id'  => 'required',
  'shift_id' => 'required'
];
$msg = [
  'user_id:required'  => 'Please select a User',
  'shift_id:required' => 'Please select a Shift',
];
$errors = Input::validate($rules, $msg);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$pieces = explode('-', Input::get('date'));
$yearmonth  = implode('-', array_slice($pieces, 0, 2));

$shiftDetails =  Shift::where('id', Input::get('shift_id'))->first();

$schedule = Schedule::where('user_id', Input::get('user_id'))
->where('date', Input::get('date'))
->first();

if(!$schedule) {
  $schedule = new Schedule;
  $message = 'Schedule stored successfully';
} else {
  $message = 'Schedule updated successfully';
}
$schedule->user_id         = Input::get('user_id');
$schedule->type            = Input::get('type');
$schedule->year_month      = $yearmonth;
$schedule->date            = Input::get('date');
$schedule->shift_id        = Input::get('shift_id');
$schedule->shift_name      = $shiftDetails->name;
$schedule->shift_start     = $shiftDetails->start_time;
$schedule->shift_end       = $shiftDetails->end_time;
$schedule->shift_color_ft  = $shiftDetails->color_ft;
$schedule->shift_color_bg	 = $shiftDetails->color_bg;
$schedule->status          = 0;
$schedule->trash           = 0;
$schedule->save();

$events = [
  'modal.close'  => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => $message
  ]
];

RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
