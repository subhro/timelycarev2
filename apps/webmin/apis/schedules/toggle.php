<?php


$data   = false;
$events = false;



// id : id,
// date : date,
// year_month : year_month,
// shift_id : shift_id,
// user_id : user_id,

$id = Input::get('id');
$date = Input::get('date');
$year_month = Input::get('year_month');
$shift_id = Input::get('shift_id');
$user_id = Input::get('user_id');

$user = User::find($user_id);
$shift = Shift::find($shift_id);
$schedule = Schedule::find($id);

if(!$shift){

  $emptySchedule = [
    'id'             => 0,
    'date'           => '',
    'shift_name'     => '',
    'shift_start'    => '',
    'shift_end'      => '',
    'shift_color_ft' => '000000',
    'shift_color_bg' => 'ffffff'
  ];

  if($schedule){
    $schedule->trash = 1;
    $schedule->save();
  }

  return $emptySchedule;
}

if(!$schedule){
  $schedule = new Schedule;
}

$schedule->user_id = $user->id;
$schedule->type = $user->type;

$schedule->year_month = $year_month;
$schedule->date = $date;

$schedule->shift_name = $shift->name;
$schedule->shift_start = $shift->start_time;
$schedule->shift_end  = $shift->end_time;
$schedule->shift_color_ft = $shift->color_ft;
$schedule->shift_color_bg	= $shift->color_bg;
$schedule->status = 0;
$schedule->trash = 0;
$schedule->save();



$schedule = [
  'id'             => $schedule->id,
  'date'           => $date,
  'shift_name'     => $shift->name,
  'shift_start'    => $shift->start_time,
  'shift_end'      => $shift->end_time,
  'shift_color_ft' => $shift->color_ft,
  'shift_color_bg' => $shift->color_bg
];


return $schedule;
