<?php

set_time_limit(60);

$data   = false;
$events = false;

$type = Input::get('type');
$descId = Input::get('desc_id');
$year_month = Input::get('year_month');

$days = intval(date('t', strtotime($year_month.'-01')));

if($descId){
  $description = Description::find($descId);
}else{
  $description = Description::where('type', $type)->first();
}

$rules = Rules::where('type', $type)
  ->where('description_id', $description->id)
  ->first();

$shifts = Shift::where('type', $type)
    ->where('description_id', $description->id)
    ->get();

$staffs = User::where('type', $type)
  ->where('description', $description->name)
  ->where('trash', 0)
  ->get();





$shiftCount = $shifts->count();
$staffCount = $staffs->count();

$staffPerDay = 0;
$staffPerShift = [];
foreach ($shifts as $shift) {
  $staffPerShift[] = $shift->staff_count ?? 1;
}

$staffPerMonth = $rules->per_month ?? 14;
$consecutiveDay = $rules->consecutive_day ?? 5;



array_unshift($staffPerShift, 0);

$options = [
  'size' => 10,
  'crossover' => 'y',

  'days' => $days,
  'shifts' => $shiftCount,
  'staffs' => $staffCount,

  'staffPerMonth' => $staffPerMonth,
  'staffPerShift' => $staffPerShift,
  'consecutiveDay' => $consecutiveDay,
];

//d($options);

$population = false;

// if(!Input::get('reset', false)){
//   $population = File::read('/roster/model.json');
//   $population = json_decode($population, true);
// }

// $population = File::read('/roster/model.json');
// $population = json_decode($population, true);

$prevStaffCount = $population[0]['chromosome'][0] ?? [];

if($staffCount != count($prevStaffCount)){
  $population = false;
}

Roster::init($population, $options);
$population = Roster::generate();

File::write('/roster/model.json', json_encode($population));

$solution = $population[0]['chromosome'];

// echo $population[0]['fitness'];
// echo '<table>';
// for ($i=0; $i < $staffCount; $i++) {
//   echo '<tr>';
//   for ($j=0; $j < $days; $j++) {
//     echo '<td>'.$solution[$j][$i].'</td>';
//   }
//   echo '</tr>';
// }
// echo '</table>';
// die;

for ($i=0; $i < $days; $i++) {
  for ($j=0; $j < $staffCount; $j++) {

      $user = $staffs[$j];
      $date = $year_month.'-'.sprintf("%02d", $i+1);

      if($solution[$i][$j]){
        $shift = $shifts[$solution[$i][$j]-1];
      }else{
        Schedule::where('date', $date)
                  ->where('user_id', $user->id)
                  ->update(['trash'=>1]);
        continue;
      }

      $schedule = Schedule::where('date', $date)
                  ->where('user_id', $user->id)
                  ->first();

      if(!$schedule){
        $schedule = new Schedule;
      }

      $schedule->user_id = $user->id;
      $schedule->type = $type;

      $schedule->year_month = $year_month;
      $schedule->date = $date;

      $schedule->shift_name = $shift->name;
      $schedule->shift_start = $shift->start_time;
      $schedule->shift_end  = $shift->end_time;
      $schedule->shift_color_ft = $shift->color_ft;
      $schedule->shift_color_bg	= $shift->color_bg;
      $schedule->status = 0;
      $schedule->trash = 0;
      $schedule->save();

  }
}

$fitness = intval($population[0]['fitness'] * 1000) / 1000;
$fitness = $fitness > 0.01 ? $fitness : 0.01;

return [
  'data' => [
    'fitness' => $fitness
  ],
  'events' => false
];
