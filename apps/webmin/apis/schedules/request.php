<?php

$data    = false;
$events  = false;
$shifts  = [];
$userIds = [];

$limit = 20;
$page = Input::get('page', 1);

$type = Input::get('usertype', 0);

$userIds = User::where(function($query){
  if(Input::get('q')) {
    $query->where('name', 'like', Input::get('q').'%');
  }
  if(Input::get('description')) {
    $query->where('description_id', Input::get('description'));
  }
})
->pluck('id');

$requests = ShiftRequest::where('type', $type)
->where(function($query) use($userIds){
  if(Input::get('q')) {
    $query->whereIn('user_id', $userIds);
  }
  if(Input::get('description')) {
    $query->whereIn('user_id', $userIds);
  }
  if(Input::get('working')) {
    $query->where('working', Input::get('working'));
  }
  if(Input::get('year_month')) {
    $query->where('year_month', Input::get('year_month'));
  }
})
->limit($limit)
->skip($limit *($page-1))
->get();

$pagination = Pagination::get($page, $limit, $requests->count());

$i = 0;
foreach($requests as $request) {
  $shifts[$i]['id']              = $request->id;
  $shifts[$i]['date']            = date('jS F, Y', strtotime($request->date));
  $shifts[$i]['working']         = $request->working;
  $shifts[$i]['status']          = $request->status;
  $shifts[$i]['users']           = User::with('description')->find($request->user_id);
  $i++;
}

$data   = [
  'requests'   => $shifts,
  'pagination' => $pagination
];

return [
  'data'   => $data,
  'events' => $events
];
