<?php
$data   = false;
$events = false;

$user = [];

$currentYear = date('Y');
$currentMonth = date('m', strtotime('+1 month'));
$firstdate = '01';
$defaultDate = $currentYear.'-'.$currentMonth.'-'.$firstdate;
$desireDate  = Input::get('date', $defaultDate);
$type = Input::get('type');

$schUsers = Schedule::where('trash', 0)
->where('type', $type)
->where('date', $desireDate)
->get();

$i = 0;
foreach($schUsers as $schUser){
  $user[$i]['id'] = $schUser->id;
  $user[$i]['date'] = $schUser->date;
  $user[$i]['shift_name'] = $schUser->shift_name;
  $user[$i]['shift_start'] = date("g:i A", strtotime($schUser->shift_start));
  $user[$i]['shift_end'] = date("g:i A", strtotime($schUser->shift_end));
  $user[$i]['shift_color_ft'] = $schUser->shift_color_ft;
  $user[$i]['users'] = User::find($schUser->user_id);
  $i++;
}

$data = [
  'schedule'     => $user,
  'fulldate'     => date('jS F, Y', strtotime($desireDate)),
  'formateddate' => $desireDate
];

return [
  'data'   => $data,
  'events' => $events
];
