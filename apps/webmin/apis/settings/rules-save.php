<?php

$data   = false;
$events = false;

$rules = [
  'value' => 'required'
];
$msg = [
  'value:required' => 'Rules value required'
];
$errors = Input::validate($rules, $msg);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}


$rules = Rules::find(Input::get('id'));
$rules->value = Input::get('value');
$rules->save();


$events = [
  'page.refresh' => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Rules saved successfully.'
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
