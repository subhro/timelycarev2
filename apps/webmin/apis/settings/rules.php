<?php

$data   = false;
$events = false;

$nurseDescId = Input::get('nurse_description_id', 0);
$doctorDescId = Input::get('doctor_description_id', 0);


$description = Description::find($doctorDescId);
if(!$description){
  $description = Description::where('type', 0)->first();
}
$doctorRules = Rules::where('type', 0)
->where('description_id', $description->id)
->first();


$description = Description::find($nurseDescId);
if(!$description){
  $description = Description::where('type', 1)->first();
}
$nurseRules = Rules::where('type', 1)
->where('description_id', $description->id)
->first();

if(!$doctorRules){
  $doctorRules = ['consecutive_day'=>0, 'per_month'=>0];
}

if(!$nurseRules){
  $nurseRules = ['consecutive_day'=>0, 'per_month'=>0];
}




$data = [
  'doctor.rules' => $doctorRules,
  'nurse.rules' => $nurseRules
];

return [
  'data'   => $data,
  'events' => $events
];
