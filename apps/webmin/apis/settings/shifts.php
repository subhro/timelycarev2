<?php

$data   = false;
$events = false;


$description = Description::where('type', 0)
->where(function($query){
  if(Input::has('doctor_description_id')){
    $query->where('id', Input::get('doctor_description_id'));
  }
})
->first();
$descriptionId = $description->id ?? 0;

$doctor_shifts = Shift::where('type', 0)
->where('description_id', $descriptionId)
->get();

foreach($doctor_shifts as $shift) {
  $shift->display_start_time = date('h:iA', strtotime($shift->start_time));
  $shift->display_end_time = date('h:iA', strtotime($shift->end_time));
}


$description = Description::where('type', 1)
->where(function($query){
  if(Input::has('nurse_description_id')){
    $query->where('id', Input::get('nurse_description_id'));
  }
})
->first();
$descriptionId = $description->id ?? 0;

$nurse_shifts  = Shift::where('type', 1)
->where('description_id', $descriptionId)
->get();

foreach($nurse_shifts as $shift) {
  $shift->display_start_time = date('h:iA', strtotime($shift->start_time));
  $shift->display_end_time = date('h:iA', strtotime($shift->end_time));
}





$data = [
  'doctors_shifts' => $doctor_shifts,
  'nurses_shifts'  => $nurse_shifts
];

return [
  'data'   => $data,
  'events' => $events
];
