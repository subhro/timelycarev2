<?php

$data   = false;
$events = false;

$doctors_group = User::where('type', 2)->where('trash', 0)->where('has_group', 1)->get();
$nurses_group  = User::where('type', 2)->where('trash', 0)->where('has_group', 2)->get();

$data   = [
  'doctors_group' => $doctors_group,
  'nurses_group'  => $nurses_group
];

return [
  'data'   => $data,
  'events' => $events
];
