<?php

$err = true;
$data = false;
$events = false;

$rules = [
  'name'        => 'required',
  'start_time'  => 'required',
  'end_time'    => 'required',
  //'shift_color' => 'required',
  'staff_count' => 'required'
];
$messages = [
  'name'        => 'Enter a shift name',
  'start_time'  => 'Enter a start time',
  'end_time'    => 'Enter a end name',
  //'shift_color' => 'Select a shift color',
  'staff_count' => 'Enter a allowed staff for the shift'
];
$errors = Input::validate($rules, $messages);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}


$checkShift = Shift::where('name', Input::get('name'))
->where('id', '!=', Input::get('id'))
->where('type', Input::get('type'))
->first();
if($checkShift) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Another shift with same name exist.'
    ]
  ];
  goto RESPONSE;
}



if(Input::get('shift_color')!=null) {
  $shift_color = explode(',', Input::get('shift_color'));
  $color_ft    = $shift_color[0];
  $color_bg    = $shift_color[1];
} else {
  if(Input::get('id')!="") {
    $color     = Shift::where('id', Input::get('id'))->first();
    $color_ft  = $color->color_ft;
    $color_bg  = $color->color_bg;
  } else {
    $events = [
      'message.show' => [
        'type'  => 'error',
        'text'  => 'Something went wrong.'
      ]
    ];
    goto RESPONSE;
  }
}



$checkColor = Shift::where('color_ft', $color_ft)
->where('color_bg', $color_bg)
->where('id', '!=', Input::get('id'))
->where('type', Input::get('type'))
->first();
if($checkColor) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Another shift with same color pattern exist.'
    ]
  ];
  goto RESPONSE;
}




if(Input::get('id')=="") {
  $shift    = new Shift;
  $message  = 'Shift added successfully.';
} else {
  $shift    = Shift::where('id', Input::get('id'))->first();
  Schedule::where('shift_id', Input::get('id'))
  ->update([
    'shift_id'        => Input::get('id'),
    'shift_name'      => Input::get('name'),
    'shift_start'     => Input::get('start_time'),
    'shift_end'       => Input::get('end_time'),
    'shift_color_ft'  => $color_ft,
    'shift_color_bg'  => $color_bg
  ]);
  $message = 'Shift updated successfully.';
}


$shift->name              = Input::get('name');
$shift->type              = Input::get('type');
$shift->description_id    = Input::get('description_id');
$shift->start_time        = Input::get('start_time');
$shift->end_time          = Input::get('end_time');
$shift->color_ft          = $color_ft;
$shift->color_bg          = $color_bg;
$shift->staff_count       = Input::get('staff_count');
$shift->save();

$err = false;

$events = [
  'message.show' => [
    'type'  => 'success',
    'text'  => $message
  ]
];



RESPONSE:
return [
  'err' => $err,
  'data' => $data,
  'events' => $events
];
