<?php

$data   = false;
$events = false;

$uid = Session::get('uid');
$user = User::find($uid);

$departments = Description::where('type', 0)->get();
$locations   = Description::where('type', 1)->get();

$description = json_decode($user->description);

if(count($description) != 2){
  $description = [0,0];
}

$data = [
  'departments' => $departments,
  'locations'   => $locations,
  'description' => $description
];

return [
  'data'   => $data,
  'events' => $events
];
