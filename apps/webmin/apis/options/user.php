<?php

$data   = false;
$events = false;
$options = [];

$users = User::where('type', Input::get('type'))
  ->where('trash', 0)
  ->limit(10)
  ->get();

foreach($users as $user) {
  $options[] = [
    'value' => $user->id,
    'label' => $user->name
  ];
}

$data = [
  'options.details' => $options
];

return [
  'data'   => $data,
  'events' => $events
];
