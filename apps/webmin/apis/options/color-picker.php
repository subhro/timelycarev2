<?php

$data   = false;
$events = false;

$options = [
  [
    'value'    => 'ffffff,f1c40f',
    'label'    => '#f1c40f',
    'color_ft' => '#ffffff',
    'color_bg' => '#f1c40f'
  ],
  [
    'value'    => 'ffffff,d35400',
    'label'    => '#d35400',
    'color_ft' => '#ffffff',
    'color_bg' => '#d35400'
  ],
  [
    'value'    => 'ffffff,c0392b',
    'label'    => '#c0392b',
    'color_ft' => '#ffffff',
    'color_bg' => '#c0392b'
  ],
  [
    'value'    => 'ffffff,16a085',
    'label'    => '#16a085',
    'color_ft' => '#ffffff',
    'color_bg' => '#16a085'
  ],
  [
    'value'    => 'ffffff,27ae60',
    'label'    => '#27ae60',
    'color_ft' => '#ffffff',
    'color_bg' => '#27ae60'
  ],
  [
    'value'    => 'ffffff,2980b9',
    'label'    => '#2980b9',
    'color_ft' => '#ffffff',
    'color_bg' => '#2980b9'
  ],
  [
    'value'    => 'ffffff,9b59b6',
    'label'    => '#9b59b6',
    'color_ft' => '#ffffff',
    'color_bg' => '#9b59b6'
  ],
  [
    'value'    => 'ffffff,566473',
    'label'    => '#566473',
    'color_ft' => '#ffffff',
    'color_bg' => '#566473'
  ]
];

$data = [
  'options.colorpicker' => $options
];

return [
  'data'   => $data,
  'events' => $events
];
