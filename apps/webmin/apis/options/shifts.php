<?php

$data   = false;
$events = false;
$options = [];

$type = Input::get('type', 0);
if(!Input::get('desc_id')) {
  $desc_id = Description::where('type', $type)->first();
} else {
  $desc_id = Input::get('desc_id');
}

$shifts = Shift::where(function($query) use(&$type, &$desc_id){
  if(Input::has('type')){
    $query->where('type', $type);
  }
  if(Input::has('desc_id')){
    $query->where('description_id', $desc_id);
  }
})->get();

foreach($shifts as $shift) {
  $options[] = [
    'value' => $shift->id,
    'label' => $shift->name
  ];
}

$data = [
  'options.shifts' => $options
];

return [
  'data'   => $data,
  'events' => $events
];
