<?php

$data   = false;
$events = false;

$options = [
    [
      'value' => 0,
      'label' => 'Doctor'
    ],
    [
      'value' => 1,
      'label' => 'Nurse'
    ]
];

$data = [
  'options.type' => $options,
  'search.type' => 0, //set value after options
];

return [
  'data'   => $data,
  'events' => $events
];
