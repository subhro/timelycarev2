<?php

$data   = false;
$events = false;

$type = Input::get('type', 0);
$descriptions = Description::where('type', $type)->get();

$options = [];
foreach($descriptions as $description) {
  $options[] = [
    'value' => $description->id,
    'label' => $description->name
  ];
}

if($type == 0){
  $key = 'doctor';
}else{
  $key = 'nurse';
}

$option = $options[0] ?? ['value' => 0];

$data = [
  'options.description' => $options,
  'search.desc_id' => $option['value'],

  'options.'.$key.'_description' => $options,
  '$'.$key.'.description_id' => $option['value']
];

return [
  'data'   => $data,
  'events' => $events
];
