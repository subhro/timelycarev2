<?php

$data   = false;
$events = false;

$options = [
  [
    'value' => date('Y-m', strtotime('-1 month')),
    'label' => date('F, Y', strtotime(date('Y-m', strtotime('-1 month'))))
  ],
  [
    'value' => date('Y-m'),
    'label' => date('F, Y', strtotime(date('Y-m')))
  ],
  [
    'value' => date('Y-m', strtotime('+1 month')),
    'label' => date('F, Y', strtotime(date('Y-m', strtotime('+1 month'))))
  ]
];

$data = [
  'options.year_month' => $options,
  'search.type' => date('Y-m'), //set value after options
];

return [
  'data'   => $data,
  'events' => $events
];
