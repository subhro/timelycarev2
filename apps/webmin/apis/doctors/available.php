<?php

$data   = false;
$events = false;

$doctors = User::where('type', 0)
->where('trash', 0)
->where(function($query){
    if(Input::has('q')) {
       $query->where('name', 'like', Input::get('q').'%');
    }
})
->get();

$data   = [
  'doctors' => $doctors
];

return [
  'data'   => $data,
  'events' => $events
];
