<?php

$data   = false;
$events = false;

$limit = 20;
$page = Input::get('page', 1);


$doctors = User::with('description')
->where('type', 0)
->where('trash', 0)
->where(function($query){
  if(Input::has('q')) {
    $query->where('name', 'like', Input::get('q').'%');
  }
  if(Input::has('department')) {
    $query->where('description_id', Input::get('department'));
  }
})
->limit($limit)
->skip($limit *($page-1))
->get();

$pagination = Pagination::get($page, $limit, $doctors->count());

$data   = [
  'doctors'    => $doctors,
  'pagination' => $pagination
];

return [
  'data'   => $data,
  'events' => $events
];
