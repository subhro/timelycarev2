<?php

$data   = false;
$events = false;
$shift_ids = [];

$uid = Input::get('id', 0);
$rules = [
  'first_name'     => 'required|max:20',
  'last_name'      => 'required|max:20',
  'description_id' => 'required',
  'phone'          => 'required|min:10|max:12|numeric',
  'email'          => 'required|email',
  'priority'       => 'required',
  'shift_ids'      => 'required'
];

$messages = [
  'first_name:required' => 'Please provide first name',
  'first_name:max'      => 'Please provide valid name',
  'last_name:required'  => 'Please provide last name',
  'last_name:max'       => 'Please provide valid name',
  'phone:min'           => 'Please provide a valid number',
  'phone:max'           => 'Please provide a valid number'
];

$errors = Input::validate($rules, $messages);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$shift = Input::get('shift_ids');
foreach($shift as $val){
  $shift_ids[] = isset($val['value']) ? $val['value'] : $val;
}


$doctor = User::where('id', $uid)->first();
if($doctor) {
  $checkUser = User::where('email', Input::get('email'))->where('id', '!=', $uid)->first();
  if($checkUser) {
    $events = [
      'message.show' => [
        'type'  => 'error',
        'text'  => 'Email address already exist.'
      ]
    ];
    goto RESPONSE;
  }
  $flag     = 'edit';
  $pass     = Input::get('password') ? Input::get('password') : false;
  $password = Input::get('password') ? Crypto::hash($pass) : $doctor->password;
  $message  = 'Doctor details saved successfully.';
} else {
  $checkUser = User::where('email', Input::get('email'))->first();
  if($checkUser) {
    $events = [
      'message.show' => [
        'type'  => 'error',
        'text'  => 'Email address already exist.'
      ]
    ];
    goto RESPONSE;
  }
  $doctor   = new User;
  $flag     = 'new';
  $pass     = Input::get('password') ? Input::get('password') : uniqid();
  $password = Crypto::hash($pass);
  $message  = 'Doctor details saved successfully.';
}


$doctor->type            = 0;
$doctor->name            = Input::get('first_name').' '.Input::get('last_name');
$doctor->email           = Input::get('email');
$doctor->password        = $password;
$doctor->phone           = Input::get('phone');
$doctor->description_id  = Input::get('description_id');
$doctor->status          = 0;
$doctor->priority        = Input::get('priority');
$doctor->shift_ids       = $shift_ids;
$doctor->timestamp       = date('Y-m-d H:i:s');
$doctor->trash           = 0;
$doctor->save();


$template = ($flag=='new') ? 'welcome' : 'update';
if($pass){
  $userData = [
    'name'  => Input::get('first_name'),
    'email' => Input::get('email'),
    'password' => $pass,
    'site_url' => SITE_URL
  ];
  $to = Input::get('email');
  $subject = 'Timely Care';
  $body = Email::message($template, $userData);
  Email::send($to, $subject, $body);
}


$events = [
  'modal.close'  => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => $message
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
