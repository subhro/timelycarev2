<?php

$data   = false;
$events = false;

$rules = [
  'name'        => 'required|min:5|max:30',
  'description' => 'required|min:10|max:250'
];
$messages = [
  'name:required'         => 'Please provide name',
  'name:min'              => 'Name should be minimum 5 characters',
  'name:max'              => 'Name should be maximum 30 characters',
  'description:required'  => 'Please provide description',
  'description:min'       => 'Description should be minimum 10 characters',
  'description:max'       => 'Description should be maximum 250 characters'
];
$errors = Input::validate($rules, $messages);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$userIds = Input::get('userIds');
if(!count($userIds)) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Please select members'
    ]
  ];
  goto RESPONSE;
}

if(Input::get('key') == 'doctor') {
  $type      = 1;
  $groupType = 0;
} else {
  $type      = 2;
  $groupType = 1;
}


$user = User::where('name', Input::get('name'))
->where('type', 2)
->where('has_group', $type)->first();
if(!$user) {
  $user = new User;
}

$user->type = 2;
$user->name = Input::get('name');
$user->has_group         = $type;
$user->description_text  = Input::get('description');
$user->timestamp         = date('Y-m-d H:i:s');
$user->save();
$userID = $user->id;

foreach($userIds as $member) {
  $group = Group::where('user_id', $member)->where('group_id', $userID)->first();
  if(!$group) {
    $group = new Group;
  }
  $group->type      = $groupType;
  $group->group_id  = $userID;
  $group->user_id   = $member;
  $group->timestamp = date('Y-m-d H:i:s');
  $group->save();
}


$events = [
  'modal.close'  => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Group added successfully.'
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
