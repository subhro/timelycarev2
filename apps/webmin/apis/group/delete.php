<?php

$data   = false;
$events = false;

$id = Input::get('id', 0);

$user = User::find($id);
$user->delete();

$group = Group::where('group_id', $id)->delete();


$events = [
  'records.delete' => [
    'key'   => Input::get('key'),
    'index' => Input::get('index')
  ],
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Deleted successfully'
  ]
];

return [
  'data'   => $data,
  'events' => $events
];
