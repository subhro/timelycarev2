<?php

$data   = false;
$events = false;

$id = Crypto::decrypt(Input::get('id'));
$pass = Input::get('pass');

$user = User::find($id);
if(!$user) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'Invalid user to update password!'
    ]
  ];
  goto RESPONSE;
}

$user->password = $pass;
$user->save();

RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
