<?php

$data   = false;
$events = false;
$shift_ids = [];

$uid = Input::get('id', 0);
$rules = [
  'first_name'     => 'required|max:20',
  'last_name'      => 'required|max:20',
  'description_id' => 'required',
  'phone'          => 'required|min:10|max:12|numeric',
  'email'          => 'required|email',
  'priority'       => 'required',
  'shift_ids'      => 'required'
];

$messages = [
  'first_name:required' => 'Please provide first name',
  'first_name:max'      => 'Please provide valid name',
  'last_name:required'  => 'Please provide last name',
  'last_name:max'       => 'Please provide valid name',
  'phone:min'           => 'Please provide a valid number',
  'phone:max'           => 'Please provide a valid number'
];

$errors = Input::validate($rules, $messages);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$shift = Input::get('shift_ids');
foreach($shift as $val){
  $shift_ids[] = isset($val['value']) ? $val['value'] : $val;
}


$nurse = User::where('id', $uid)->first();
if($nurse) {
  $checkUser = User::where('email', Input::get('email'))->where('id', '!=', $uid)->first();
  if($checkUser) {
    $events = [
      'message.show' => [
        'type'  => 'error',
        'text'  => 'Email address already exist.'
      ]
    ];
    goto RESPONSE;
  }
  $flag     = 'edit';
  $pass     = Input::get('password') ? Input::get('password') : false;
  $password = Input::get('password') ? Crypto::hash($pass) : $nurse->password;
  $message  = 'Nurse details saved successfully.';
} else {
  $checkUser = User::where('email', Input::get('email'))->first();
  if($checkUser) {
    $events = [
      'message.show' => [
        'type'  => 'error',
        'text'  => 'Email address already exist.'
      ]
    ];
    goto RESPONSE;
  }
  $nurse   = new User;
  $flag     = 'new';
  $pass     = Input::get('password') ? Input::get('password') : uniqid();
  $password = Crypto::hash($pass);
  $message  = 'Nurse details saved successfully.';
}



$nurse->type              = 1;
$nurse->name              = Input::get('first_name').' '.Input::get('last_name');
$nurse->email             = Input::get('email');
$nurse->password          = $password;
$nurse->phone             = Input::get('phone');
$nurse->description_id    = Input::get('description_id');
$nurse->status            = 0;
$nurse->priority          = Input::get('priority');
$nurse->shift_ids         = $shift_ids;
$nurse->timestamp         = date('Y-m-d H:i:s');
$nurse->trash             = 0;
$nurse->save();

$template = ($flag=='new') ? 'welcome' : 'update';
if($pass){
  $userData = [
    'name'  => Input::get('first_name'),
    'email' => Input::get('email'),
    'password' => $pass,
    'site_url' => SITE_URL,
  ];
  $to = Input::get('email');
  $subject = 'Timely Care';
  $body = Email::message($template, $userData);
  Email::send($to, $subject, $body);
}


$events = [
  'modal.close'  => false,
  'message.show' => [
    'type'  => 'success',
    'text'  => $message
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
