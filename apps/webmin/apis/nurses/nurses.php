<?php

$data   = false;
$events = false;

$limit = 20;
$page = Input::get('page', 1);


$nurses = User::with('description')
->where('type', 1)
->where('trash', 0)
->where(function($query){
  if(Input::has('q')) {
    $query->where('name', 'like', Input::get('q').'%');
  }
  if(Input::has('location')) {
    $query->where('description_id', Input::get('location'));
  }
})
->limit($limit)
->skip($limit *($page-1))
->get();

$pagination = Pagination::get($page, $limit, $nurses->count());

$data   = [
  'nurses'     => $nurses,
  'pagination' => $pagination
];

return [
  'data'   => $data,
  'events' => $events
];
