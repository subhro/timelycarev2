<?php

$data   = false;
$events = false;

$nurses = User::where('type', 1)
->where('trash', 0)
->where(function($query){
    if(Input::has('q')) {
       $query->where('name', 'like', Input::get('q').'%');
    }
})
->get();

$data   = [
  'nurses' => $nurses
];

return [
  'data'   => $data,
  'events' => $events
];
