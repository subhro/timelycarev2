<?php

$data   = false;
$events = false;

$rules = [
  'type' => 'required',
  'name' => 'required|min:5|max:30'
];
$messages = [
  'type:required'  => 'Please select user type',
  'name:required'  => 'Please provide name',
  'name:min'       => 'Name should be minimum 5 characters',
  'name:max'       => 'Name should be maximum 30 characters'
];
$errors = Input::validate($rules, $messages);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$type = Input::get('type');
$name = Input::get('name');

$description = Description::where('type', $type)
              ->where('name', $name)
              ->first();

if(!$description){
  $description = new Description;
}

$description->type = $type;
$description->name = $name;
$description->save();

$type    = 'success';
$message = 'Category added successfully.';

$events = [
  'message.show' => [
    'type'  => $type,
    'text'  => $message
  ]
];



RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
