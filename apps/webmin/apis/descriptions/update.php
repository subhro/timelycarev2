<?php

$data   = false;
$events = false;

$uid = Session::get('uid');
$user = User::find($uid);
$description = Input::get('description');

$user->description = json_encode($description);
$user->save();

RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
