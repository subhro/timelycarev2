<?php

$data   = false;
$events = false;

$rules = [
  'email'     => 'required|email',
  'password'  => 'required|min:6|max:20',
  'cpassword' => 'required|min:6|max:20'
];

$messages = [
  'email:required'     => 'Please provide Email',
  'email:email'        => 'Please provide a valid Email',
  'password:required'  => 'Please provide a password',
  'password:min'       => 'Please password a minimum 6 characters',
  'password:max'       => 'Please password a maximum 20 characters',
  'cpassword:required' => 'Please retype to confirm password',
  'cpassword:min'      => 'Please confirm password a minimum 6 characters',
  'cpassword:max'      => 'Please confirm password a maximum 20 characters'
];

$errors = Input::validate($rules, $messages);

if($errors){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => $errors[0]
    ]
  ];
  goto RESPONSE;
}

$email = Input::get('email');
$pass  = Input::get('password');
$cpass = Input::get('cpassword');

$user = User::where('email', $email)->first();

if(!$user) {
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'No user found with this email.'
    ]
  ];
  goto RESPONSE;
}

if($pass != $cpass){
  $events = [
    'message.show' => [
      'type'  => 'error',
      'text'  => 'New password & confirm password not matched'
    ]
  ];
  goto RESPONSE;
}

$redirect = SITE_URL.'/admin/confirm/?_auth='.Crypto::encrypt($user->id).'&_token='.Crypto::hash($pass);
$userData = [
  'name' 			=> $user->name,
  'email' 		=> $user->email,
  'password'  => $pass,
  'site_url'  => SITE_URL,
  'redirect'  => $redirect
];
$to = $user->email;
$subject = 'Reset Password | TimelyCare';
$body = Email::message('forgot-password', $userData);
Email::send($to, $subject, $body);

$events = [
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Please check confirmation mail to activate the new password'
  ],
  'modal.target.close' => 'forgot-password'
];

RESPONSE:
return [
  'data'   => $data,
  'events' => $events
];
