<?php

$data   = false;
$events = false;


$doctor_count       = User::where('type', 0)->where('trash', 0)->get();
$nurse_count        = User::where('type', 1)->where('trash', 0)->get();
$group_count        = User::where('type', 2)->get();
$department_count   = Description::where('type', 0)->get();


$dateLabel = [];
$leaveDoc  = [];
$leaveNur  = [];
$shiftDoc  = [];
$shiftNur  = [];

$curdate  = date('Y-m-d');
$lastdate = date('Y-m-d', strtotime('+6 day'));

for($d=0;$d<=6;$d++) {
  $date    = date('Y-m-d', strtotime('+'.$d.' day'));
  $docData = Analytics::where('type', 0)->where('date', $date)->first();
  $nurData = Analytics::where('type', 1)->where('date', $date)->first();

  $dateLabel[] = date('d-M', strtotime('+'.$d.' day'));
  $leaveDoc[]  = !empty($docData->leave_request) ? $docData->leave_request : 0;
  $leaveNur[]  = !empty($nurData->leave_request) ? $nurData->leave_request : 0;
  $shiftDoc[]  = !empty($docData->shift_request) ? $docData->shift_request : 0;
  $shiftNur[]  = !empty($nurData->shift_request) ? $nurData->shift_request : 0;
}



$data   = [
  'doctor_count'      => !empty($doctor_count) ? count($doctor_count) : 0,
  'nurse_count'       => !empty($nurse_count) ? count($nurse_count) : 0,
  'group_count'       => !empty($group_count) ? count($group_count) : 0,
  'department_count'  => !empty($department_count) ? count($department_count) : 0,
  'leave_requests'    => [
    'label'  => $dateLabel,
    'doctor' => $leaveDoc,
    'nurse'  => $leaveNur
  ],
  'shift_requests'    => [
    'label'  => $dateLabel,
    'doctor' => $shiftDoc,
    'nurse'  => $shiftNur
  ]
];

$events = [
  'chart.init' => false
];

return [
  'data'   => $data,
  'events' => $events
];
