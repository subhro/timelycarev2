<?php

$data   = false;
$events = false;

$users = User::where('type', Input::get('type'))
->where('trash', 0)
->where(function($query){
    if(Input::has('q')) {
       $query->where('name', 'like', Input::get('q').'%');
    }
})
->get();

$data   = [
  'users' => $users
];

return [
  'data'   => $data,
  'events' => $events
];
