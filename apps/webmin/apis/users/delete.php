<?php

$data   = false;
$events = false;

$id = Input::get('id', 0);

$user = User::find($id);
$user->delete();

DB::table('groups')->where('user_id', $id)->delete();
DB::table('schedules')->where('user_id', $id)->delete();
DB::table('availabilities')->where('user_id', $id)->delete();
DB::table('shifts_requests')->where('user_id', $id)->delete();
DB::table('chats')->where('sender_id', $id)->orWhere('receiver_id', $id)->delete();
DB::table('favorites')->where('user_id', $id)->orWhere('favorite_id', $id)->delete();

$events = [
  'records.delete' => [
    'key'   => Input::get('key'),
    'index' => Input::get('index')
  ],
  'message.show' => [
    'type'  => 'success',
    'text'  => 'Deleted successfully'
  ]
];

return [
  'data'   => $data,
  'events' => $events
];
