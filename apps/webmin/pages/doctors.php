@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Doctors'])
@include('shared.sidebar', ['page'=>'doctors'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('modals.doctors.add-doctor')
@include('modals.schedule.user-schedule')

@section('styles')
<style>
.show-password {
  position: absolute;
  right: 5px;
  top: 6px;
  font-size: 18px;
  color: #2A7CEE;
  cursor: pointer;
}
</style>
@endsection

@section('content')

	<br><br>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			@include('common.search') <br>
		</div>

		<div class="col-xs-6 col-sm-2">
			<dropdown class="fluid"
			value="{{search.department}}"
			options="{{options.doctor_description}}"
			on-select="page.refresh" />
		</div>

		<div class="col-xs-6 col-sm-4 end-xs">
			<button class="ui btn-default button" on-click="modal.open" target="add-doctor">
				<i class="fa fa-plus"></i> Add Doctor
			</button>
			@include('common.pagination')
		</div>
	</div>

	<br><br>

	<div class="scroll-content">

	<div class="row">
		<div class="col-xs-12">
			<table class="ui unstackable striped table">
				<thead>
					<tr>
						<td>
							<b>Name</b> <br>
						</td>
						<td><b>Phone no.</b></td>
						<td><b>Email</b></td>
						<td><b>Department</b></td>
						<td class="action">
							<b>Actions</b>
						</td>
					</tr>
				</thead>

				<tbody>
					{{#each doctors:index}}
						<tr>
							<td>{{name}}</td>
							<td>{{phone}}</td>
							<td>{{email}}</td>
							<td>{{description.name}}</td>
							<td class="action">
								{{#if deleted != index}}
									<a class="ui green label" on-click="doctor.schedule" href="#">
										<i class="fa fa-calendar"></i>
									</a>
									<a class="ui blue label" on-click="doctor.edit" href="#">
										<i class="fa fa-pencil"></i>
									</a>
									<a class="ui red label" on-click="@this.set('deleted',index)">
										<i class="fa fa-trash"></i>
									</a>
								{{else}}
									<a class="ui green label" on-click="doctor.delete"> Yes</a>
									<a class="ui yellow label" on-click="@this.set('deleted',-1)"> No</a>
								{{/if}}
							</td>
						</tr>
					{{else}}
						<tr>
							<td colspan="5">No records found.</td>
						</tr>
					{{/each}}
				</tbody>
			</table>

		</div>

	</div>
	</div>

@endsection



@section('scripts')
@parent
<script type="text/javascript">

	Data.set('$url', '/admin/api/doctors/doctors');

	Event.on('page.init', function() {
		Api.get('/admin/api/doctors/doctors').send();
		Api.get('/admin/api/options/description').params({ type : 0 }).send();
    Api.get('/admin/api/options/shifts').params({ type : 0 }).send();
	});

	Event.on('doctor.schedule', function(ctx) {
		var doctor = ctx.get();
		Data.set('$user', doctor);
		var params = {
	      uid  : doctor.id,
	      type : doctor.type
	  }
	  Api.get('/admin/api/users/schedule').params(params).send();
		Tag.get('user-schedule').fire('modal.open');
	});

	Event.on('doctor.edit', function(self) {
		var params = {
			desc_id : self.get('description_id')
		};
		Api.get('/admin/api/options/shifts').params(params).send(function(){
			Data.set('$doctor', self.get());
			Tag.get('add-doctor').fire('modal.open');
		});
	});

	Event.on('doctor.delete', function(doctor) {
		var index = doctor.get('index');
		var params = {
			id 		: doctor.get('id'),
			index : index,
			key   : 'doctors'
		};
		Api.post('/admin/api/users/delete').params(params).send();
	});

</script>
@endsection
