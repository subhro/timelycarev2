@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Dashboard'])
@include('shared.sidebar', ['page'=>'dashboard'])

@include('widgets.modal')
@include('widgets.message')


@section('scripts')
@parent
<script src="/assets/js/chart.js" charset="utf-8"></script>
@endsection

@section('content')
  <br><br>

  <div class="row count-section">
    <div class="col-xs-3">
      <div class="ui segments">
        <span class="pull-right">
          <i class="fa fa-user-md"></i>
        </span>
        <div class="page-header">{{doctor_count}}</div>
        <div class="label">Doctors</div>
        <a href="/admin/doctors" class="view-details">
          View Details <i class="fa fa-arrow-right"></i>
        </a>
      </div>
      <br>
    </div>
    <div class="col-xs-3">
      <div class="ui segments">
        <span class="pull-right">
          <i class="fa fa-stethoscope"></i>
        </span>
        <div class="page-header">{{nurse_count}}</div>
        <div class="label">Nurses</div>
        <a href="/admin/nurses" class="view-details">
          View Details <i class="fa fa-arrow-right"></i>
        </a>
      </div>
      <br>
    </div>
    <div class="col-xs-3">
      <div class="ui segments">
        <span class="pull-right">
          <i class="fa fa-group"></i>
        </span>
        <div class="page-header">{{group_count}}</div>
        <div class="label">Groups</div>
        <a href="/admin/settings/groups" class="view-details">
          View Details <i class="fa fa-arrow-right"></i>
        </a>
      </div>
      <br>
    </div>
    <div class="col-xs-3">
      <div class="ui segments">
        <span class="pull-right">
          <i class="fa fa-building-o"></i>
        </span>
        <div class="page-header">{{department_count}}</div>
        <div class="label">Departments</div>
        <a href="/admin/settings/basic" class="view-details">
          View Details <i class="fa fa-arrow-right"></i>
        </a>
      </div>
      <br>
    </div>
    <br><br>
  </div>

  <div class="row">
    <div class="col-xs-6">
      <canvas id="leavesChart" style="height:250px;"></canvas>
      <h4 class="header text-center">Leave Requests</h4>
    </div>
    <div class="col-xs-6">
      <canvas id="shiftChart" style="height:250px;"></canvas>
      <h4 class="header text-center">Shift Requests</h4>
    </div>
  </div>

@endsection

@section('scripts')
<script>

  Data.set('$url', '/admin/api/dashboard/home');

  Event.on('page.init', function() {
    Api.get('/admin/api/dashboard/home').send();
  });

  // Event.on('chart.init', function() {
  //   setTimeout(function() {
  //     Event.fire('chart.initdelay');
  //   }, 2000);
  // });

  Event.on('chart.init', function() {
    var leaveData = Data.get('leave_requests');
    var leavesChart = document.getElementById('leavesChart').getContext('2d');
    var doctorsLeaves = {
      label: 'Doctors - Leave Requests',
      data: leaveData.doctor,
      lineTension: 0.3,
      fill: false,
      borderColor: 'red',
      backgroundColor: 'transparent',
      pointBorderColor: 'red',
      pointBackgroundColor: 'lightgreen',
      pointRadius: 5,
      pointHoverRadius: 10,
      pointHitRadius: 15,
      pointBorderWidth: 2,
      pointStyle: 'rect'
    };
    var nursesLeaves = {
      label: 'Nurses - Leave Requests',
      data: leaveData.nurse,
      lineTension: 0.3,
      fill: false,
      borderColor: 'purple',
      backgroundColor: 'transparent',
      pointBorderColor: 'purple',
      pointBackgroundColor: 'lightgreen',
      pointRadius: 5,
      pointHoverRadius: 10,
      pointHitRadius: 15,
      pointBorderWidth: 2
    };
    var leavesData  = new Chart(leavesChart, {
        type: 'line',
        data: {
            labels: leaveData.label,
            datasets: [doctorsLeaves, nursesLeaves],
        }
    });

    var shiftData = Data.get('shift_requests');
    var shiftChart = document.getElementById('shiftChart').getContext('2d');
    var doctorsShifts = {
      label: 'Doctors - Shift Requests',
      data: shiftData.doctor,
      lineTension: 0.3,
      fill: false,
      borderColor: 'blue',
      backgroundColor: 'transparent',
      pointBorderColor: 'blue',
      pointBackgroundColor: 'lightgreen',
      pointRadius: 5,
      pointHoverRadius: 10,
      pointHitRadius: 15,
      pointBorderWidth: 2,
      pointStyle: 'rect'
    };
    var nursesShifts = {
      label: 'Nurses - Shift Requests',
      data: shiftData.nurse,
      lineTension: 0.3,
      fill: false,
      borderColor: 'orange',
      backgroundColor: 'transparent',
      pointBorderColor: 'orange',
      pointBackgroundColor: 'lightblue',
      pointRadius: 5,
      pointHoverRadius: 10,
      pointHitRadius: 15,
      pointBorderWidth: 2
    };
    var leavesData  = new Chart(shiftChart, {
        type: 'line',
        data: {
            labels: shiftData.label,
            datasets: [doctorsShifts, nursesShifts],
        }
    });
	});

</script>
@endsection
