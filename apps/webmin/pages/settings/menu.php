<div class="ui top attached tabular menu">

	  <a class="item @if($page=='basics') active @endif" href="/admin/settings/basic">Basics</a>

	  <a class="item @if($page=='shifts') active @endif" href="/admin/settings/shifts">Shifts</a>

		<a class="item @if($page=='rules') active @endif" href="/admin/settings/rules">Rules</a>

		<a class="item @if($page=='groups') active @endif" href="/admin/settings/groups">Groups</a>

</div>
