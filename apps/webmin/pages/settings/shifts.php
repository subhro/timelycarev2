@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Settings - Shifts'])
@include('shared.sidebar', ['page'=>'settings'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('modals.doctors.add-doctor')

@section('content')

<div class="scroll-content">

	@include('settings.menu', ['page'=>'shifts'])

	<div class="ui bottom attached horizontal segments">
		@include('settings.shifts-user', ['key'=>'doctor'])
		@include('settings.shifts-user', ['key'=>'nurse'])
	</div>


</div>


@endsection



@section('scripts')
@parent
<script type="text/javascript">

Data.set('$doctor', {});
Data.set('$nurse', {});

Event.on('page.init', function() {
	Api.get('/admin/api/settings/shifts').send();
	Api.get('/admin/api/options/description').params({ type : 0 }).send();
	Api.get('/admin/api/options/description').params({ type : 1 }).send();
	Api.get('/admin/api/options/color-picker').send();
});

Event.on('shift.edit', function(self){
	if(self.get('type') == 0){
		key = '$doctor';
	}else{
		key = '$nurse';
	}
	Data.set(key, self.get());
});

Event.on('shift.save', function(e){
	var key = e.node.getAttribute('shift-type');
	var params = Data.get(key);
	if(key == '$doctor'){
		params.type = 0;
	}else{
		params.type = 1;
	}
	Api.post('/admin/api/settings/shifts-save').params(params).send(function(res){
		if(!res.err) {
			Data.set(key, {});
			Api.get('/admin/api/settings/shifts').send();
		}
	});
});

Event.on('shift.delete', function(shift){
	var params = shift.get();
	Api.post('/admin/api/settings/shifts-delete').params(params).send();
	Api.get('/admin/api/settings/shifts').send();
});

Event.on('description.select', function(){
	var params = {};
	params.doctor_description_id = Data.get('$doctor.description_id');
	params.nurse_description_id = Data.get('$nurse.description_id');
	Api.get('/admin/api/settings/shifts').params(params).send();
});





</script>
@endsection
