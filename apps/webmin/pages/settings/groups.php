@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Settings - Groups'])
@include('shared.sidebar', ['page'=>'settings'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('modals.doctors.doctors-group')
@include('modals.nurses.nurses-group')

@section('content')

  <div class="scroll-content">

		@include('settings.menu', ['page'=>'groups'])

  	<div class="ui bottom attached horizontal segments">
  		<div class="ui segment">
        <div class="row">
          <div class="col-xs-6">
            <h3 class="page-header">Doctors Groups</h3>
          </div>
          <div class="col-xs-6 end-xs">
            <button class="ui blue button" on-click="create.doctors_group">Create Doctors Group</button>
          </div>
        </div><br>
        <div class="table-container">
    			<table class="ui unstackable striped table">
    				<thead>
    					<tr>
    						<td><b>Name</b></td>
								<td><b>Description</b></td>
								<td class="action">
									<b>Actions</b>
								</td>
    					</tr>
    				</thead>
    				<tbody>
            	{{#each doctors_group:index}}
	    					<tr>
	    						<td>{{name}}</td>
									<td>{{description_text}}</td>
	    						<td class="action">
	    							{{#if $deleted_doctors_group != index}}
	    								<a class="ui red label" on-click="@this.set('$deleted_doctors_group',index)">
	    									<i class="fa fa-trash"></i>
	    								</a>
	    							{{else}}
	    								<a class="ui green label" on-click="doctors_group.delete"> Yes</a>
	    								<a class="ui yellow label" on-click="@this.set('$deleted_doctors_group',-1)"> No</a>
	    							{{/if}}
	    						</td>
	    					</tr>
							{{else}}
								<tr>
									<td colspan="3">No Group found.</td>
								</tr>
	  					{{/each}}
    				</tbody>
    			</table>
    		</div>
  		</div>

      <div class="ui segment">
        <div class="row">
          <div class="col-xs-6">
            <h3 class="page-header">Nurses Groups</h3>
          </div>
          <div class="col-xs-6 end-xs">
            <button class="ui blue button" on-click="create.nurses_group">Create Nurses Group</button>
          </div>
        </div><br>
        <div class="table-container">
    			<table class="ui unstackable striped table">
    				<thead>
    					<tr>
    						<td><b>Name</b></td>
								<td><b>Description</b></td>
								<td class="action">
									<b>Actions</b>
								</td>
    					</tr>
    				</thead>
    				<tbody>
              {{#each nurses_group:index}}
	    					<tr>
	    						<td>{{name}}</td>
									<td>{{description_text}}</td>
	    						<td class="action">
	    							{{#if $deleted_nurses_group != index}}
	    								<a class="ui red label" on-click="@this.set('$deleted_nurses_group',index)">
	    									<i class="fa fa-trash"></i>
	    								</a>
	    							{{else}}
	    								<a class="ui green label" on-click="nurses_group.delete"> Yes</a>
	    								<a class="ui yellow label" on-click="@this.set('$deleted_nurses_group',-1)"> No</a>
	    							{{/if}}
	    						</td>
	    					</tr>
							{{else}}
								<tr>
									<td colspan="3">No Group found.</td>
								</tr>
    					{{/each}}
    				</tbody>
    			</table>
    		</div>
  		</div>
  	</div>
	</div>

@endsection



@section('scripts')
@parent
<script type="text/javascript">

	Data.set('$url', '/admin/api/settings/groups');

	Event.on('page.init', function(){
		Api.get('/admin/api/settings/groups').send(function() {
      Api.get('/admin/api/doctors/available').send(function() {
				Api.get('/admin/api/nurses/available').send();
			});
    });
	});

  Event.on('create.doctors_group', function() {
    Tag.get('doctors-group').fire('modal.open');
  });

	Event.on('doctors_group.delete', function(doctors_group){
		var index = doctors_group.get('index');
		var params = {
			id 		: doctors_group.get('id'),
			index : index,
			key   : 'doctor'
		};
		Event.fire('page.refresh');
		Api.post('/admin/api/group/delete').params(params).send();
	});

	Event.on('create.nurses_group', function() {
    Tag.get('nurses-group').fire('modal.open');
  });

	Event.on('nurses_group.delete', function(nurses_group){
		var index = nurses_group.get('index');
		var params = {
			id 		: nurses_group.get('id'),
			index : index,
			key   : 'nurse'
		};
		Event.fire('page.refresh');
		Api.post('/admin/api/group/delete').params(params).send();
	});

</script>
@endsection
