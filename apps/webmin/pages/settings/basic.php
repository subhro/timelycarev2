@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Settings - Basics'])
@include('shared.sidebar', ['page'=>'settings'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('modals.doctors.add-doctor')

@section('content')

	<div class="scroll-content">

		@include('settings.menu', ['page'=>'basics'])

  	<div class="ui bottom attached horizontal segments">
  		<div class="ui segment">
        <div class="ui secondary segment">
          <h2>Doctors Departments</h2>
        </div>
        <div class="ui segment form">
          <div class="row">
            <div class="col-sm-9">
              <input placeholder="Departments" type="text" value="{{$department.name}}" />
            </div>
            <div class="col-sm-3 end-xs">
              <button class="ui blue button" on-click="add.departments">
        				<i class="fa fa-save"></i> Add
        			</button>
            </div>
          </div>
        </div>
        <div class="table-container">
    			<table class="ui unstackable striped table">
    				<thead>
    					<tr>
    						<td><b>Name</b></td>
								<td class="action">
									<b>Actions</b>
								</td>
    					</tr>
    				</thead>
    				<tbody>
    					{{#each departments:index}}
	    					<tr>
	    						<td>{{name}}</td>
									<td class="action">

										{{#if description[0] != id}}
											<a class="ui gray label" on-click="description.update">
												<i class="fa fa-check"></i>
											</a>
										{{else}}
											<a class="ui green label">
												<i class="fa fa-check"></i>
											</a>
										{{/if}}

										{{#if $deleted_left != index}}
											<a class="ui red label" on-click="@this.set('$deleted_left',index)">
												<i class="fa fa-trash"></i>
											</a>
										{{else}}
												<a class="ui green label" on-click="delete.departments"> Yes</a>
												<a class="ui yellow label" on-click="@this.set('$deleted_left',-1)"> No</a>
										{{/if}}


									</td>
	    					</tr>
							{{else}}
								<tr>
									<td colspan="2">No Department found.</td>
								</tr>
    					{{/each}}
    				</tbody>
    			</table>
    		</div>
  		</div>
      <div class="ui segment">
        <div class="ui secondary segment">
          <h2>Nurses Locations</h2>
        </div>
        <div class="ui segment form">
          <div class="row">
            <div class="col-sm-9">
              <input placeholder="Locations" type="text" value="{{$location.name}}" />
            </div>
            <div class="col-sm-3 end-xs">
              <button class="ui blue button" on-click="add.locations">
        				<i class="fa fa-save"></i> Add
        			</button>
            </div>
          </div>
        </div>
        <div class="table-container">
    			<table class="ui unstackable striped table">
    				<thead>
    					<tr>
    						<td><b>Name</b></td>
								<td class="action">
									<b>Actions</b>
								</td>
    					</tr>
    				</thead>
    				<tbody>
    					{{#each locations:index}}
	    					<tr>
	    						<td>{{name}}</td>
									<td class="action">

										{{#if description[1] != id}}
											<a class="ui gray label" on-click="description.update">
												<i class="fa fa-check"></i>
											</a>
										{{else}}
											<a class="ui green label">
												<i class="fa fa-check"></i>
											</a>
										{{/if}}

										{{#if $deleted_right != index}}
											<a class="ui red label" on-click="@this.set('$deleted_right',index)">
												<i class="fa fa-trash"></i>
											</a>
										{{else}}
											<a class="ui green label" on-click="delete.locations"> Yes</a>
											<a class="ui yellow label" on-click="@this.set('$deleted_right',-1)"> No</a>
										{{/if}}
									</td>
	    					</tr>
							{{else}}
								<tr>
									<td colspan="2">No Location found.</td>
								</tr>
    					{{/each}}
    				</tbody>
    			</table>
    		</div>
  		</div>
  	</div>
	</div>


@endsection



@section('scripts')
@parent
<script type="text/javascript">

	Event.on('page.init', function() {
		Api.get('/admin/api/settings/basic').send();
	});

	Event.on('description.update', function(e) {
		var update = e.get();
		var description = Data.get('description');
		description[update.type] = update.id;
		Data.set('description', description);

		Api.post('/admin/api/descriptions/update')
			.params({description:description})
			.send();
	});

	Event.on('add.departments', function(e) {
    var params = Data.get('$department');
		params.type = 0;
    Api.post('/admin/api/descriptions/add').params(params).send(function() {
      Data.set('$department', {});
      Api.get('/admin/api/settings/basic').send();
    });
	});

  Event.on('add.locations', function(e) {
    var params = Data.get('$location');
		params.type = 1;
    Api.post('/admin/api/descriptions/add').params(params).send(function() {
      Data.set('$location', {});
      Api.get('/admin/api/settings/basic').send();
    });
	});

	Event.on('delete.departments', function(department){
		var index = department.get('index');
		var params = {
			id 		: department.get('id'),
			index : index,
			key   : 'department'
		};
		Api.post('/admin/api/descriptions/delete').params(params).send(function() {
			Api.get('/admin/api/settings/basic').send();
		});
	});

	Event.on('delete.locations', function(location){
		var index = location.get('index');
		var params = {
			id 		: location.get('id'),
			index : index,
			key   : 'location'
		};
		Api.post('/admin/api/descriptions/delete').params(params).send(function() {
			Api.get('/admin/api/settings/basic').send();
		});
	});

</script>
@endsection
