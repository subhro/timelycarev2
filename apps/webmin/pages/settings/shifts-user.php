
<div class="ui segment">
	<br>
	<div class="ui secondary">
		<div class="row">
			<div class="col-xs-6">
				<h2>
					@if($key=='doctor')
						Doctors
					@else
						Nurses
					@endif
					Shifts
				</h2>
			</div>
			<div class="col-xs-6">
				<dropdown class="fluid"
				value="{{$<%$key%>.description_id}}"
				options="{{options.<%$key%>_description}}"
				on-select="description.select" />
			</div>
		</div>
	</div>
	<br>
	<div class="ui segment form">
		<div class="row">
			<div class="col-sm-4">
				<input placeholder="Shift Name" type="text" value="{{$<%$key%>.name}}" />
			</div>
			<div class="col-sm-4">
				<input placeholder="Shift Start" type="time" pattern="^([0-1]?[0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])?$" value="{{$<%$key%>.start_time}}" />
			</div>
			<div class="col-sm-4">
				<input placeholder="Shift End" type="time" pattern="^([0-1]?[0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])?$" value="{{$<%$key%>.end_time}}" />
			</div>
			<div class="col-sm-5"><br>
				<dropdown-color value="{{$<%$key%>.shift_color}}" options="{{options.colorpicker}}" />
			</div>
			<div class="col-sm-4"><br>
				<input placeholder="Staff Allowed" type="number" maxlength="3" minlength="1" value="{{$<%$key%>.staff_count}}" />
			</div>
			<div class="col-sm-3 end-xs"><br>
				<input type="hidden" value="{{$<%$key%>.id}}" />
				<button class="ui blue button" on-click="shift.save" shift-type="$<%$key%>">
					<i class="fa fa-save"></i>
					{{#if $<%$key%>.id}}
						Update
					{{else}}
						Add
					{{/if}}
				</button>
			</div>
		</div>
	</div>
	<div class="table-container">
		<table class="ui unstackable striped table">
			<thead>
				<tr>
					<td><b>Name</b></td>
					<td><b>Timing</b></td>
					<td><b>Staff</b></td>
					<td><b>Color</b></td>
					<td class="action">
						<b>Actions</b>
					</td>
				</tr>
			</thead>
			<tbody>
				{{#each <%$key%>s_shifts:index}}
					<tr>
						<td>{{name}}</td>
						<td>
							<b>Start: </b>{{display_start_time}}
							<b>End: </b>{{display_end_time}}
						</td>
						<td>{{staff_count}}</td>
						<td> <span style="color:#{{color_ft}};background-color:#{{color_bg}};text-align:center;padding:5px; border-radius:5px;">
							color</span>
						</td>
						<td class="action">
							<a class="ui label" on-click="shift.edit">
								<i class="fa fa-pencil"></i>
							</a>
							{{#if $<%$key%>_delete != index}}
								<a class="ui red label" on-click="@this.set('$<%$key%>_delete', index)">
									<i class="fa fa-trash"></i>
								</a>
							{{else}}
								<a class="ui green label" on-click="shift.delete"> Yes</a>
								<a class="ui yellow label" on-click="@this.set('$<%$key%>_delete', -1)"> No</a>
							{{/if}}
						</td>
					</tr>
				{{else}}
					<tr>
						<td colspan="4">No Shift found.</td>
					</tr>
				{{/each}}
			</tbody>
		</table>
	</div>
</div>
