@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Settings - Rules'])
@include('shared.sidebar', ['page'=>'settings'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')


@section('content')

	<div class="scroll-content">

		@include('settings.menu', ['page'=>'rules'])

		<div class="ui bottom attached horizontal segments">
			@include('settings.rules-user', ['key'=>'doctor'])
			@include('settings.rules-user', ['key'=>'nurse'])
		</div>

	</div>


@endsection



@section('scripts')
@parent
<script type="text/javascript">

Data.set('$nurse', {});
Data.set('$doctor', {});

Event.on('page.init', function() {
	Api.get('/admin/api/settings/rules').send();
	Api.get('/admin/api/options/description').params({ type : 0 }).send();
	Api.get('/admin/api/options/description').params({ type : 1 }).send();
});


Event.on('rules.save', function() {

});

Event.on('description.select', function(){
	var params = {};
	params.doctor_description_id = Data.get('$doctor.description_id');
	params.nurse_description_id = Data.get('$nurse.description_id');
	Api.get('/admin/api/settings/rules').params(params).send();
	//Api.get('/admin/api/settings/shifts').params(params).send();
});



</script>
@endsection
