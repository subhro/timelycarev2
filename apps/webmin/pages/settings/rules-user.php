
<div class="ui segment">
	<br>
	<div class="ui secondary">
		<div class="row">
			<div class="col-xs-6">
				<h2>
					@if($key=='doctor')
						Doctors
					@else
						Nurses
					@endif
					Rules
				</h2>
			</div>
			<div class="col-xs-6">
				<dropdown class="fluid"
				value="{{$<%$key%>.description_id}}"
				options="{{options.<%$key%>_description}}"
				on-select="description.select" />
			</div>
		</div>

		<div class="row ui segment form">
			<div class="col-sm-6">
					<h4 style="margin-top:10px;">Consecutive Day</h4>
			</div>
			<div class="col-sm-6">
				<input type="number" value="{{<%$key%>.rules.consecutive_day}}" />
				<br><br>
			</div>
			<div class="col-sm-6">
					<h4 style="margin-top:10px;">Working Day / Month</h4>
			</div>
			<div class="col-sm-6">
				<input type="number" value="{{<%$key%>.rules.per_month}}" />
				<br><br>
			</div>

			<div class="col-sm-12 end-xs">
				<button class="ui blue button" on-click="rules .save">Save</button>
			</div>
		</div>
	</div>
</div>
