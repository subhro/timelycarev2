@extends('shared.layout')
@include('shared.events')

@include('widgets.message')

@section('styles')
<style>
#content {
  padding: 0;
}
.login-body {
  background: linear-gradient(rgba(255, 255, 255, 0.96), rgba(255, 255, 255, 0.96)), url(/assets/imgs/bg.jpg) repeat;
  background-size: 720px;
}
.login-panel .login-header{
  text-align: center;
}
.login-logo {
  margin-top: 22px;
  margin-bottom: 16px;
}
.login-text {
  display: inline-block;
  font-size: 38px;
  margin-bottom: 18px;
  top: -20px;
  position: relative;
  color: #ffffffd1;
}
.show-password {
  position: absolute;
  right: 5px;
  top: 9px;
  font-size: 18px;
  color: #2A7CEE;
  cursor: pointer;
}
</style>
@endsection

@section('content')

<div class="row center-xs">
  <div class="col-xs-12 col-sm-8  col-md-5 login-panel">

    <div class="login-header">
      <img class="login-logo" src="/assets/imgs/logo-white.png" />
    </div>

    <div class="ui segment">
      <div class="ui fluid labeled input" style="margin:35px 0px;font-size:22px;line-height:28px;color:#3f813f;">
        Your Password has been saved successfully. Please click to continue Login.
      </div>

      <div class="one fields">
        <div class="welcome-bottom text-center">
          <a class="ui primary button btn-default" href="/admin">GO TO LOGIN</a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section('scripts')
@parent
<script type="text/javascript">

  Event.on('page.init', function(){
    var params = {
      'id'   : window.location.search.substring(1).split('=')[1],
      'pass' : window.location.search.substring(1).split('=')[2]
    };
    Api.hideLoading = true;
    Api.get('/admin/api/confirm').params(params).send();
  });

</script>
@endsection
