@section('segments')
@parent

<modal id="send-notification" size="large">

  <div class="header">Send Notification</div>
	<div class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="ui form">
          <div class="two fields">
            <div class="field">
              <label>Email Address</label>
              <input placeholder="Email" type="text" value="{{$user.email}}" />
            </div>
            <div class="field">
              <label>Subject</label>
              <input placeholder="Subject" type="text" value="{{$user.subject}}" />
            </div>
          </div>
          <div class="field">
            <label>Message</label>
            <textarea>{{$user.message}}</textarea>
          </div>
        </div>
      </div>
    </div>
	</div>
	<div class="actions">
    <button class="ui blue button" on-click="send.notification">Send</button>
		<button class="ui red button" on-click="modal.close" target="send-notification">Close</button>
	</div>

</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('send.notification', function(){
    var params = Data.get('$user');
    Api.post('/admin/api/notification-send').params(params).send(function() {
       Data.set('$user', {});
       Tag.get('send-notification').fire('modal.close');
    });
  });

</script>
@endsection
