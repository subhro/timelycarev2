@section('segments')
@parent
<modal id="forgot-password" size="small">

  <div class="header">Forgot Password?</div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.user}}">
    <div class="row">
      <div class="col-sm-12">
        <label style="margin:5px 0;display:inline-block;">Enter Registered Email</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Email Address" type="email" value="{{$reset.email}}" on-enter="user.resetpassowrd" />
        </div>
      </div>
      <div class="col-sm-12">
        <label style="margin:7px 0;display:inline-block;">New Password</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="New Password" type="password" value="{{$reset.password}}" on-enter="user.resetpassowrd" />
        </div>
      </div>
      <div class="col-sm-12">
        <label style="margin:7px 0;display:inline-block;">Confirm Password</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Confirm Password" type="password" value="{{$reset.cpassword}}" on-enter="user.resetpassowrd" />
        </div>
      </div>
    </div>
	</div>
	<div class="actions">
		<button class="ui red button" on-click="modal.close" target="forgot-password">Close</button>
    <button class="ui blue button" on-click="user.resetpassowrd">Send</button>
	</div>



</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('forgot-password.close', function() {
    Data.set('$reset', {});
  });

  Event.on('user.resetpassowrd', function() {
    var params = Data.get('$reset');
    Api.post('/admin/api/forgot-password').params(params).send();
  });

</script>
@endsection
