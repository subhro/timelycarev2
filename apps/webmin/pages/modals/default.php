@section('segments')
@parent

<modal id="default-modal" size="large">

  <div class="header">Default Modal</div>
	<div class="content">
    <div class="row">
      <div class="col-sm-12">
        <p>Default Modal Content</p>
      </div>
    </div>
	</div>
	<div class="actions">
		<button class="ui red button" on-click="modal.close" target="#">Close</button>
	</div>

</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">



</script>
@endsection
