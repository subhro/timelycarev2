@section('segments')
@parent
<modal id="add-doctor" size="small">

  <div class="header">
    {{#if $doctor.id}}
      Edit
    {{else}}
      Add
    {{/if}} Doctor
  </div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.doc}}">
    <div class="row">
      <div class="col-sm-6">
        <label>First Name</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="First Name" type="text" value="{{$doctor.first_name}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Last Name</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Last Name" type="text" value="{{$doctor.last_name}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Department</label>
        <div class="ui form select">
          <dropdown value="{{$doctor.description_id}}" options="{{options.doctor_description}}" on-select="load.shift" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Shift</label>
        <div class="ui form select">
          <dropdown-multiple value="{{$doctor.shift_ids}}" options="{{options.shifts}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Phone</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Phone" type="text" value="{{$doctor.phone}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Email</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Email Address" type="email" value="{{$doctor.email}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Priority</label>
        <div class="ui fluid input">
          <dropdown value="{{$doctor.priority}}" options="{{options.doctors_priority}}"></dropdown>
        </div>
      </div>

      <div class="col-sm-6">
        <label>Password</label>
        <div class="ui fluid input">
          <input class="form-control" id="doctorPassword" placeholder="Password" type="password" value="{{$doctor.password}}" />
          <a class="show-password" on-click="show.password" on-enter="show.password" title="Show Password">
            <i class="fa fa-eye"></i>
          </a>
        </div>
      </div>
    </div>
	</div>
	<div class="actions">
    <button class="ui blue button" on-click="doctor.save">Save</button>
		<button class="ui red button" on-click="modal.close" target="add-doctor">Close</button>
	</div>



</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Data.set('options.doctors_priority', [
    { value: 1, label: 'Part Time' },
    { value: 2, label: 'Full Time' }
  ]);

  Event.on('load.shift', function() {
		var desc_id = Data.get('$doctor.description_id');
		Api.get('/admin/api/options/shifts').params({desc_id : desc_id}).send();
  });

  Event.on('add-doctor.close', function() {
    Data.set('$doctor', {});
  });

  Event.on('doctor.save', function() {
    var params = Data.get('$doctor');
    Api.post('/admin/api/doctors/save').params(params).send(function(){
      Event.fire('page.refresh');
    });
  });

  Event.on('show.password', function() {
    var field = document.getElementById('doctorPassword');
    field.type = (field.type === 'password') ? 'text' : 'password';
  });

</script>
@endsection
