@section('segments')
@parent
<modal id="doctors-group" size="large">

  <div class="header">Doctor Group</div>
	<div class="content">
    <div class="row">
      <div class="col-xs-4">
        <div class="ui fluid input">
          <input value="{{$group.name}}" placeholder="Enter Group Name" type="text" />
        </div>
      </div>
      <div class="col-xs-8">
        <div class="ui fluid input">
          <input value="{{$group.description}}" placeholder="Enter Description" type="text" />
        </div>
      </div>
      <div class="col-xs-12"><br>
        <div class="ui fluid input">
          <input type="text" value="{{$doctor.q}}" on-enter="doctor.search"  placeholder="Search by Name">
        </div>
      </div>
    </div> <br>
    <div class="row">
      <div class="col-xs-8">
        <label class="page-label">Available Doctors</label>
        <div class="table-container">
          <table class="ui unstackable striped table">
            <thead>
              <tr>
                <td><b>Name</b></td>
                <td><b>Department</b></td>
                <td class="action">
                  <b>Actions</b>
                </td>
              </tr>
            </thead>
            <tbody>
              {{#each doctors:index}}
                {{#if !_hide}}
                  <tr>
                    <td>{{name}}</td>
                    <td>{{description}}</td>
                    <td class="action">
                      <a class="ui blue circular label" on-click="doctor.select">
                        <i class="fa fa-plus"></i>
                      </a>
                    </td>
                  </tr>
                {{/if}}
              {{/each}}
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-xs-4">
        <label class="page-label">Selected Doctors</label>
        <div class="table-container">
          <table class="ui unstackable striped table">
            <thead>
              <tr>
                <td><b>Name</b></td>
                <td class="action">
                  <b>Actions</b>
                </td>
              </tr>
            </thead>
            <tbody>
              {{#each $group.selected:index}}
                <tr>
                  <td>{{name}}</td>
                  <td class="action">
                    <a class="ui red circular label" on-click="doctor.remove">
                      <i class="fa fa-times"></i>
                    </a>
                  </td>
                </tr>
              {{/each}}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
	<div class="actions">
    <button class="ui blue button" on-click="doctor_group.save">Save</button>
		<button class="ui red button" on-click="modal.close" target="doctors-group">Close</button>
	</div>

</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('doctors-group.open', function() {
    Api.get('/admin/api/doctors/available').send();
    Data.set('$doctor', {});
    Data.set('$group', {});
  });

  Event.on('doctor.search', function() {
    Api.get('/admin/api/doctors/available').params({ q : Data.get('$doctor.q') }).send();
  });

  Event.on('doctor.select', function(ctx) {
    var doctor = ctx.get();
    var index = ctx.get('index');
    doctor._hide = true;
    doctor._index = index;
    Data.set('doctors.'+index+'._hide', false);
    Data.set('doctors.'+index+'._hide', true);
    Data.push('$group.selected', doctor);
  });

  Event.on('doctor.remove', function(ctx) {
    var index = ctx.get('index');
    var _index = ctx.get('_index');
    Data.set('doctors.'+_index+'._hide', false);
    Data.splice('$group.selected', index, 1);
  });

  Event.on('doctor_group.save', function(){
    var selected  = Data.get('$group.selected');
    var doctorIds = [];
    for(var i in selected) {
      doctorIds.push(selected[i].id);
    }
    var params = {
      name        : Data.get('$group.name'),
      description : Data.get('$group.description'),
      userIds     : doctorIds,
      key         : 'doctor'
    }
    Api.post('/admin/api/group/save').params(params).send(function() {
      Event.fire('page.refresh');
    });
  });

</script>
@endsection
