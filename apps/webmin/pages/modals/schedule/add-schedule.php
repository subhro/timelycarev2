@section('segments')
@parent
<modal id="add-schedule" size="small">
  <div class="header">
    <h2>Add Schedule</h2>
  </div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.doc}}">
    <div class="row">
      <div class="col-sm-6">
        <label>Name</label>
        <div class="ui form select">
          <dropdown value="{{$listUser.name}}" options="{{options.details}}"></dropdown>
        </div>
      </div>

      <div class="col-sm-6">
        <label>Shift</label>
        <div class="ui form select">
          <dropdown value="{{$listUser.shift}}" options="{{options.shifts}}"></dropdown>
        </div>
      </div>
    </div>
	</div>
	<div class="actions">
    <button class="ui blue button" on-click="schedule.save">Save</button>
		<button class="ui red button" on-click="modal.close" target="add-schedule">Close</button>
	</div>
</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('schedule.save', function(){
    var schedule = Data.get('$listUser');
    var usertype = Data.get('$schedule.usertype');
    var date = Data.get('$user');
    var params = {
      user_id   : schedule.name,
      type      : usertype,
      date      : date.date,
      shift_id  : schedule.shift
    };
    Api.post('/admin/api/schedules/save').params(params).send(function() {
      Api.get('/admin/api/schedules/data').params({date : date.date, type : usertype}).send(function() {
        Api.get('/admin/api/schedules/view').params({date : date.date, type : usertype}).send(function() {
          Data.set('$listUser', {});
        });
      });
    });
  });

</script>
@endsection
