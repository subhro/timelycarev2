@section('segments')
@parent
<modal id="user-schedule" size="small">

  <div class="header">Schedule</div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.doc}}">
    <div class="row modal-calendar">
      <div class="control col-sm-6">
    		{{#if prev_year_month}}
    			<button class="ui primary button btn-default" on-click="schedule.prev">
    				<i class="fa fa-arrow-circle-left"></i>
    			</button>
    		{{else}}
    			<button class="ui primary button btn-default" disabled>
    				<i class="fa fa-arrow-circle-left"></i>
    			</button>
    		{{/if}}

    		{{#if next_year_month}}
    			<button class="ui primary button btn-default" on-click="schedule.next">
    				<i class="fa fa-arrow-circle-right"></i>
    			</button>
    		{{else}}
    			<button class="ui primary button btn-default" disabled>
    				<i class="fa fa-arrow-circle-right"></i>
    			</button>
    		{{/if}}
    	</div>

    	<div class="month col-sm-6 end-sm calendar-text">
    		<h2>{{year_month_text}}</h2>
    	</div>

    	<div class="col-sm-12">
    		<table class="ui celled table calendar-table">
    			<thead>
    				<tr>
    					<th>SUN</th>
    					<th>MON</th>
    					<th>TUE</th>
    					<th>WED</th>
    					<th>THU</th>
    					<th>FRI</th>
    					<th>SAT</th>
    				</tr>
    			</thead>
    			<tbody>
    				{{#each calendar}}
    					<tr>
    						{{#each this}}
    							<td style="color:#{{color_ft}};background:#{{color_bg}};">
    								{{text}}
    							</td>
    						{{/each}}
    					</tr>
    				{{/each}}
    			</tbody>
    		</table><br>
    	</div>

      {{#each shifts}}
        <div class="col-sm">
          <h4 style="color:#{{color_ft}};background-color:#{{color_bg}};text-align:center;font-family:Constantia;padding:5px 0;">
            {{name}}
          </h4>
        </div>
      {{/each}}

    </div>
	</div>
	<div class="actions">
		<button class="ui red button" on-click="modal.close" target="user-schedule">Close</button>
	</div>



</modal>

@endsection

@section('scripts')
@parent
<script>

	Event.on('schedule.prev', function() {
		var date = Data.get('prev_year_month');
    var user = Data.get('$user');
		var params = {
	      uid        : user.id,
	      type       : user.type,
        year_month : date
	  }
		Api.get('/admin/api/users/schedule').params(params).send();
	});

	Event.on('schedule.next', function() {
		var date = Data.get('next_year_month');
    var user = Data.get('$user');
		var params = {
	      uid        : user.id,
	      type       : user.type,
        year_month : date
	  }
		Api.get('/admin/api/users/schedule').params(params).send();
	});

</script>
@endsection
