@section('modals')
@parent

<div id="vsat-result" class="modal modal-sm">
    <a href="#modals-sizes" class="modal-overlay" aria-label="Close"></a>
    <div class="modal-container" role="document">
        <div class="modal-header">
            <a href="#modals-sizes" class="btn btn-clear float-right" aria-label="Close"></a>
            <div class="modal-title h5">VSAT Rusult</div>
        </div>
        <div class="modal-body">
            <div id="vsat-result-form" class="content">
                <input type="hidden" name="phase" value="0">
                <div class="form-group">
                    <label class="form-label">Your Name</label>
                    <input class="form-input" type="text" name="name" placeholder="Full Name">
                </div>
                <div class="form-group">
                    <label class="form-label" for="input-example-7">Your Phone</label>
                    <input class="form-input" type="text" name="phone" placeholder="Phone Number">
                </div>
                <div class="d-none toast text-center"></div>
            </div>
            <div id="vsat-result-form-success-1" class="d-none">
                <div class="row">
                    <div class="col-xs-6">
                        Phase-I
                    </div>
                    <div class="col-xs-6 end-xs">
                        <a class="btn btn-success" href="/uploads/vsat-results-2018-phase-1.pdf" download>Download</a>
                    </div>
                </div>
            </div>
            <div id="vsat-result-form-success-2" class="d-none">
                <div class="row">
                    <div class="col-xs-6">
                        Phase-II
                    </div>
                    <div class="col-xs-6 end-xs">
                        <a class="btn btn-success" href="/uploads/vsat-results-2018-phase-2.pdf" download>Download</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" onclick="vsatResult(this)">Submit</button>
            <a href="#modals-sizes" class="btn btn-link" aria-label="Close">Close</a>
        </div>
    </div>
</div>

@endsection

@section('script')
@parent

<script type="text/javascript">

function vsatResult(button){
    var formId = '#vsat-result-form';
    $(button).addClass('loading');
    message(formId, false);
    var params = input(formId);

    $.post('/api/vsat-results', params, function(response){
        console.log(response);
        if(response.type=='success'){
            $('#vsat-result .modal-footer').addClass('d-none');
            update(formId, formId+'-success-'+response.phase);
        }else{
            message(formId, response);
        }

        $(button).removeClass('loading');
    });

}

</script>


@endsection
