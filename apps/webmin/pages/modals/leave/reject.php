@section('segments')
@parent
<modal id="leave-reject" size="small">
  <div class="header">
    <h2>Reject Request</h2>
  </div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.doc}}">
    <div class="row">
      <div class="col-sm-12">
        <p>Are you sure you want to Reject this request?</p>
      </div>
    </div>
	</div>
	<div class="actions">
    <button class="ui blue button" on-click="request.reject">Reject</button>
		<button class="ui red button" on-click="modal.close" target="leave-reject">Close</button>
	</div>
</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('request.reject', function(){
    var reqId = Data.get('$reqId');
    var params = {
      id   : reqId,
      type : 2
    };
    Event.fire('page.refresh');
    Api.post('/admin/api/requests/save').params(params).send();
  });

</script>
@endsection
