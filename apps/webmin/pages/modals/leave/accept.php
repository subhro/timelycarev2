@section('segments')
@parent
<modal id="leave-accept" size="small">
  <div class="header">
    <h2>Accept Request</h2>
  </div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.doc}}">
    <div class="row">
      <div class="col-sm-12">
        <p>Are you sure you want to Accept this request?</p>
      </div>
    </div>
	</div>
	<div class="actions">
    <button class="ui blue button" on-click="request.accept">Accept</button>
		<button class="ui red button" on-click="modal.close" target="leave-accept">Close</button>
	</div>
</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('request.accept', function(){
    var reqId = Data.get('$reqId');
    var params = {
      id   : reqId,
      type : 1
    };
    Event.fire('page.refresh');
    Api.post('/admin/api/requests/save').params(params).send();
  });

</script>
@endsection
