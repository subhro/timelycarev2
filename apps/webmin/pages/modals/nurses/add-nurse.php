@section('segments')
@parent
<modal id="add-nurse" size="small">

  <div class="header">
    {{#if $nurse.id}}
      Edit
    {{else}}
      Add
    {{/if}} Nurse
  </div>
	<div class="content">
		<img class="ui centered large image" src="{{modal.doc}}">
    <div class="row">
      <div class="col-sm-6">
        <label>First Name</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="First Name" type="text" value="{{$nurse.first_name}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Last Name</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Last Name" type="text" value="{{$nurse.last_name}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Location</label>
        <div class="ui form select">
          <dropdown value="{{$nurse.description_id}}" options="{{options.nurse_description}}" on-select="load.shift" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Shift</label>
        <div class="ui form select">
          <dropdown-multiple value="{{$nurse.shift_ids}}" options="{{options.shifts}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Phone</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Phone" type="text" value="{{$nurse.phone}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Email</label>
        <div class="ui fluid input">
          <input class="form-control" placeholder="Email Address" type="email" value="{{$nurse.email}}" />
        </div>
      </div>

      <div class="col-sm-6">
        <label>Priority</label>
        <div class="ui fluid input">
          <dropdown value="{{$nurse.priority}}" options="{{options.nurses_priority}}"></dropdown>
        </div>
      </div>

      <div class="col-sm-6">
        <label>Password</label>
        <div class="ui fluid input">
          <input class="form-control" id="nursePassword" placeholder="Password" type="password" value="{{$nurse.password}}" />
          <a class="show-password" on-click="show.password" on-enter="show.password" title="Show Password">
            <i class="fa fa-eye"></i>
          </a>
        </div>
      </div>
    </div>
	</div>
	<div class="actions">
    <button class="ui blue button" on-click="nurse.save">Save</button>
		<button class="ui red button" on-click="modal.close" target="add-nurse">Close</button>
	</div>



</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Data.set('options.nurses_priority', [
    { value: 1, label: 'Part Time' },
    { value: 2, label: 'Full Time' }
  ]);

  Event.on('load.shift', function() {
		var desc_id = Data.get('$nurse.description_id');
		Api.get('/admin/api/options/shifts').params({desc_id : desc_id}).send();
  });

  Event.on('add-nurse.close', function() {
    Data.set('$nurse', {});
  });

  Event.on('nurse.save', function() {
    var params = Data.get('$nurse');
    Api.post('/admin/api/nurses/save').params(params).send(function(){
        Event.fire('page.refresh');
    });
  });

  Event.on('show.password', function() {
    var field = document.getElementById('nursePassword');
    field.type = (field.type === 'password') ? 'text' : 'password';
  });

</script>
@endsection
