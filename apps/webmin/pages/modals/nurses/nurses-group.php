@section('segments')
@parent
<modal id="nurses-group" size="large">

  <div class="header">Nurse Group</div>
	<div class="content">
    <div class="row">
      <div class="col-xs-4">
        <div class="ui fluid input">
          <input value="{{$group.name}}" placeholder="Enter Group Name" type="text" />
        </div>
      </div>
      <div class="col-xs-8">
        <div class="ui fluid input">
          <input value="{{$group.description}}" placeholder="Enter Description" type="text" />
        </div>
      </div>
      <div class="col-xs-12"><br>
        <div class="ui fluid input">
          <input type="text" value="{{$nurse.q}}" on-enter="nurse.search"  placeholder="Search by Name">
        </div>
      </div>
    </div> <br>
    <div class="row">
      <div class="col-xs-8">
        <label class="page-label">Available Nurses</label>
        <div class="table-container">
          <table class="ui unstackable striped table">
            <thead>
              <tr>
                <td><b>Name</b></td>
                <td><b>Department</b></td>
                <td class="action">
                  <b>Actions</b>
                </td>
              </tr>
            </thead>
            <tbody>
              {{#each nurses:index}}
                {{#if !_hide}}
                  <tr>
                    <td>{{name}}</td>
                    <td>{{description}}</td>
                    <td class="action">
                      <a class="ui blue circular label" on-click="nurse.select">
                        <i class="fa fa-plus"></i>
                      </a>
                    </td>
                  </tr>
                {{/if}}
              {{/each}}
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-xs-4">
        <label class="page-label">Selected Nurses</label>
        <div class="table-container">
          <table class="ui unstackable striped table">
            <thead>
              <tr>
                <td><b>Name</b></td>
                <td class="action">
                  <b>Actions</b>
                </td>
              </tr>
            </thead>
            <tbody>
              {{#each $group.selected:index}}
                <tr>
                  <td>{{name}}</td>
                  <td class="action">
                    <a class="ui red circular label" on-click="nurse.remove">
                      <i class="fa fa-times"></i>
                    </a>
                  </td>
                </tr>
              {{/each}}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
	<div class="actions">
    <button class="ui blue button" on-click="nurse_group.save">Save</button>
		<button class="ui red button" on-click="modal.close" target="nurses-group">Close</button>
	</div>

</modal>

@endsection




@section('scripts')
@parent
<script type="text/javascript">

  Event.on('nurses-group.open', function() {
    Api.get('/admin/api/nurses/available').send();
    Data.set('$nurse', {});
    Data.set('$group', {});
  });

  Event.on('nurse.search', function() {
    Api.get('/admin/api/nurses/available').params({ q : Data.get('$nurse.q') }).send();
  });

  Event.on('nurse.select', function(ctx) {
    var nurse = ctx.get();
    var index = ctx.get('index');
    nurse._hide = true;
    nurse._index = index;
    Data.set('nurses.'+index+'._hide', false);
    Data.set('nurses.'+index+'._hide', true);
    Data.push('$group.selected', nurse);
  });

  Event.on('nurse.remove', function(ctx) {
    var index = ctx.get('index');
    var _index = ctx.get('_index');
    Data.set('nurses.'+_index+'._hide', false);
    Data.splice('$group.selected', index, 1);
  });

  Event.on('nurse_group.save', function(){
    var selected  = Data.get('$group.selected');
    var nurseIds = [];
    for(var i in selected) {
      nurseIds.push(selected[i].id);
    }
    var params = {
      name        : Data.get('$group.name'),
      description : Data.get('$group.description'),
      userIds     : nurseIds,
      key         : 'nurse'
    }
    Api.post('/admin/api/group/save').params(params).send(function() {
      Event.fire('page.refresh');
    });
  });

</script>
@endsection
