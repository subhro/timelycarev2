@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Schedules - Request'])
@include('shared.sidebar', ['page'=>'schedules'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@section('content')

	<div class="scroll-content">
		<div class="ui top attached tabular menu">
			<a class="item" href="/admin/schedules/view">Calendar</a>
			<a class="item" href="/admin/schedules/list">List</a>
			<a class="item active">Request</a>
		</div><br>

		<div class="row">
			<div class="col-xs-12 col-sm-10">
				@include('common.search') <br>
			</div>
			<div class="col-xs-12 col-sm-2 end-xs">
				@include('common.pagination')
			</div>
			<div class="col-xs-12 col-sm-3">
				<dropdown value="{{$filter.usertype}}" options="{{options.usertype}}" on-select="user.select"  />
			</div>
			<div class="col-xs-12 col-sm-3">
				<dropdown value="{{$filter.year_month}}" options="{{options.year_month}}" on-select="year_month.select"  />
			</div>
			<div class="col-xs-12 col-sm-3">
				<dropdown value="{{$filter.working}}" options="{{options.working}}" on-select="working.select"  />
			</div>
			<div class="col-xs-12 col-sm-3">
				<dropdown value="{{$filter.description}}" options="{{options.description}}" on-select="description.select"  />
			</div>
		</div>
		<br><br>

		<div class="ui bottom attached">
			<div class="row">
				<div class="col-xs-12">
					<table class="ui unstackable striped table">
						<thead>
    					<tr>
    						<td><b>Name</b></td>
								<td><b>Designation</b></td>
								<td><b>Department / Location</b></td>
    						<td><b>Date</b></td>
    						<td><b>Working</b></td>
    					</tr>
    				</thead>

    				<tbody>
  						{{#each requests:index}}
  							<tr>
  								<td>{{users.name}}</td>
  								{{#if users.type==0}}
  									<td>Doctor</td>
  								{{elseif users.type==1}}
  									<td>Nurse</td>
  								{{/if}}
									<td>{{users.description.name}}</td>
									<td>{{date}}</td>
									{{#if working==1}}
  									<td class="success">Yes</td>
  								{{else}}
  									<td class="danger">No</td>
  								{{/if}}
  							</tr>
							{{else}}
								<tr>
									<td colspan="4">No Shift request found.</td>
								</tr>
  						{{/each}}
    				</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection



@section('scripts')
@parent
<script type="text/javascript">

	Data.set('$filter.usertype', 0);
	Data.set('$url', '/admin/api/schedules/request');

	Data.set('options.working', [
		{ value: 1, label: 'Yes' },
		{ value: 2, label: 'No' },
  ]);

	Data.set('options.usertype', [
    { value: 0, label: 'Doctor' },
    { value: 1, label: 'Nurse' }
  ]);

	Event.on('page.init', function() {
		Api.get('/admin/api/options/year-month').send();
		Api.get('/admin/api/options/description').params({ type : 0 }).send();
		Api.get('/admin/api/schedules/request').send();
	});

	Event.on('user.select', function() {
		var params = Data.get('$filter');
		Api.get('/admin/api/options/description').params({ type : params.usertype }).send();
		Api.get('/admin/api/schedules/request').params(params).send();
	});

	Event.on('year_month.select', function() {
		var params = Data.get('$filter');
		Api.get('/admin/api/schedules/request').params(params).send();
	});

	Event.on('working.select', function() {
		var params = Data.get('$filter');
		Api.get('/admin/api/schedules/request').params(params).send();
	});

	Event.on('description.select', function() {
		var params = Data.get('$filter');
		Api.get('/admin/api/schedules/request').params(params).send();
	});

</script>
@endsection
