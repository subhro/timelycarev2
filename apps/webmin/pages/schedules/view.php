@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Schedules - Calendar'])
@include('shared.sidebar', ['page'=>'schedules'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('modals.schedule.add-schedule')

@section('content')

	<div class="scroll-content">

		<div class="ui top attached tabular menu">
			<a class="item active">Calendar</a>
			<a class="item" href="/admin/schedules/list">List</a>
			<a class="item" href="/admin/schedules/request">Request</a>
		</div>

		<div class="row calendar">
			<div class="month col-sm-2">
				<div class="ui form select">
		      @include('options.usertype', ['key' => '$schedule.usertype'])
		    </div>
			</div>
			<div class="month col-sm-3">
				<h2 class="text-center">{{year_month_text}}</h2>
			</div>
			<div class="control col-sm-2 end-sm">
				{{#if prev_year_month}}
					<button class="ui primary button btn-default" on-click="schedule.prev">
						<i class="fa fa-arrow-circle-left"></i>
					</button>
				{{else}}
					<button class="ui primary button btn-default" disabled>
						<i class="fa fa-arrow-circle-left"></i>
					</button>
				{{/if}}

				{{#if next_year_month}}
					<button class="ui primary button btn-default" on-click="schedule.next">
						<i class="fa fa-arrow-circle-right"></i>
					</button>
				{{else}}
					<button class="ui primary button btn-default" disabled>
						<i class="fa fa-arrow-circle-right"></i>
					</button>
				{{/if}}
			</div>

			<div class="month col-sm-3 schedule-date">
				<h2>{{fulldate}}</h2>
			</div>
			<div class="month col-sm-2 end-sm">
				<button class="ui btn-default button" role="{{formateddate}}" on-click="add-schedule">
					<i class="fa fa-plus"></i>
				</button>
			</div>
			<div class="col-sm-7">
				<table class="ui celled table date-calendar">
					<thead>
						<tr>
							<th>SUN</th>
							<th>MON</th>
							<th>TUE</th>
							<th>WED</th>
							<th>THU</th>
							<th>FRI</th>
							<th>SAT</th>
						</tr>
					</thead>
					<tbody>
						{{#each calendar}}
							<tr>
								{{#each this}}
									{{#if exactdate==formateddate}}
										<td dir="{{exactdate}}" class="date-selected" on-click="schedule.data">
											<span>{{userCount}}</span>
											{{text}}
										</td>
									{{else}}
										<td dir="{{exactdate}}" class="{{todayselect}}" on-click="schedule.data">
											<span>{{userCount}}</span>
											{{text}}
										</td>
									{{/if}}
								{{/each}}
							</tr>
						{{/each}}
					</tbody>
				</table>
			</div>
			<div class="col-sm-5">
				<table class="ui celled table data-schedule">
					<thead>
						<tr>
							<th class="text-center">Name</th>
							<th class="text-center">Schedule</th>
							<th class="text-right">
								<b>Action</b>
							</th>
						</tr>
					</thead>
					<tbody>
						{{#each schedule:index}}
							<tr>
								<td>{{users.name}}</td>
								<td>{{shift_start}} to {{shift_end}}</td>
								<td class="text-right">
									{{#if $deleted_schedule != index}}
										<a class="ui red label pull-right" on-click="@this.set('$deleted_schedule',index)">
											<i class="fa fa-trash"></i>
										</a>
									{{else}}
										<a class="ui green label" on-click="schedule.delete"> Yes</a>
										<a class="ui yellow label" on-click="@this.set('$deleted_schedule',-1)"> No</a>
									{{/if}}
								</td>
							</tr>
						{{else}}
							<tr>
								<td colspan="2">No schedule found for the day.</td>
							</tr>
						{{/each}}
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection



@section('scripts')
<script>

	Data.set('$type', 0);

	Event.on('page.init', function() {
		var type = Data.get('$type');
		Api.get('/admin/api/schedules/view').params({type : type}).send();
		Api.get('/admin/api/users/lists').params({type : type}).send();
		Api.get('/admin/api/options/shifts').params({type : type}).send();
		Api.get('/admin/api/options/user').params({type : type}).send();
	});

	Event.on('type.select', function() {
		var user = Data.get('$schedule');
		Data.set('$type', user.usertype);
		Event.fire('page.init');
  });

	Event.on('schedule.prev', function() {
		var user = Data.get('$schedule');
		var date = Data.get('prev_year_month');
		Api.get('/admin/api/schedules/view').params({year_month : date, type : user.usertype}).send();
	});

	Event.on('schedule.next', function() {
		var user = Data.get('$schedule');
		var date = Data.get('next_year_month');
		Api.get('/admin/api/schedules/view').params({year_month : date, type : user.usertype}).send();
	});

	Event.on('add-schedule', function(e) {
		var date = e.node.attrs('role');
		Data.set('$user', {date: date});
		Tag.get('add-schedule').fire('modal.open');
	});

	Event.on('schedule.data', function(e) {
		var date = e.node.attrs('dir');
		var type = Data.get('$schedule');
		var usertype = (type) ? type.usertype : 0;
		var params = {
			date 			 : date,
			type 			 : usertype
		}
		Api.get('/admin/api/schedules/view').params(params).send();
	});

	Event.on('schedule.delete', function(schedule) {
		var index = schedule.get('index');
		var user = Data.get('$schedule');
		var params = {
			id 		: schedule.get('id'),
			index : index,
			key   : 'schedule'
		};
		Event.fire('page.refresh');
		Api.post('/admin/api/schedules/delete').params(params).send(function() {
			Api.get('/admin/api/schedules/view').params({type : user.usertype}).send();
		});
	});

</script>
@endsection
