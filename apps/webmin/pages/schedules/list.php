@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Schedules - List'])
@include('shared.sidebar', ['page'=>'schedules'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@section('content')

	<div class="scroll-content">

		<div class="ui top attached tabular menu">
			<a class="item" href="/admin/schedules/view">Calendar</a>
			<a class="item active">List</a>
			<a class="item" href="/admin/schedules/request">Request</a>
			<button style="position:absolute;right:20px;" class="ui button green" on-click="schedule.calculate">Calculate</button>
		</div>

		<div class="row calendar">
			<div class="month col-sm-6">
				<h2>{{year_month_text}}</h2>
			</div>
			<div class="month col-sm-2">
				<div class="ui form select">
					<dropdown class="fluid"
		 	 			value="{{search.type}}"
		 	 			options="{{options.type}}"
		 	 			on-select="type.select" />
		    </div>
			</div>
			<div class="month col-sm-2">
				<div class="ui form select">
					<dropdown class="fluid"
		 	 			value="{{search.desc_id}}"
		 	 			options="{{options.description}}"
		 	 			on-select="description.select" />
		    </div>
			</div>

			<div class="control col-sm-2 end-sm">
				{{#if prev_year_month}}
					<button class="ui primary button btn-default" on-click="schedule.prev">
						<i class="fa fa-arrow-circle-left"></i>
					</button>
				{{else}}
					<button class="ui primary button btn-default" disabled>
						<i class="fa fa-arrow-circle-left"></i>
					</button>
				{{/if}}

				{{#if next_year_month}}
					<button class="ui primary button btn-default" on-click="schedule.next">
						<i class="fa fa-arrow-circle-right"></i>
					</button>
				{{else}}
					<button class="ui primary button btn-default" disabled>
						<i class="fa fa-arrow-circle-right"></i>
					</button>
				{{/if}}
			</div>

			<div class="month col-sm-12 list-calendar">
				<table class="ui celled table">
					<thead>
						<tr>
							<th class="text-center">Name</th>
							{{#each calendar}}
								<th class-weekends="{{weekends}}">
									{{text}}
									<br>
									<small style="font-size:10px">{{week}}</small>
								</th>
							{{/each}}
						</tr>
					</thead>
					<tbody>
						{{#each users:uindex}}
							<tr>
								<td>
									{{#if !image}}
										<img src="/assets/imgs/avatar-user.png" />
									{{/if}}
									{{#if image}}
										<img src="{{image}}" />
									{{/if}}
									<span>{{name}}</span>
									<small>{{description.name}}</small>
								</td>
								{{#each schedule:sindex}}
									<td class-loading="{{loading}}" class-weekends="{{calendar[sindex].weekends}}" on-click="schedule.toggle" style="background-color:#{{shift_color_bg}};color:#{{shift_color_ft}};">.</td>
								{{/each}}
							</tr>
						{{/each}}
					</tbody>
				</table>
			</div>

			{{#each shifts}}
        <div class="col-sm">
          <h4 style="color:#{{color_ft}};background-color:#{{color_bg}};text-align:center;font-family:Constantia;padding:5px 0;">
            {{name}}
          </h4>
        </div>
      {{/each}}
		</div>
	</div>

@endsection



@section('scripts')
<script>


	Data.set('search', {});
	Data.set('$schedule.reset', true);

	Event.on('page.init', function() {
		var params = Data.get('search');
		Api.get('/admin/api/schedules/list').params(params).send();
		Api.get('/admin/api/options/type').params(params).send();
		Api.get('/admin/api/options/description').params(params).send();
	});

	Event.on('type.select', function() {
		Data.set('search.desc_id', 0);
    var params = Data.get('search');
		Api.get('/admin/api/schedules/list').params(params).send();
		Api.get('/admin/api/options/description').params(params).send();
  });

	Event.on('description.select', function() {
		var params = Data.get('search');
		Api.get('/admin/api/schedules/list').params(params).send();
  });

	Event.on('schedule.prev', function() {
		var params = Data.get('search');
		params.year_month = Data.get('prev_year_month');
		Api.get('/admin/api/schedules/list').params(params).send();
	});

	Event.on('schedule.next', function() {
		var params = Data.get('search');
		params.year_month = Data.get('next_year_month');
		Api.get('/admin/api/schedules/list').params(params).send();
	});

	Event.on('schedule.calculate', function() {
		var params = 	{
			type : Data.get('search.type'),
			year_month : Data.get('year_month'),
			desc_id : Data.get('search.desc_id'),
			reset: Data.get('$schedule.reset')
		};
		Api.get('/admin/api/schedules/calculate').params(params).send();
		Api.get('/admin/api/schedules/list').params(params).send();
		Data.set('$schedule.reset', false);
	});


	Event.on('schedule.toggle', function(self){


			var data = self.get();
			data.loading = true;
			self.set(data);

			var id = self.get('id');
			var day  = self.get('sindex') + 1;
			var year_month = Data.get('year_month');
			var date = year_month + '-' + day;
			var user_id = self.get('user_id');


			var shiftIndex = Data.get('shiftIndex');
			var shiftCount = Data.get('shifts').length;

			if(!id) shiftIndex = 0;
			if(!shiftIndex) shiftIndex = 0;

			if(shiftIndex > shiftCount){
				shiftIndex = 0;
			}

			var shift_id  = Data.get('shifts.'+shiftIndex+'.id');
			if(!shift_id) shift_id = 0;

			Data.set('shiftIndex', shiftIndex+1);

			var params = {
				id : id,
				date : date,
				year_month : year_month,
				shift_id : shift_id,
				user_id : user_id,
			}
			Api.$hideLoading = true;
			Api.get('/admin/api/schedules/toggle').params(params).send(function(res){
				self.set('id', res.id);
				self.set('shift_color_ft', res.shift_color_ft);
				self.set('shift_color_bg', res.shift_color_bg);
				self.set('loading', false);
			});



	});






</script>
@endsection
