@section('scripts')
@parent
<script type="text/javascript">

	Event.on('type.select', function() {
    var user = Data.get('$usertype');
    Api.get('/admin/api/users/lists').params({type : user.usertype}).send();
  });

	Event.on('user.search', function() {
		var user = Data.get('$usertype');
		var params = {
			q 	 : Data.get('$user.q'),
			type : user.usertype
		};
    Api.get('/admin/api/users/available').params(params).send();
  });

	Event.on('user.select', function(ctx) {
    var user = ctx.get();
    var index = ctx.get('index');
    user._hide = true;
    user._index = index;
    Data.set('users.'+index+'._hide', false);
    Data.set('users.'+index+'._hide', true);
    Data.push('$user.selected', user);
  });

  Event.on('user.remove', function(ctx) {
    var index = ctx.get('index');
    var user  = ctx.get();
		var _index = -1;
		var users = Data.get('users');
		for(var i in users) {
			if(users[i].id == user.id) {
				_index = i;
			}
		}
		if(_index > -1) {
    	Data.set('users.'+_index+'._hide', false);
		} else {
			user._hide = false;
			Data.push('users', user);
		}
    Data.splice('$user.selected', index, 1);
  });

</script>
@endsection
