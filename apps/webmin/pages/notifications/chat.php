@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Notifications - Chat'])
@include('shared.sidebar', ['page'=>'notifications'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('notifications.event')
@include('modals.notifications.send')

@section('content')

	<div class="scroll-content">

		@include('notifications.menu', ['page'=>'chat'])

		<br>
		<div class="ui bottom attached form">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
	        <div class="ui fluid input">
	          <input type="text" value="{{$user.q}}" on-enter="user.search"  placeholder="Search by Name">
	        </div>
	      </div>
				<div class="month col-xs-12 col-sm-6">
					<div class="ui form select">
			      @include('options.usertype', ['key' => '$usertype.usertype'])
			    </div>
				</div>
	    </div><br>
	    <div class="row">
	      <div class="col-xs-8">
	        <label class="page-label">
						Available {{#if type==0}} Doctors {{elseif type==1}} Nurses {{/if}}
					</label>
	        <div class="table-container">
	          <table class="ui unstackable striped table">
	            <thead>
	              <tr>
	                <td><b>Name</b></td>
	                <td><b>Department</b></td>
	                <td class="action">
	                  <b>Actions</b>
	                </td>
	              </tr>
	            </thead>
	            <tbody>
	              {{#each users:index}}
	                {{#if !_hide}}
	                  <tr>
	                    <td>{{name}}</td>
	                    <td>{{description.name}}</td>
	                    <td class="action">
	                      <a class="ui blue circular label" on-click="user.select">
	                        <i class="fa fa-plus"></i>
	                      </a>
	                    </td>
	                  </tr>
	                {{/if}}
								{{else}}
									<tr>
										<td colspan="3">No records found.</td>
									</tr>
	              {{/each}}
	            </tbody>
	          </table>
	        </div>
	      </div>

	      <div class="col-xs-4">
	        <label class="page-label">Selected {{#if type==0}} Doctors {{elseif type==1}} Nurses {{/if}}</label>
	        <div class="table-container">
	          <table class="ui unstackable striped table">
	            <thead>
	              <tr>
	                <td><b>Name</b></td>
	                <td class="action">
	                  <b>Actions</b>
	                </td>
	              </tr>
	            </thead>
	            <tbody>
	              {{#each $user.selected:index}}
	                <tr>
	                  <td>{{name}}</td>
	                  <td class="action">
	                    <a class="ui red circular label" on-click="user.remove">
	                      <i class="fa fa-times"></i>
	                    </a>
	                  </td>
	                </tr>
	              {{/each}}
	            </tbody>
	          </table>
	        </div>
	      </div>
	    </div>
			<div class="row">
				<div class="col-xs-12"><br>
	        <div class="field">
						<label>Message: </label>
	          <textarea placeholder="Enter Message">{{$chat.message}}</textarea>
	        </div>
	      </div>
			</div>
			<div class="actions pull-right"><br>
		    <button class="ui blue button" on-click="send.chat">Send</button><br><br>
			</div>
		</div>
	</div>

@endsection



@section('scripts')
@parent
<script type="text/javascript">

	Data.set('$url', '/admin/api/notifications/chat');

	Event.on('page.init', function() {
		Api.get('/admin/api/notifications/chat').send();
	});

	Event.on('send.chat', function(user) {
		var user 		 = Data.get('$usertype');
		var type 		 = (user) ? user.usertype : 'error';
		var selected = Data.get('$user.selected');
    var userIds  = [];
    for(var i in selected) {
      userIds.push(selected[i].id);
    }
    var params = {
      message	: Data.get('$chat.message'),
      userIds : userIds,
      type    : type
    }
    Api.post('/admin/api/notifications/send-chat').params(params).send(function() {
			Data.set('$usertype', {});
      Data.set('$chat', {});
			Data.set('$user', {});
    });
	});

</script>
@endsection
