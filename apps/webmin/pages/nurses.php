@extends('shared.layout')
@include('shared.events')
@include('shared.header', ['page'=>'Nurses'])
@include('shared.sidebar', ['page'=>'nurses'])

@include('widgets.modal')
@include('widgets.message')
@include('widgets.dropdown')

@include('modals.nurses.add-nurse')
@include('modals.schedule.user-schedule')

@section('styles')
<style>
.show-password {
  position: absolute;
  right: 5px;
  top: 6px;
  font-size: 18px;
  color: #2A7CEE;
  cursor: pointer;
}
</style>
@endsection

@section('content')

	<br><br>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			@include('common.search') <br>
		</div>

		<div class="col-xs-6 col-sm-2">
			<dropdown class="fluid"
			value="{{search.location}}"
			options="{{options.nurse_description}}"
			on-select="page.refresh" />
		</div>

		<div class="col-xs-6 col-sm-4 end-xs">
			<button class="ui btn-default button" on-click="modal.open" target="add-nurse">
				<i class="fa fa-plus"></i> Add Nurse
			</button>
			@include('common.pagination')
		</div>
	</div>

	<br><br>

	<div class="scroll-content">

	<div class="row">
		<div class="col-xs-12">
			<table class="ui unstackable striped table">
				<thead>
					<tr>
						<td>
							<b>Name</b> <br>
						</td>
						<td><b>Phone no.</b></td>
						<td><b>Email</b></td>
						<td><b>Location</b></td>
						<td class="action">
							<b>Actions</b>
						</td>
					</tr>
				</thead>

				<tbody>
					{{#each nurses:index}}
						<tr>
							<td>{{name}}</td>
							<td>{{phone}}</td>
							<td>{{email}}</td>
							<td>{{description.name}}</td>
							<td class="action">
								{{#if deleted != index}}
									<a class="ui green label" on-click="nurse.schedule" href="#">
										<i class="fa fa-calendar"></i>
									</a>
									<a class="ui blue label" on-click="nurse.edit" href="#">
										<i class="fa fa-pencil"></i>
									</a>
									<a class="ui red label" on-click="@this.set('deleted',index)">
										<i class="fa fa-trash"></i>
									</a>
								{{else}}
									<a class="ui green label" on-click="nurse.delete"> Yes</a>
									<a class="ui yellow label" on-click="@this.set('deleted',-1)"> No</a>
								{{/if}}
							</td>
						</tr>
					{{else}}
						<tr>
							<td colspan="5">No records found.</td>
						</tr>
					{{/each}}
				</tbody>
			</table>

		</div>

	</div>
	</div>

@endsection



@section('scripts')
@parent
<script type="text/javascript">

	Data.set('$url', '/admin/api/nurses/nurses');

	Event.on('page.init', function() {
		Api.get('/admin/api/nurses/nurses').send();
		Api.get('/admin/api/options/description').params({ type : 1 }).send();
    Api.get('/admin/api/options/shifts').params({ type : 1 }).send();
	});

	Event.on('nurse.schedule', function(ctx) {
		var nurse = ctx.get();
		Data.set('$user', nurse);
		var params = {
	      uid  : nurse.id,
	      type : nurse.type
	  }
	  Api.get('/admin/api/users/schedule').params(params).send();
		Tag.get('user-schedule').fire('modal.open');
	});

	Event.on('nurse.edit', function(self) {
		var params = {
			desc_id : self.get('description_id')
		};
		Api.get('/admin/api/options/shifts').params(params).send(function(){
			Data.set('$nurse', self.get());
			Tag.get('add-nurse').fire('modal.open');
		});
	});

	Event.on('nurse.delete', function(nurse) {
		var index = nurse.get('index');
		var params = {
			id 		: nurse.get('id'),
			index : index,
			key   : 'nurses'
		};
		Api.post('/admin/api/users/delete').params(params).send();
	});

</script>
@endsection
