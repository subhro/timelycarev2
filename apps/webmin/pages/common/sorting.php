<button class="ui button transparent" on-click="page.sort" column="<%$column%>">
    <b><%$label%></b>
    {{#if search.order_by=='<%$column%>'}}
      <i class="fa {{_sort_class}}"></i>
    {{/if}}
</button>



