<div class="ui icon buttons">

    {{#if pagination.left}}
        <button class="ui primary button" on-click="page.prev">
            <i class="fa fa-chevron-left"></i>
        </button>
    {{else}}
        <button class="ui grey button">
            <i class="fa fa-chevron-left"></i>
        </button>
    {{/if}}

    <button class="ui disabled button">
        {{pagination.page}}
    </button>

    {{#if pagination.right}}
        <button class="ui primary button" on-click="page.next">
            <i class="fa fa-chevron-right"></i>
        </button>
    {{else}}
        <button class="ui grey button">
            <i class="fa fa-chevron-right"></i>
        </button>
    {{/if}}

</div>



@section('scripts')
@parent
<script type="text/javascript">
    Data.set('_url', '<%@$url%>');
</script>

@endsection
