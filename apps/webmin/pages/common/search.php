<div class="ui fluid right action left icon input">
    <i class="icon fa-search"></i>
    <input type="text" value="{{search.q}}" on-enter="page.refresh"  placeholder="Search ..">
    <button class="ui button" on-click="page.refresh">Search</button>
</div>
