<a class="ui blue label" v-if="!<%$item%>.delete"  on-click="<%$onEdit%>">
    <i class="fa fa-pencil"></i>
</a>
<a class="ui red label" v-if="!<%$item%>.delete" on-click="<%$onDelete%>">
    <i class="fa fa-trash"></i>
</a>
<a class="ui green label" v-if="<%$item%>.delete" on-click="<%$onDelete%>">
    Yes
</a>
<a class="ui yellow label" v-if="<%$item%>.delete" on-click="<%$item%>.delete = 0">
    No
</a>
