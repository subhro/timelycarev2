<?php
$cookie_time = (3600 * 24 * 30);
setcookie('__tcUn', '', time() - $cookie_time, '/');
setcookie('__tcPw', '', time() - $cookie_time, '/');

Session::destroy();
Response::redirect('/admin');
