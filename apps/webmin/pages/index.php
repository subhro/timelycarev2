@extends('shared.layout')
@include('shared.events')

@include('widgets.message')
@include('widgets.modal')

@include('modals.forgot-password')

@section('styles')
<style>
#content {
  padding: 0;
}
.login-body {
  background: linear-gradient(rgba(255, 255, 255, 0.96), rgba(255, 255, 255, 0.96)), url(/assets/imgs/bg.jpg) repeat;
  background-size: 720px;
}
.login-panel .login-header{
  text-align: left;
}
.login-logo {
  margin-top: 22px;
  margin-bottom: 16px;
}
.login-text {
  display: inline-block;
  font-size: 38px;
  margin-bottom: 18px;
  top: -20px;
  position: relative;
  color: #ffffffd1;
}
.show-password {
  position: absolute;
  right: 5px;
  top: 9px;
  font-size: 18px;
  color: #2A7CEE;
  cursor: pointer;
}
</style>
@endsection

@section('content')

<div class="row center-xs">
  <div class="col-xs-12 col-sm-8  col-md-5 login-panel">

    <div class="login-header">
      <img class="login-logo" src="/assets/imgs/logo-white.png" />
    </div>

    <div class="ui segment">
      <div class="ui fluid labeled input">
        <div class="ui default label">
          <i class="fa fa-envelope"></i>
        </div>
        <input type="text" placeholder="Username" type="text" autofocus value="{{$admin.email}}" on-enter="admin.login" />
      </div>

      <div class="ui fluid labeled input">
        <div class="ui default label">
          <i class="fa fa-lock" style="padding:0 3px 0 2px"></i>
        </div>
        <input id="adminPassword" type="password" placeholder="Password" type="password" value="{{$admin.password}}" on-enter="admin.login" />
        <a class="show-password" on-click="show.password" on-enter="show.password" title="Show Password">
          <i class="fa fa-eye"></i>
        </a>
      </div>

      <div class="two fields">
        <div class="field pull-left">
          <a on-click="open.forgotpass" style="cursor:pointer;">
            <span class="remember pull-right">Forgot Password?</span>
          </a>
        </div>
        <div class="field pull-right">
          <button class="ui primary button btn-default pull-right" on-enter="admin.login" on-click="admin.login">Login</button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section('scripts')
@parent
<script type="text/javascript">

  Event.on('admin.login', function(){
    var params = Data.get('$admin');
    Api.post('/admin/api/index').params(params).send();
  });

  Event.on('open.forgotpass', function(){
    Tag.get('forgot-password').fire('modal.open');
  });

  Event.on('show.password', function() {
    var field = document.getElementById('adminPassword');
    field.type = (field.type === 'password') ? 'text' : 'password';
  });

</script>
@endsection
