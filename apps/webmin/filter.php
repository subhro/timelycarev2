<?php


if(Request::is('/admin')){
  return [
    'denied' => false
  ];
}

if(Request::is('/admin/api/index')||Request::is('/admin/api/forgot-password')||Request::is('/admin/confirm/')||Request::is('/admin/api/confirm')){
  return [
    'denied' => false
  ];
}



if(Session::get('uid')){
  return [
    'denied' => false
  ];
}



return [
  'denied' => true,
  'redirect' => '/admin'
];
