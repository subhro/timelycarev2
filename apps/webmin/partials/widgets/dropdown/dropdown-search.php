@section('markups')
@parent

<script id="dropdown-search.tpl" type="text/template">

  <div class="ui selection fluid dropdown search {{$.tagClass}}" class-loading="{{loading}}">
    <input class="search" on-keyup="tag.type" value="{{$.value}}" placeholder="Search ..." autocomplete="off">
    <i class="dropdown icon {{$.icon}}"></i>
    <div class="menu transition {{$.menuClass}}">
      {{#each $.options:index}}
      <div class="item key{{index}}" class-selected="{{$.selected == index}}" on-click="tag.click" index={{index}}>
        {{label}}
        {{#if details}}
            <br> <small>{{details}}</small>
        {{/if}}
      </div>
      {{/each}}
    </div>
  </div>

</script>

@endsection




@section('scripts')
@parent
<script type="text/javascript">


Tag('dropdown-search', '#dropdown-search.tpl');

Tag.on('tag.init', function(){
  //
  this.set('$.options', []);

  Event.on('body.clicked', this.trigger('tag.close'));

});


Tag.observe('label', function(){
    this.set('$.value',  this.get('label'));
});


Tag.on('tag.show', function(){
  this.set('$.selected', -1);
  Event.fire('dropdown.show', this);
});

Tag.on('tag.close', function(){
  Event.fire('dropdown.hide', this);
});


Tag.on('tag.type', function(e){

  if(e.original.key=='ArrowUp')
  return this.fire('tag.arrow-change', 1);
  else if(e.original.key=='ArrowDown')
  return this.fire('tag.arrow-change', -1);
  else if(e.original.key=='Enter')
  return this.fire('tag.enter');


  var self = this;
  self.set('loading', true);

  clearTimeout(this.timer);
  this.timer = setTimeout(function(){
    Api.get(self.get('url')).params({q:self.get('$.value')}).send(function(response){
      self.fire('tag.show');
      self.set('loading', false);
      self.set('$.options', response);
    });

  },1000);

});

Tag.on('tag.arrow-change', function(direction){
  Event.fire('dropdown.arrow-change', this, direction);
});

Tag.on('tag.click', function(e){

  e.original.stopPropagation();

  var index = e.node.attrs('index');
  var value = e.get();

  this.fire('tag.select', value, index);

});


Tag.on('tag.enter', function(e){

  var options = this.get('$.options');
  var index = this.get('$.selected');
  var value = options[index];

  this.fire('tag.select', value, index);


});



Tag.on('tag.select', function(value, index){

  this.set('value', value.value);
  this.set('$.value', value.label);
  this.set('$.selected', index);

  this.fire('select', this, value);

  Event.fire('dropdown.hide', this);
});


</script>
@endsection
