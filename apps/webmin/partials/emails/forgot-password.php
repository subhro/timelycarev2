<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FCFCFD url('<%$site_url%>/assets/imgs/body-bg.png') repeat 0 0;border:1px solid rgba(0, 0, 0, 0.15);font-family:Verdana,sans-serif;">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="75" align="center" style="background:rgba(0, 0, 0, 0.15) repeat;border-bottom: 1px solid rgba(0, 0, 0, 0.15);">
						<img width="200" alt="" src="<%$site_url%>/assets/imgs/logo.png" alt="TimelyCare" />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#0e59ac" size="2.5">
							<br /><br /><span>Dear <%$name%>,<br /><br />Please click the below link to confirm to reset the password.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font size="2">
							<span style="font-size:14px;font-weight:700;margin:0 7px;color:#ff6c00;">
								New Password: <strong><%$password%></strong>
							</span>
						</font><br /><br />
						<font color="#161616" size="2">
							<a href="<%$redirect%>" target="_blank"><span><%$redirect%></span></a>
						</font><br />
					</td>
				</tr>
				<tr>
					<td>
						<hr>
						<br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>In case if you have any questions, please send an Email to : <a style="color:#41a5e1;text-decoration:none;" href="mailto:support@timelycare.org">support@timelycare.org</a></span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>Regards,<br /><br />The <a href="<%$site_url%>">TimelyCare</a> Team.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="center">
						<font color="#aaa" size="2">
								<span>P.S: This is a system generated email. Please do not reply.</span><br /><br />
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
