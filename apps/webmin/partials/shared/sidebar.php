@section('sidebar')

  <div id="sidebar" class="ui vertical menu">

    <a class="logo" href="/admin">
      <img src="/assets/imgs/logo-white.png">
    </a>

    <a class="item @if($page=='dashboard') active @endif" href="/admin/dashboard">
      <i class="fa fa-dashboard"></i> Dashboard
    </a>
    <a class="item @if($page=='doctors') active @endif" href="/admin/doctors">
      <i class="fa fa-user-md"></i> Doctors
    </a>
    <a class="item @if($page=='nurses') active @endif" href="/admin/nurses">
      <i class="fa fa-user-md"></i> Nurses
    </a>
    <a class="item @if($page=='notifications') active @endif" href="/admin/notifications/chat">
      <i class="fa fa-bell-o"></i> Notifications
    </a>
    <a class="item @if($page=='schedules') active @endif" href="/admin/schedules/view">
      <i class="fa fa-calendar"></i> Schedules
    </a>
    <a class="item @if($page=='settings') active @endif" href="/admin/settings/basic">
      <i class="fa fa-gear"></i> Settings
    </a>

  </div>

@endsection
