@section('header')
<div id="header">
  <label for="sidebar-toggle" id="sidebar-button">
    <i class="fa fa-bars"></i>
  </label>
  <br>
  <div class="row">
    <div class="col-xs-8">
        <h3 class="page-header"><%$page%></h3>
    </div>
    <div class="col-xs-4 end-xs">
      <a href="/admin/logout">
        <i class="fa fa-power-off"></i> Logout
      </a>
    </div>
  </div>
</div>
@endsection
