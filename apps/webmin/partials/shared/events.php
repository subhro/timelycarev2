@section('scripts')
@parent
<script type="text/javascript">

Event.on('records.delete', function(record) {
  Data.splice(record.key, record.index, 1);
  Data.set('deleted', -1);
  Data.set('$deleted_left', -1);
  Data.set('$deleted_right', -1);
  Data.set('$deleted_doctors_group', -1);
  Data.set('$deleted_nurses_group', -1);
  Data.set('$deleted_schedule', -1);
  Data.set('$deleted_doc_shift', -1);
  Data.set('$deleted_nur_shift', -1);
});

Event.on('message.show', function(message){
  Data.push('messages', message);
});


Event.on('page.next', function(){
  Event.fire('page.paginate', 1);
});
Event.on('page.prev', function(){
  Event.fire('page.paginate', -1);
});
Event.on('page.refresh', function(){
  Data.set('pagination.page', 1);
  Event.fire('page.paginate', 0);
});
Event.on('page.redirect', function(url){
  window.location = url;
});
Event.on('page.redirect.newtab', function(url){
  window.open(url, '_blank');
});
Event.on('delay.redirect', function(url){
  setTimeout(function(){
    window.location = url;
  }, 1000);
});
Event.on('delay.redirect.newtab', function(url){
  setTimeout(function(){
    window.open(url, '_blank');
  }, 1000);
});

Event.on('modal.target.close', function(target){
  setTimeout(function(){
    Tag.get(target).fire('modal.close');
  },600);
});


Event.on('page.paginate', function(direction){

  var url = Data.get('$url');

  var search = Data.get('search');

  if(!search)
  search = {};

  var params = {};
  for (var key in search) { params[key] = search[key]; }

  params.page = Data.get('pagination.page');
  params.page = parseInt(params.page) + direction;


  if(!params.page<1)
  Data.set('pagination.page', params.page);

  document.body.className = '_requesting';

  setTimeout(function(){
    Api.get(url).params(params).send();
  },1000);


});


Event.on('page.sort', function(e){

  var column = e.node.attrs('column');
  console.log(column);
  var order_by = Data.get('search.order_by');
  var order_direction = Data.get('search.order_direction');

  if(order_by != column)
  order_direction = 'none';

  order_by = column;

  if(order_direction == 'asc'){
    order_direction = 'desc';
    Data.set('_sort_class', 'fa-chevron-up');
  }else{
    order_direction = 'asc';
    Data.set('_sort_class', 'fa-chevron-down');
  }

  Data.set('pagination.page', 1);
  Data.set('search.order_by', order_by);
  Data.set('search.order_direction', order_direction);


  Event.fire('page.paginate', 0);

});


Event.on('api.init', function($request){
  Data.set('$loading', true);
  if(!Api.$hideLoading){
    document.body.className = '_requesting';
  }
});


Event.on('api.success', function($response){

  Data.set('$loading', false);
  document.body.className = '';
  Api.$hideLoading = false;

  if(!$response)
  return;

  if(!$response.data)
  $response.data = [];

  if($response.data.options)
  Data.set('options', $response.data.options);

  for(var key in $response.data){
    Data.set(key, $response.data[key]);
  }

  if(!$response.events)
  $response.events = [];

  for(var key in $response.events){
    Event.fire(key, $response.events[key]);
  }

});


Event.on('api.error', function(){
  Data.set('loading', false);
  document.body.className = '';
});





</script>
@endsection
