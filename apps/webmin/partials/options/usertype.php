@section('scripts')
@parent
<script>
  Data.set('<%$key%>', 0);

  Data.set('options.usertype', [

    { value: 0, label: 'Doctor' },
    { value: 1, label: 'Nurse' }

  ]);
</script>
@endsection

<dropdown value="{{<%$key%>}}" options="{{options.usertype}}" on-select="type.select"></dropdown>
