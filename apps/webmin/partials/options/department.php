@section('scripts')
@parent
<script>

  Data.set('options.departments', [

    { value: 'Addiction Physician', label: 'Addiction Physician' },
    { value: 'Allergist', label: 'Allergist' },
    { value: 'Anesthesiologist', label: 'Anesthesiologist' },
    { value: 'Andrologist', label: 'Andrologist' },
    { value: 'Army Doctor', label: 'Army Doctor' },
    { value: 'Audiologist', label: 'Audiologist' },
    { value: 'Cardiologist', label: 'Cardiologist' },
    { value: 'Cardiac Electrophysiologist', label: 'Cardiac Electrophysiologist' },
    { value: 'Chiropractor', label: 'Chiropractor' },
    { value: 'Dermatologist', label: 'Dermatologist' },
    { value: 'Diabetologist', label: 'Diabetologist' },
    { value: 'Diagnostician', label: 'Diagnostician' },
    { value: 'Emergency Physician / Emergency (ER) Doctors', label: 'Emergency Physician / Emergency (ER) Doctors' },
    { value: 'Endocrinologist', label: 'Endocrinologist' },
    { value: 'Epidemiologist', label: 'Epidemiologist' },
    { value: 'Euthanasia Doctors‎', label: 'Euthanasia Doctors‎' },
    { value: 'Family Medicine Physician', label: 'Family Medicine Physician' },
    { value: 'Fictional Medical Specialist', label: 'Fictional Medical Specialist' },
    { value: 'Gastroenterologist', label: 'Gastroenterologist' },
    { value: 'General Physician', label: 'General Physician' },
    { value: 'Geriatrician', label: 'Geriatrician' },
    { value: 'Gynaecologist‎', label: 'Gynaecologist' },
    { value: 'Hematologist', label: 'Hematologist' },
    { value: 'Hepatologist', label: 'Hepatologist' },
    { value: 'High Altitude Medicine Physician', label: 'High Altitude Medicine Physician' },
    { value: 'Hygienists‎', label: 'Hygienists‎' },
    { value: 'Hyperbaric Physician', label: 'Hyperbaric Physician' },
    { value: 'Immunologist', label: 'Immunologist' },
    { value: 'Infectious Disease Specialist', label: 'Infectious Disease Specialist' },
    { value: 'Intensivist', label: 'Intensivist' },
    { value: 'Internal Medicine Specialist', label: 'Internal Medicine Specialist' },
    { value: 'Leprologist', label: 'Leprologist' },
    { value: 'Maxillofacial Surgeon / Oral Surgeon', label: 'Maxillofacial Surgeon / Oral Surgeon' },
    { value: 'Medical Geneticist', label: 'Medical Geneticist' },
    { value: 'Microbiologist', label: 'Microbiologist' },
    { value: 'Neonatologist', label: 'Neonatologist' },
    { value: 'Nephrologist', label: 'Nephrologist' },
    { value: 'Neurologist', label: 'Neurologist' },
    { value: 'Neurosurgeon', label: 'Neurosurgeon' },
    { value: 'Nuclear Medicine Specialist', label: 'Nuclear Medicine Specialist' },
    { value: 'Obstetrician/Gynaecologist (OB/GYN)', label: 'Obstetrician/Gynaecologist (OB/GYN)' },
    { value: 'Occupational Medicine Specialist', label: 'Occupational Medicine Specialist' },
    { value: 'Oncologist', label: 'Oncologist' },
    { value: 'Ophthalmologist', label: 'Ophthalmologist' },
    { value: 'Osteopathic Physician', label: 'Osteopathic Physician' },
    { value: 'Orthopedic Surgeon / Orthopedist', label: 'Orthopedic Surgeon / Orthopedist' },
    { value: 'Otolaryngologist (ENT Specialist)', label: 'Otolaryngologist (ENT Specialist)' },
    { value: 'Pain Management Physician', label: 'Pain Management Physician' },
    { value: 'Palliative Care Specialist', label: 'Palliative Care Specialist' },
    { value: 'Parasitologist', label: 'Parasitologist' },
    { value: 'Pathologist', label: 'Pathologist' },
    { value: 'Perinatologist', label: 'Perinatologist' },
    { value: 'Periodontist', label: 'Periodontist' },
    { value: 'Pediatrician', label: 'Pediatrician' },
    { value: 'Pharmacologist‎', label: 'Pharmacologist‎' },
    { value: 'Psychiatrist‎', label: 'Psychiatrist‎' },
    { value: 'Physiotherapist', label: 'Physiotherapist' },
    { value: 'Plastic Surgeon', label: 'Plastic Surgeon' },
    { value: 'Podiatrist / Chiropodist', label: 'Podiatrist / Chiropodist' },
    { value: 'Prison Physician', label: 'Prison Physician' },
    { value: 'Psychiatrist', label: 'Psychiatrist' },
    { value: 'Pulmonologist', label: 'Pulmonologist' },
    { value: 'Radiologist', label: 'Radiologist' },
    { value: 'Rehabilitation Physician', label: 'Rehabilitation Physician' },
    { value: 'Rheumatologist', label: 'Rheumatologist' },
    { value: 'Sexologist', label: 'Sexologist' },
    { value: 'Sleep Doctor / Sleep Disorders Specialist', label: 'Sleep Doctor / Sleep Disorders Specialist' },
    { value: 'Spinal Cord Injury Specialist', label: 'Spinal Cord Injury Specialist' },
    { value: 'Sports Physician', label: 'Sports Physician' },
    { value: 'Surgeon', label: 'Surgeon' },
    { value: 'Thoracic Surgeon', label: 'Thoracic Surgeon' },
    { value: 'Toxicologist', label: 'Toxicologist' },
    { value: 'Traditional Medicine Practitioner', label: 'Traditional Medicine Practitioner' },
    { value: 'Tropical Physician', label: 'Tropical Physician' },
    { value: 'Urologist', label: 'Urologist' },
    { value: 'Vascular Surgeon', label: 'Vascular Surgeon' },
    { value: 'Veterinarian', label: 'Veterinarian' },
    { value: 'Venereologist', label: 'Venereologist' }

  ]);

</script>
@endsection

<dropdown value="{{<%$key%>}}" options="{{options.departments}}"></dropdown>
