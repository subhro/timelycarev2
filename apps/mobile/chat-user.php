<?php

$uid         = Input::get('uid');
$token       = Input::get('token');
$receiver_id = Input::get('receiverId', 0);
$lastChatId  = Input::get('lastChatId', 0);

$limit = 10;

$receiver = User::with('description')->find($receiver_id);

$chats = Chat::where('id', '>', $lastChatId)
->where(function($query) use(&$uid, &$receiver_id){
  $query->where(function($query) use(&$uid, &$receiver_id){
    $query->where('sender_id', $uid);
    $query->where('receiver_id', $receiver_id);
  });
  $query->orWhere(function($query) use(&$uid, &$receiver_id){
    $query->where('sender_id', $receiver_id);
    $query->where('receiver_id', $uid);
  });
})
->whereIn('type', [0,1,3,8])
->limit($limit)
->orderBy('timestamp', 'desc')
->get();

if(!$chats->count()){
  $response = [
    'type'    => 'error',
    'message' => 'No chat history'
  ];
  goto RESPONSE;
}

Chat::where('sender_id', $receiver_id)
->where('receiver_id', $uid)
->where('status', 0)
->update(['status' => 1]);

$response = [
  'type'  => 'success',
  'chats' => $chats
];

RESPONSE:
return $response;
