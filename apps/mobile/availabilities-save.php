<?php

$rules = [
  'value' => 'required'
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'Field required'
  ];
  goto RESPONSE;
}

$uid   = Input::get('uid');
$token = Input::get('token');
$field = Input::get('field');
$value = Input::get('value');

$availability = Availability::where('user_id', $uid)->first();
if(!$availability){
  $availability = new Availability;
}

$availability->user_id  = $uid;
$availability->{$field} = $value;
$availability->save();

$response = [
  'type'    => 'success',
  'message' => 'Status updated'
];
RESPONSE:
return $response;
