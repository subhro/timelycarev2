<?php

$dateAry = [];

$uid         = Input::get('uid');
$token       = Input::get('token');
$isworking   = Input::get('isWorking');

$User = User::with('description')->find($uid);
$rules = [
  'start_date' => 'required',
  'end_date'   => 'required'
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'Invalid date selection'
  ];
  goto RESPONSE;
}

$dateRange = new DatePeriod(new DateTime(Input::get('start_date')),new DateInterval('P1D'),new DateTime(Input::get('end_date').'23:59:59'));

foreach($dateRange as $date){
  $leave = ShiftRequest::where('user_id', $uid)
  ->where('request_type', 1)
  ->where('leave_date', $date->format("Y-m-d"))
  ->first();

  if(!$leave) {
    $leave = new ShiftRequest;
  }

  $leave->user_id	      = $uid;
  $leave->type	        = $User->type;
  $leave->request_type  = 1;
  $leave->leave_date	  = $date->format("Y-m-d");
  $leave->is_working    = $isworking;
  $leave->status	      = 0;
  $leave->timestamp	    = date('Y-m-d H:i:s');
  $leave->save();

  $analytics = Analytics::where('type', $User->type)->where('date', $date->format("Y-m-d"))->first();
  if(!$analytics) {
    $analytics = new Analytics();
  }
  $analytics->date = $date->format("Y-m-d");
  $analytics->type = $User->type;
  $analytics->leave_request = $analytics->leave_request + 1;
  $analytics->save();
}

$response = [
  'type'    => 'success',
  'message' => 'Your request successfully stored.'
];

RESPONSE:
return $response;
