<?php

$rules = [
  'receiverId' => 'required'
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    'type'    => 'error',
    'message' => 'Invalid sender or receiver'
  ];
  goto RESPONSE;
}

$uid = Input::get('uid');
$token = Input::get('token');

$chat = new Chat;
$chat->type = Input::get('chatType');
$chat->sender_id = Input::get('uid');
$chat->receiver_id = Input::get('receiverId');
$chat->message = Input::get('message');
$chat->message_type = Input::get('messageType', 'text');
$chat->timestamp = date('Y-m-d H:i:s');
$chat->save();

$response = [
  'type'     => 'success',
  'lastChat' => $chat
];


RESPONSE:
return $response;
