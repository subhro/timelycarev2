<?php

$uid   = Input::get('uid');
$token = Input::get('token');

$availabilities = Availability::where('user_id', $uid)->first();
if(!$availabilities){
  $response = [
    'type' => 'error'
  ];
  goto RESPONSE;
}

$response = [
  'type'           => 'success',
  'availabilities' => $availabilities
];

RESPONSE:
return $response;
