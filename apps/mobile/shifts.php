<?php

$uid = Input::get('uid');
$token = Input::get('token');

$user = User::find($uid);

$shifts = Shift::where('type', $user->type)->get();

$response = [
  'type'   => 'success',
  'shifts' => $shifts
];

return $response;
