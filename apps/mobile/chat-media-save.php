<?php

$rules = [
  'receiverId' => 'required'
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'Invalid sender or receiver'
  ];
  goto RESPONSE;
}

$aExts = ['3gpp', 'aa', 'aac', 'aax', 'act', 'aiff', 'amr', 'ape', 'au', 'awb', 'dct', 'dss', 'dvf', 'flac', 'gsm', 'iklax', 'ivs', 'm4a', 'm4b', 'm4p', 'mmf', 'mp3', 'mpc', 'msv', 'ogg', 'oga', 'mogg', 'opus', 'ra', 'rm', 'raw', 'sln', 'tta', 'vox', 'wav', 'wma', 'wv', 'webm', '8svx'];

$vExts = ['webm', 'mkv', 'flv', 'vob', 'ogv', 'ogg', 'drc', 'avi', 'mov', 'qt', 'wmv', 'yuv', 'rm', 'rmvb', 'asf', 'amv', 'mp4', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'm2v', 'm4v', 'svi', '3gp', '3g2', 'mxf', 'roq', 'nsv', 'f4v', 'f4p', 'f4a', 'f4b'];

$iExts = ['jpg', 'jpeg', 'gif', 'png', 'bmp'];

$uid         = Input::get('uid');
$token       = Input::get('token');
$receiverId  = Input::get('receiverId');
$chatType    = Input::get('chatType');

$inputFile   = $_FILES['file']['tmp_name'];
$fileType    = strtolower($_FILES['file']['type']);

$fileName    = $_FILES['file']['name'];
$mediaExt    = explode('.', $fileName);
$ext         = strtolower(end($mediaExt));
$path        = STORAGE_DIR.'/media';
$mediaName   = uniqid();
$messageType = 'url';
if(in_array($ext, $aExts)){
  $messageType = 'audio';
  $mediaName   = $mediaName.'.mp3';
  $outputFile  = $path.'/'.$mediaName;
  exec("ffmpeg -i $inputFile -c:a libmp3lame $outputFile");
}
if(in_array($ext, $vExts)){
  $messageType = 'video';
  $mediaName   = $mediaName.'.mp4';
  $outputFile  = $path.'/'.$mediaName;
  exec("ffmpeg -i $inputFile $outputFile -hide_banner");
}
if(in_array($ext, $iExts)){
  $messageType = 'image';
  $mediaName   = $mediaName.'.'.$ext;
  $outputFile  = $path.'/'.$mediaName;
  @move_uploaded_file($_FILES['file']['tmp_name'], $outputFile);
}

$chatMediaUrl = FULL_URL . '/storage/media/'.$mediaName;

$chat = new Chat;

$chat->type        = $chatType;
$chat->sender_id   = $uid;
$chat->receiver_id = $receiverId;
$chat->message     = $chatMediaUrl;
$chat->message_type	= $messageType;
$chat->timestamp	 = date('Y-m-d H:i:s');
$chat->save();

$response = [
  'type'     => 'success',
  'lastChat' => $chat
];


RESPONSE:
return $response;
