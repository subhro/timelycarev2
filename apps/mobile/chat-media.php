<?php

$sender_id   = Input::get('uid');
$token       = Input::get('token');
$receiver_id = Input::get('id');

$medias = Chat::where(function($query) use(&$sender_id, &$receiver_id){
  $query->where(function($query) use(&$sender_id, &$receiver_id){
    $query->where('sender_id', $sender_id);
    $query->where('receiver_id', $receiver_id);
    $query->where('message_type', '!=', 'text');
  });
  $query->orWhere(function($query) use(&$sender_id, &$receiver_id){
    $query->where('sender_id', $receiver_id);
    $query->where('receiver_id', $sender_id);
    $query->where('message_type', '!=', 'text');
  });
})
->get();

if(!$medias->count()){
  $response = [
    'type'    => 'error',
    'message' => 'No chat history'
  ];
  goto RESPONSE;
}

$response = [
  'type'  => 'success',
  'medias' => $medias
];

RESPONSE:
return $response;
