<?php

$limit = 30;
$uid   = Input::get('uid');
$token = Input::get('token');
$page  = Input::get('p', 1);

$grpupIds = Group::where('user_id', $uid)
->where('type', 0)
->pluck('group_id');

$chats = Chat::where(function($query) use(&$uid, &$grpupIds){
  $query->where('sender_id', $uid);
  $query->orWhere('receiver_id', $uid);
  if(count($grpupIds)){
    $query->orWhereIn('receiver_id', $grpupIds);
  }
})
->whereIn('type', [0,2,3,8])
->orderBy('timestamp', 'desc')
->limit($limit)
->skip($limit *($page-1))
->get();


$doctors = [];
$userIdAdded = [];
foreach($chats as $chat){
  if($chat->receiver_id == $uid){
    $chat->receiver_id = $chat->sender_id;
  }
  $userId = $chat->receiver_id;
  if(!isset($userIdAdded[$userId])){
    $userIdAdded[$userId] = true;
    $user = User::with('description')
          ->whereIn('type', [0,2,8])
          ->where('id', $userId)
          ->first();
    if($user)
      $doctors[] = $user;
  }
}

$pagination = Pagination::get($page, $limit, count($doctors));

if(!count($doctors)){
  $response = [
    'type'       => 'success',
    'message'    => 'No doctors available',
    'pagination' => $pagination
  ];
  goto RESPONSE;
}

$response = [
  'type'  => 'success',
  'users' => $doctors,
  'pagination' => $pagination
];


RESPONSE:
return $response;
