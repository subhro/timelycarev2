<?php

$uid   = Input::get('uid');
$token = Input::get('token');

$status = Input::get('status');

$user = User::with('description')->find($uid);

if(!$user){
  $response = [
    'type' => 'error'
  ];
  return $response;
}

$user->status = $status;
$user->save();

$response = [
  'type' => 'success'
];

return $response;
