<?php

$rules = [
  'current_password' => 'required',
  'new_password'     => 'required|different:current_password',
  'confirm_password' => 'required_with:new_password|same:new_password'
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => $errors[0]
  ];
  goto RESPONSE;
}

$uid     = Input::get('uid');
$token   = Input::get('token');
$current = Input::get('current_password');
$new     = Input::get('new_password');

$user = User::find($uid);

if(Crypto::hash($current) == $user->password){
  $user->password = Crypto::hash($new);
  $user->save();
  $response = [
    'type'    => 'success',
    'message' => 'Password has been updated',
  ];
}else{
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'Wrong password entered',
  ];
}

RESPONSE:
return $response;
