<?php

$limit = 30;
$uid   = Input::get('uid');
$token = Input::get('token');
$page  = Input::get('p', 1);
$tab   = Input::get('t', 'all');

$userType = [1,8];
$userIds = [];
$message = 'No nurses available';

if($tab == 'fav'){
  $userIds = Favorite::whereIn('type', $userType)
  ->where('user_id', $uid)
  ->where('trash', 0)
  ->pluck('favorite_id');

  $message = 'No favorite nurses';
}

if($tab == 'call'){
  $today = date('Y-m-d');
  $currentTime = date('H:i:s');
  $userIds = Schedule::whereIn('type', $userType)
  ->where('date', $today)
  ->where('shift_start', '<', $currentTime)
  ->where('shift_end', '>', $currentTime)
  ->pluck('user_id');

  $message = 'No nurses on call';
}

$grpupIds = [];
if($tab != 'call'){
  $grpupIds = Group::where('user_id', $uid)
  ->whereIn('type', $userType)
  ->pluck('group_id');
}

$records = User::with('description')
->where(function($query) use(&$uid, &$tab, &$userIds, &$userType){
  $query->where('id', '!=', $uid);
  $query->whereIn('type', $userType);
  if($tab != 'all' || count($userIds)){
    $query->whereIn('id', $userIds);
  }
  if(Input::get('q')){
    $query->where('name', 'like', Input::get('q').'%');
  }
})
->orWhere(function($query) use(&$tab, &$grpupIds){
  if($tab == 'all' && count($grpupIds)){
    $query->where('type', 2);
    $query->whereIn('id', $grpupIds);
  }
})
->orderBy('name', 'asc')
->limit($limit)
->skip($limit *($page-1))
->get();

$pagination = Pagination::get($page, $limit, $records->count());

if(!$records->count()){
  $response = [
    'type'       => 'success',
    'message'    => $message,
    'pagination' => $pagination
  ];
  goto RESPONSE;
}

$response = [
  'type' => 'success',
  'users' => $records,
  'pagination' => $pagination
];

RESPONSE:
return $response;
