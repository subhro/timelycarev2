<?php

$uid   = Input::get('uid');
$token = Input::get('token');

if(Input::has('id')){
  $uid = Input::get('id');
}

$user = User::with('description')->find($uid);

$response = [
  'type' => 'success',
  'user' => $user
];

return $response;
