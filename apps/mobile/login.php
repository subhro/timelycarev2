<?php

$rules = [
  'email'    => 'required|email',
  'password' => 'required',
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    'type'    => 'error',
    'message' => 'Invalid email & password'
  ];
  goto RESPONSE;
}

$email = Input::get('email');
$password = Input::get('password');

$password = Crypto::hash($password);

$email = strtolower($email);
$user = User::with('description')
->where('email', $email)
->where('password', $password)
->first();

if(!$user){
  $response = [
    'type'    => 'error',
    'message' => 'Invalid email & password'
  ];
  goto RESPONSE;
}

$token = uniqid();
$user->token = $token;
$user->timezone_offset = Input::get('offset', 0);
$user->push_os = Input::get('push_os');
$user->push_token = Input::get('push_token');
$user->status = 1;
$user->save();

$availability = Availability::where('user_id', $user->id)->first();
if(!$availability){
  $availability = new Availability;
  $availability->user_id = $user->id;
  $availability->voice_call = 1;
  $availability->text_message = 1;
  $availability->custom_message = '';
  $availability->available_again = date('Y-m-d H:i:s');
  $availability->save();
}

$response = [
  'type' => 'success',
  'user' => $user
];

RESPONSE:
return $response;
