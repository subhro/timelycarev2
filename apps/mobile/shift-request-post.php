<?php

$rules = [
  'date' => 'required',
  'is_work' => 'required'
];

$errors = Input::validate($rules);

if($errors){
  $events = [
    'message.show' => [
      // 'code'    => 400,
      'type'    => 'error',
      'message' => 'Invalid date selection'
    ]
  ];
  goto RESPONSE;
}

$uid    = Input::get('uid');
$token  = Input::get('token');
$date   = Input::get('date');
$isWork = Input::get('is_work');

$user = User::with('description')->find($uid);
$request = new ShiftRequest;
$request->type       = $user->type;
$request->user_id    = $uid;
$request->year_month = date('Y-m', strtotime($date));
$request->date       = $date;
$request->working    = $isWork;
$request->status     = 0;
$request->timestamp  = date('Y-m-d H:i:s');
$request->save();

$analytics = Analytics::where('type', $user->type)->where('date', $date)->first();
if(!$analytics){
  $analytics = new Analytics();
}
$analytics->date = $date;
$analytics->type = $user->type;
$analytics->shift_request = $analytics->shift_request + 1;
$analytics->save();

$response = [
  'type'        => 'success',
  'message'     => 'Request successfully sent',
  'lastRequest' => $request
];


RESPONSE:
return $response;
