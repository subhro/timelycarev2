<?php

$uid         = Input::get('uid');
$token       = Input::get('token');

$limit = 10;
$user = User::with('description')->find($uid);

$leaves = ShiftRequest::where('user_id', $uid)
->where('request_type', 1)
->where('leave_date', '>',  date('Y-m-d'))
->where('trash', 0)
->orderBy('id', 'desc')
->get();

if(!$leaves->count()){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'No leave request found'
  ];
  goto RESPONSE;
}

$response = [
  'type'  => 'success',
  'leaves' => $leaves
];

RESPONSE:
return $response;
