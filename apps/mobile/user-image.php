<?php

$uid   = Input::get('uid');
$token = Input::get('token');

if(Input::has('id')){
  $uid = Input::get('id');
}

$user = User::find($uid);
if($user){
  $user->image = Input::get('image');
  $user->save();
}

$response = [
  'type' => 'success',
  'message' => 'Uploaded successfully'
];

return $response;
