<?php

$uid    = Input::get('uid');
$token  = Input::get('token');

$timestamp = date('Y-m-d H:i:s');
$requests = ShiftRequest::where('user_id', $uid)
->where('type', 1)
->orderBy('timestamp')
->get();

if(!$requests->count()){
  $response = [
    'type'    => 'error',
    'message' => 'No request'
  ];
  goto RESPONSE;
}

$response = [
  'type'     => 'success',
  'requests' => $requests
];


RESPONSE:
return $response;
