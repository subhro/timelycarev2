<?php

$uid         = Input::get('uid');
$token       = Input::get('token');
$receiver_id = Input::get('receiverId', 0);
$firstChatId  = Input::get('firstChatId', 0);

$limit = 4;

$user = User::with('description')->find($uid);

$chats = Chat::where('id', '<', $firstChatId)
->where(function($query) use(&$uid, &$receiver_id){
  $query->where(function($query) use(&$uid, &$receiver_id){
    $query->where('sender_id', $uid);
    $query->where('receiver_id', $receiver_id);
  });
  $query->orWhere(function($query) use(&$uid, &$receiver_id){
    $query->where('sender_id', $receiver_id);
    $query->where('receiver_id', $uid);
  });
})
->where('type', $user->type)
->limit($limit)
->orderBy('timestamp', 'desc')
->get();

if(!$chats->count()){
  $response = [
    'type'    => 'error',
    'message' => 'No chat history'
  ];
  goto RESPONSE;
}

$response = [
  'type'  => 'success',
  'chats' => $chats
];

RESPONSE:
return $response;
