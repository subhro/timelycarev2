<?php

$uid         = Input::get('uid');
$token       = Input::get('token');
$receiver_id = Input::get('receiverId', 0);
$lastChatId  = Input::get('lastChatId', 0);

$limit = 10;

$chats = Chat::with(['sender', 'receiver'])->where('id', '>', $lastChatId)
->where('type', 2)
->where('receiver_id', $receiver_id)
->orderBy('timestamp', 'desc')
->limit($limit)
->get();

if(!$chats->count()){
  $response = [
    'type'    => 'error',
    'message' => 'No chat history'
  ];
  goto RESPONSE;
}

$response = [
  'type'  => 'success',
  'chats' => $chats
];

RESPONSE:
return $response;
