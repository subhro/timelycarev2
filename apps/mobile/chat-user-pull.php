<?php

$uid         = Input::get('uid');
$token       = Input::get('token');
$receiverId = Input::get('receiverId', 0);
$lastChatId  = Input::get('lastChatId', 0);

$limit = 10;

$user = User::with('description')->find($uid);

$chatCout = 0;
$timestamp = time();

$chats = [];

while($chatCout == 0 && $timestamp + 10 > time()){
  $chats = Chat::where('id', '>', $lastChatId)
  ->where(function($query) use(&$uid, &$receiverId){
    $query->where(function($query) use(&$uid, &$receiverId){
      $query->where('sender_id', $uid);
      $query->where('receiver_id', $receiverId);
    });
    $query->orWhere(function($query) use(&$uid, &$receiverId){
      $query->where('sender_id', $receiverId);
      $query->where('receiver_id', $uid);
    });
  })
  ->where('type', $user->type)
  ->limit($limit)
  ->orderBy('timestamp', 'desc')
  ->get();

  $chatCout = $chats->count();
}

if(!$chatCout){
  $response = [
    'type'    => 'error',
    'message' => 'No chat history'
  ];
  goto RESPONSE;
}

Chat::where('sender_id', $receiverId)
->where('receiver_id', $uid)
->where('status', 0)
->update(['status' => 1]);

$response = [
  'type'  => 'success',
  'chats' => $chats
];

RESPONSE:
return $response;
