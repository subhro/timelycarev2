<?php

$uid   = Input::get('uid');
$token = Input::get('token');

$path = STORAGE_DIR . '/users';
$path = $path.'/'.$uid.'.jpg';

$upload = move_uploaded_file($_FILES['file']['tmp_name'], $path);
if(!$upload){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'Error while uploading photo'
  ];
  goto RESPONSE;
}

$response = [
  'type'    => 'success',
  'message' => 'Photo successfully changed',
  'image'   => FULL_URL . '/storage/users/'.$uid.'.jpg?rand='.rand()
];

RESPONSE:
return $response;
