<?php

$limit = 30;
$uid   = Input::get('uid');
$token = Input::get('token');
$page  = Input::get('p', 1);

$grpupIds = Group::where('user_id', $uid)
->where('type', 1)
->pluck('group_id');

$chats = Chat::where(function($query) use(&$uid, &$grpupIds){
  $query->where('sender_id', $uid);
  $query->orWhere('receiver_id', $uid);
  if(count($grpupIds)){
    $query->orWhereIn('receiver_id', $grpupIds);
  }
})
->whereIn('type', [1,2,3,8])
->orderBy('timestamp', 'desc')
->limit($limit)
->skip($limit *($page-1))
->get();


$nurses = [];
$userIdAdded = [];
foreach($chats as $chat){
  if($chat->receiver_id == $uid){
    $chat->receiver_id = $chat->sender_id;
  }
  $userId = $chat->receiver_id;
  if(!isset($userIdAdded[$userId])){
    $userIdAdded[$userId] = true;
    $user = User::with('description')
            ->whereIn('type', [1,2,8])
            ->where('id', $userId)->first();
    if($user)
      $nurses[] = $user;
  }
}

$pagination = Pagination::get($page, $limit, count($nurses));

if(!count($nurses)){
  $response = [
    'type'       => 'success',
    'message'    => 'No nurses available',
    'pagination' => $pagination
  ];
  goto RESPONSE;
}

$response = [
  'type'  => 'success',
  'users' => $nurses,
  'pagination' => $pagination
];


RESPONSE:
return $response;
