<?php

$rules = [
  'receiverId' => 'required'
];

$errors = Input::validate($rules);

if($errors){
  $response = [
    // 'code'    => 400,
    'type'    => 'error',
    'message' => 'Invalid sender or receiver'
  ];
  goto RESPONSE;
}

$uid        = Input::get('uid');
$token      = Input::get('token');
$receiverId = Input::get('receiverId');
$chatType   = Input::get('chatType');

$inputFile  = $_FILES['file']['tmp_name'];
$mediaName  = uniqid().'.mp3';
$path       = STORAGE_DIR.'/media';
$outputFile = $path.'/'.$mediaName;

exec("ffmpeg -i $inputFile -c:a libmp3lame $outputFile");
$chatMediaUrl = FULL_URL . '/storage/media/'.$mediaName;

$chat = new Chat;

$chat->type        = $chatType;
$chat->sender_id   = $uid;
$chat->receiver_id = $receiverId;
$chat->message     = $chatMediaUrl;
$chat->message_type = 'audio';
$chat->timestamp	 = date('Y-m-d H:i:s');
$chat->save();

$response = [
  'type'     => 'success',
  'lastChat' => $chat
];


RESPONSE:
return $response;
