<?php

$uid   = Input::get('uid');
$token = Input::get('token');
$favId = Input::get('favId');
$role  = Input::get('role');
$flag  = Input::get('flag');

$favorite = Favorite::where('user_id', $uid)->where('favorite_id', $favId)->first();
if(!$favorite){
  $favorite = new Favorite;
}

$favorite->user_id     = $uid;
$favorite->favorite_id = $favId;
$favorite->type        = $role;
$favorite->trash       = $flag;
$favorite->save();

if($flag){
  $message = 'Successfully unfavorited';
}else{
  $message = 'Successfully favorited';
}

$response = [
  'type'     => 'success',
  'message'  => $message,
  'favorite' => (1-$flag)
];

return $response;
