<?php

$data            = false;
$events          = false;
$prev_year_month = false;
$next_year_month = false;

$uid   = Input::get('uid');
$token = Input::get('token');

$year_month = Input::get('year_month');

if(!$year_month){
  $year_month = date('Y-m');
}

if($year_month != date('Y-m')){
  $prev_year_month = date('Y-m', strtotime($year_month.' -1 month'));
}

if($year_month != date('Y-m', strtotime('+2 month'))){
  $next_year_month = date('Y-m', strtotime($year_month.' +1 month'));
}
$year_month_text = date('F, Y', strtotime($year_month));

$year_month_array = explode('-', $year_month);
$year  = $year_month_array[0];
$month = $year_month_array[1];

$offset = 1 - date('w', mktime(12, 0, 0, $month, 1, $year));
if($offset == '-5' && $month != '2'):
  $cells = 42;
elseif($offset == '1' && $year%4 != 0 && $month == '2'):
  $cells = 28;
endif;

$schedules = Schedule::where('user_id', $uid)
->where('year_month', '=', $year_month)
->get();

$schedulesSearch = [];
foreach ($schedules as $schedule) {
  $schedulesSearch[$schedule->date] = $schedule;
}

$count = 42;
$class = '';
for($day=0; $day<$count; $day++):
  $time = mktime(12, 0, 0, $month, $day + $offset, $year);
  $week = date('w', $time);
  $index = (Int)$day/7;

  $colorIndex = rand(0,5);

  if(date('m', $time)!=$month):
    $calendar[$index][$week] = [];
  else:
    $date = date('Y-m-d', $time);
    if(!isset($schedulesSearch[$date])){
        $schedulesSearch[$date] = (object)[
          'shift_color_ft' => '333333',
          'shift_color_bg' => 'ffffff'
        ];
    }
    $calendar[$index][$week] = [
      'text'  => $day + $offset,
      'date'  => date('jS F, Y', $time),
      'color_ft' => $schedulesSearch[$date]->shift_color_ft,
      'color_bg' => $schedulesSearch[$date]->shift_color_bg
    ];
  endif;
endfor;

$data = [
  'calendar'        => $calendar,
  'prev_year_month' => $prev_year_month,
  'next_year_month' => $next_year_month,
  'year_month_text' => $year_month_text
];

return [
  'data'   => $data,
  'events' => $events
];
