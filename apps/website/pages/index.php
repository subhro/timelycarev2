@extends('shared.layout')
@include('shared.events')

@section('styles')
<style>
#content,
.page-content {
  padding: 0;
}
.home-body .main-wrapper{
  min-height: 631px;
  height: auto;
}
.home-body .menu .item {
  font-family: Constania !important;
  font-size: 16px !important;
  color: #000000 !important;
}
.home-body .menu .item:hover,
.home-body .menu .item.active {
  color: #000000 !important;
}
.home-body .menu .item .inverted {
  font-family: Constania !important;
  font-size: 18px !important;
}
.home-body .ui.text.container {
  margin-top: 170px;
}
.home-body .ui.text.container .header {
  font-family: Constania;
  font-size: 66px;
}
.home-body .ui.text.container .subheader {
  font-family: Constania;
  font-size: 30px;
  font-weight: normal;
}
.home-body .ui.text.container .button {
  font-family: Constania;
  font-size: 28px;
}
.ui.secondary.inverted.pointing.menu .item:hover,
.ui.secondary.inverted.pointing.menu .active.item {
  color: #000000 !important;
}
</style>
@endsection

@section('content')

<div class="home-body">
  <div class="ui inverted vertical masthead center aligned segment main-wrapper">

    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="active item">Home</a>
        <a class="item">Doctor</a>
        <a class="item">Nurse</a>
        <a class="item">About Us</a>
        <a class="item">Contact Us</a>
        <div class="right item">
          <a class="ui inverted button">Log in</a>&nbsp;
          <a class="ui inverted button">Sign Up</a>
        </div>
      </div>
    </div>

    <div class="ui text container">
      <h1 class="ui inverted header">
        Timely Care
      </h1>
      <h2 class="subheader">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum erat. Proin eu rhoncus libero. Integer eget hendrerit mi.
      </h2><br>
      <button class="ui huge blue button">Get Started <i class="fa fa-arrow-right"></i></button>
    </div>
  </div>
</div>

@endsection





@section('scripts')
@parent

<script type="text/javascript">

Event.on('page.init', function(){

});

</script>

@endsection
