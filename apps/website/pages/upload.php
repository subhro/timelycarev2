@extends('shared.layout')
@include('shared.events')


@include('widgets.upload')



@section('content')

<div class="home-body">
<upload id="file" multiple> Browse </upload>

<br><br>
<button on-click="upload"> Upload </button>
</div>

@endsection





@section('scripts')
@parent

<script type="text/javascript">

Event.on('page.init', function(){

});

Event.on('upload', function(){
  Api.upload('/api/upload').send();
});

</script>

@endsection
