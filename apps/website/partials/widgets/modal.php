@section('markups')
@parent

<script id="modal.tpl" type="text/template">

  <div class="modal-container">
    <div class="ui modal scrolling {{size}} transition {{$.modalClass}}" class-active="{{$.active}}">
        {{yield}}
    </div>
  </div>

  <div class="ui dimmer transition {{$.overlayClass}}" on-click="modal.close"></div>


</script>

@endsection





@section('scripts')
@parent

<script type="text/javascript">


Tag('modal', '#modal.tpl');

Tag.on('tag.init', function(){


});

Tag.on('modal.open', function(e){
    this.set('$.active', true);
    this.fire('modal.toggle');
});

Tag.on('modal.close', function(e){
    this.set('$.active', false);
    this.fire('modal.toggle');
});

Tag.on('modal.toggle', function(e){

  var self = this;
  var active = this.get('$.active');

  if(active){
    this.set('$.modalClass', 'visible animating in scale');
    this.set('$.overlayClass', 'visible animating in fade');
  }else{
    this.set('$.modalClass', 'visible animating out scale');
    this.set('$.overlayClass', 'visible animating out fade');
  }


  clearTimeout(self._timer1);
  self._timer1 = setTimeout(function(){
      self.set('$.modalClass', active?'visible':'hidden');
      self.set('$.overlayClass', active?'active':'hidden');
  },300);

});




Event.on('modal.open', function(e){
    var target = e.node.attrs('target');
    Tag.get(target).fire('modal.open');
});

Event.on('modal.close', function(e){
  var target = e.node.attrs('target');
  Tag.get(target).fire('modal.close');
});


</script>

@endsection
