
@section('body')
@parent
<iframe id="iframe-file-upload" name="iframe-file-upload" height="0" width="0" frameborder="0"></iframe>
@endsection

@section('segments')
@parent

<form id="form-file-upload" method="post" enctype="multipart/form-data" target="iframe-file-upload">
  {{#each $.file_inputs}}
  <input id="file-{{id}}" name="{{id}}" on-change="{{select}}" type="file">
  {{/each}}
</form>

@endsection


@section('markups')
@parent

<script id="upload.tpl" type="text/template">
  <button class="{{class}}" on-click="upload.click">{{yield}}</button>
</script>

@endsection



@section('scripts')
@parent

<script type="text/javascript">

Tag('upload', '#upload.tpl');

Tag.on('tag.init', function(){

  var file_inputs = Data.get('$.file_inputs');
  var last_file_input = Data.get('$.last_file_input');
  if(!file_inputs)
  file_inputs = [];

  if(this.get('id') == last_file_input)
  return;

  file_inputs.push({
    id:this.get('id'),
    params: this.get('params'),
    select:this.component.template.m[0].f
  });

  Data.set('$.file_inputs', file_inputs);
  Data.set('$.last_file_input', this.get('id'));
});

Tag.on('upload.click', function(e){
  document.getElementById('file-'+this.get('id')).click();
});

</script>

@endsection





@section('scripts')
@parent
<script type="text/javascript">

Event.on('page.init', function(){

  document.getElementById('iframe-file-upload').addEventListener('load', function(){
    Api.$loading = false;
    document.body.className = '';
    try{
      Api.$response = JSON.parse(this.contentWindow.document.body.innerText);
      Event.fire('api.success', Api.$response);
      Api.$request.success(Api.$response);
    }catch(e){
      Api.$response = { error: true };
      Event.fire('api.error', Api.$response);
    }
  });

});



</script>
@endsection
