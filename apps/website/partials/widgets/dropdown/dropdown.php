@section('markups')
@parent

<script id="dropdown.tpl" type="text/template">
  <div class="ui selection fluid dropdown {{class}} {{$.tagClass}}" on-keyup="tag.type" tabindex="0">
    <i class="dropdown icon {{$.icon}}"></i>
    <div class="text" on-click="tag.show">{{$.value}}</div>
    <div class="menu transition {{$.menuClass}}">
      {{#each options:index}}
      <div class="item key{{index}}" class-selected="{{$.selected == index}}" on-click="tag.click" index={{index}}>
        {{label}}
      </div>
      {{/each}}
    </div>
  </div>
</script>

@endsection


@section('scripts')
@parent

<script type="text/javascript">

Tag('dropdown', '#dropdown.tpl');


Tag.on('tag.init', function(){
  Event.fire('dropdown.setup', this);
  Event.on('body.clicked', this.trigger('tag.close'));
});


Tag.observe('value', function(){
  this.fire('tag.change');
});

Tag.on('tag.change', function(){

  var value, options, selected;

  value = this.get('value');
  options = this.get('options');

  if(!options || !options.length)
  return;

  selected = options.filter(function(option){
    return option.value==value?true:false
  });

  if(!selected.length)
  return;

  this.set('$.value', selected[0].label);
});



Tag.on('tag.show', function(e){
  Event.fire('dropdown.toggle', this);
});


Tag.on('tag.close', function(){
  Event.fire('dropdown.hide', this);
});

Tag.on('tag.type', function(e){

  if(e.original.key=='Enter')
  return this.fire('tag.enter');
  else if(e.original.key=='ArrowUp')
  return this.fire('tag.arrow-change', 1);
  else if(e.original.key=='ArrowDown')
  return this.fire('tag.arrow-change', -1);

});

Tag.on('tag.arrow-change', function(direction){
  Event.fire('dropdown.arrow-change', this, direction);
});


Tag.on('tag.click', function(e){

  e.original.stopPropagation();
  var index = e.node.attrs('index');
  var value = e.get();
  this.fire('tag.select', value, index);
});


Tag.on('tag.enter', function(e){

  var options = this.get('options');
  var index = this.get('$.selected');
  var value = options[index];

  this.fire('tag.select', value, index);


});


Tag.on('tag.select', function(value, index){
  this.set('value', value.value);
  this.set('$.selected', index);
  this.fire('select', this, value);
  Event.fire('dropdown.hide', this);

});



</script>


@endsection








@section('scripts')
@parent

<script type="text/javascript">


Event.on('dropdown.setup', function(self){

  self.set('$.selected', -1);
  self.set('$.active', false);
  self.set('$.icon', 'fa-caret-down');


  self.set('$.options',[]);
  if(self.get('$.value') == 'empty')
  self.set('$.value', '');
  else if(!self.get('$.value'))
  self.set('$.value', 'Select ...');


});



Event.on('dropdown.show', function(self){
  var active = self.get('$.active');
  if(!active)
  Event.fire('dropdown.animate', self);
});

Event.on('dropdown.hide', function(self){
  var active = self.get('$.active');
  if(active)
  Event.fire('dropdown.animate', self);
});

Event.on('dropdown.toggle', function(self){
  Event.fire('dropdown.animate', self);
});

Event.on('dropdown.animate', function(self){

  var active = self.get('$.active');

  active = !active;


  if(active){
    self.set('$.tagClass', 'visible active');
    self.set('$.menuClass', 'visible animating in slide down');
    self.set('$.icon', 'fa-times-circle');
  }else{
    self.set('$.tagClass', '');
    self.set('$.menuClass', 'visible animating out slide down');
    self.set('$.icon', 'fa-caret-down');
  }

  clearTimeout(self._timer2);
  self._timer2 = setTimeout(function(){
    self.set('$.active', active);
    self.set('$.menuClass', active?'visible':'hidden');
  },300);

});




Event.on('dropdown.arrow-change', function(self, direction){

  var options = self.get('options');
  var selected = self.get('$.selected');

  if(!options.length)
  return;

  selected -= direction;
  
  if(selected<0)
  selected = options.length - 1;
  if(selected == options.length)
  selected = 0;

  self.set('$.selected', selected);
  self.find('.menu .key'+selected).scrollIntoViewIfNeeded();

});


</script>
@endsection
