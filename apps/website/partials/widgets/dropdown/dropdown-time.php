@section('markups')
@parent


<script id="dropdown-time.tpl" type="text/template">

    <div class="ui selection fluid dropdown {{$.tagClass}}" on-keyup="tag.type" tabindex="0">
        <i class="dropdown icon {{$.icon}}"></i>
        <div class="text" on-click="tag.show">
            {{$.value[0]}}:{{$.value[1]}}:{{$.value[2]}}
        </div>
        <div class="menu transition {{$.menuClass}}">
            <div class="space-between">
                <div class="space-between-item">
                    <button class="circular ui icon button" on-click="tag.update" params="Hours,1">
                        <i class="icon fa-caret-up"></i>
                    </button>
                    <div class="label">
                        {{$.value[0]}}
                    </div>
                    <button class="circular ui icon button" on-click="tag.update" params="Hours,-1">
                        <i class="icon fa-caret-down"></i>
                    </button>
                </div>
                <div class="space-between-item">
                    <button class="circular ui icon button" on-click="tag.update" params="Minutes,1">
                        <i class="icon fa-caret-up"></i>
                    </button>
                    <div class="label">
                        {{$.value[1]}}
                    </div>
                    <button class="circular ui icon button" on-click="tag.update" params="Minutes,-1">
                        <i class="icon fa-caret-down"></i>
                    </button>
                </div>
                <div class="space-between-item">
                    <button class="circular ui icon button" on-click="tag.update" params="Hours,12">
                        <i class="icon fa-caret-up"></i>
                    </button>
                    <div class="label">
                        {{$.value[2]}}
                    </div>
                    <button class="circular ui icon button" on-click="tag.update" params="Hours,-12">
                        <i class="icon fa-caret-down"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>


</script>
@endsection



@section('scripts')
@parent

<script type="text/javascript">

Tag('dropdown-time', '#dropdown-time.tpl');

Tag.on('tag.init', function(){

    var value = this.get('value');
    this.date = new Date();

    if(value){
        value = value.split(':');
        this.date.setHours(value[0]);
        this.date.setMinutes(value[1]);
    }

    this.set('$.value', this.date.format('hh-mm-A').split('-'));

    Event.fire('dropdown.setup', this);
    Event.on('body.clicked', this.trigger('tag.close'));
    

});

Tag.observe('value', function(value){
});

Tag.on('tag.show', function(e){
    Event.fire('dropdown.toggle', this);
});


Tag.on('tag.close', function(){
    Event.fire('dropdown.hide', this);
});

Tag.on('tag.update', function(e){

    e.original.stopPropagation();

    var params = e.node.attrs('params').split(',');
    var action = params[0];
    var value = parseInt(params[1]);

    this.date['set'+ action](this.date['get'+ action]() + value);

    this.set('value', this.date.format('HH:mm:00'));
    this.set('$.value', this.date.format('hh-mm-A').split('-'));

    this.fire('selected');

});


</script>

@endsection
