@section('markups')
@parent


<script id="dropdown-date.tpl" type="text/template">
    <div class="ui selection fluid dropdown {{$.tagClass}}" on-keyup="tag.type" tabindex="0" style="min-width:14rem;">
        <i class="dropdown icon {{$.icon}}"></i>
        <div class="text" on-click="tag.show">
            {{$.value[0]}}-{{$.value[1]}}-{{$.value[2]}}
        </div>

        <div class="menu calendar transition {{$.menuClass}}">
            <div class="month-header">
                <button class="btn" on-click="tag.getPrevMonth"><i class="fa fa-chevron-left"></i></button>
                <div class="month">{{month}}</div>
                <button class="btn" on-click="tag.getNextMonth"><i class="fa fa-chevron-right"></i></button>
            </div>
            <div class="calendar-container">
                <div class="day-header">
                    {{#each days:index}}
                    <div class="calendar-date">{{days[index]}}</div>
                    {{/each}}
                </div>
                <div class="day-body">
                    {{#each dates:index}}
                    <div class="calendar-date">
                        <button on-click="date.select" class="date-item" class-disable="{{othermonths[index]==true}}">
                            {{dates[index]}}
                        </button>
                    </div>
                    {{/each}}
                </div>
            </div>
        </div>
    </div>


</script>
@endsection



@section('scripts')
@parent
<script type="text/javascript">

Tag('dropdown-date', '#dropdown-date.tpl');

Tag.on('tag.init', function(){

    this.set('$.icon', 'fa fa-caret-down');


    var value = this.get('value');
    if(!value){
        let defaultValue = 'dd-mm-yyyy'.split('-')
        this.set('$.value',defaultValue);
    }

    else{
        this.date = new Date(value);
        this.set('$.value', this.date.format('DD-MMM-YYYY').split('-'));
    }

     this.date = new Date();
    // let currentdate = this.date.getDate();
    // this.set('currentDate',currentdate);


    let month = this.date.format("MMMM YYYY");
    this.set('month',month);

    let days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    this.set('days',days);

    renderDays(this.date);
    // Event.on('body.clicked', this.trigger('tag.close'));


});

Tag.observe('value', function(value){
    var value = this.get('value');
    if(!value){
        value = 'dd-mm-yyyy'.split('-')
        this.set('$.value', value);
    }else{
        this.date = new Date(value);
        this.set('$.value', this.date.format('DD-MMM-YYYY').split('-'));
    }
});

Tag.on('tag.render', function(){
});

Tag.on('tag.show', function(e){
    var date = this.get('value');
    Event.fire('dropdown.toggle', this);
});

Tag.on('date.reset', function(){
    let defaultValue = 'dd-mm-yyyy'.split('-')
    Data.set('$.value',defaultValue);
});


Tag.on('tag.close', function(){
      Event.fire('dropdown.hide', this);
});

Tag.on('date.select',function(self){
    let date = self.get();
    let selectedDate = new Date(this.date.getFullYear(),this.date.getMonth(), date);
    this.set('$.value', selectedDate.format('DD-MMM-YYYY').split('-'));
    this.set('value', selectedDate.format('YYYY-MM-DD'));
    this.fire('select', this);
    this.set('$.cancelIcon', 'fa fa-trash');
    Event.fire('dropdown.hide', this);

});

Tag.on('tag.getPrevMonth', function(){
    let prevMonth= this.date['setMonth'](this.date['getMonth']()+ -1);
    prevMonth = this.date.format("MMMM YYYY");
    this.set('month',prevMonth);
    renderDays(this.date);
});

Tag.on('tag.getNextMonth', function(){
    let nextMonth= this.date['setMonth'](this.date['getMonth']()+ 1);
    nextMonth = this.date.format("MMMM YYYY");
    this.set('month',nextMonth);
    renderDays(this.date);
});

function renderDays(date) {

    let dates = [];
    let year = date.getFullYear();
    let months = date.getMonth()+1;

    let firstofmonth = new Date(year+"-"+months+"-"+"01");
    let firstWeekDay = firstofmonth.getDay() -1;
    var d= new Date(date.getFullYear(),date.getMonth(), 0);
    var prevmonthdays = d.getDate();

    var e= new Date(date.getFullYear(), date.getMonth()+1, 0);
    var currmonthdays = e.getDate();

    for(let i = 0; i<42; i++){
        dates.push(i-firstWeekDay);
    }

    let newDates = [];
    let otherMonths = [];

    dates.forEach(function(item){
        if(item<1){
            otherMonths.push(dates.indexOf(item));
            item = item + prevmonthdays

        } else if(item > currmonthdays) {
            otherMonths.push(dates.indexOf(item));
            item = item - currmonthdays
        }
        newDates.push(item);
    });

    let others = {};

    otherMonths.forEach(function(element,index){
        others[element] = true;
    });

    Data.set('othermonths',others);

    Data.set('dates',newDates);

}

</script>

@endsection
