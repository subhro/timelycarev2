
@section('markups')
@parent

<script id="dropdown-multiple.tpl" type="text/template">

  <div class="ui selection fluid dropdown {{$.tagClass}}" on-keyup="tag.type" tabindex="0">
    <i class="dropdown icon {{$.icon}}"></i>
    <div class="text" on-click="tag.show">{{$.value}}</div>
    <div class="menu transition {{$.menuClass}}">
      {{#each options:index}}
      <div class="item key{{index}}" class-selected="{{$.selected == index}}" on-click="tag.click" index={{index}}>
        {{label}} {{#if $.options[index]}} <i class="right icon fa-check"></i>{{/if}}
      </div>
      {{/each}}
    </div>
  </div>


</script>

@endsection




@section('scripts')
@parent


<script type="text/javascript">


Tag('dropdown-multiple', '#dropdown-multiple.tpl');

Tag.on('tag.init', function(){

  Event.fire('dropdown.setup', this);
  Event.on('body.clicked', this.trigger('tag.close'));
});

Tag.on('tag.show', function(e){
  Event.fire('dropdown.toggle', this);
});


Tag.on('tag.close', function(){
  Event.fire('dropdown.hide', this);
});

Tag.on('tag.type', function(e){

  if(e.original.key=='Enter')
  return this.fire('tag.enter');
  else if(e.original.key=='ArrowUp')
  return this.fire('tag.arrow-change', 1);
  else if(e.original.key=='ArrowDown')
  return this.fire('tag.arrow-change', -1);

});

Tag.on('tag.arrow-change', function(direction){
  Event.fire('dropdown.arrow-change', this, direction);
});

Tag.on('tag.click', function(e){

  e.original.stopPropagation();

  var index = e.node.attrs('index');
  var selected = e.get();

  this.fire('tag.select', selected, index);

});


Tag.on('tag.enter', function(e){

  var options = this.get('options');
  var index = this.get('$.selected');
  var selected = options[index];

  this.fire('tag.select', selected, index);


});



Tag.on('tag.select', function(selected, index){

  this.toggle('$.options.'+index);

  var options = this.get('options');
  var $options = this.get('$.options');
  //
  selected = options.filter(function(item, index){
    return $options[index];
  });

  var value = "Select ...";
  if(selected.length > 1)
  value = selected[0].text
  + " +" +(selected.length -1) + " more";
  else if(selected.length)
  value = selected[0].text;

  this.set('$.value', value);
  this.set('value', selected);

  this.fire('selected');

});

</script>


@endsection
