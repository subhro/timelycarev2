@section('markups')
@parent

<script id="message.tpl" type="text/template">
  <div class="ui {{type}} message transition {{$.messageClass}}">
    <i class="close icon fa-times" on-click="tag.close" index={{index}}></i>
    <div class="header">
      Alert !
    </div>
    <div class="list">
      {{text}}
    </div>
  </div>
</script>

@endsection




@section('scripts')
@parent

<script type="text/javascript">

Data.set('messages', []);

Tag('message', '#message.tpl');

Tag.on('tag.init', function(){
    var self = this;
    self.set('$.messageClass', 'visible animating in fly left');

    clearTimeout(self._timer1);
    self._timer1 = setTimeout(function(){
        self.set('$.messageClass', 'visible animating out fly left');
    },8000);

    clearTimeout(self._timer2);
    self._timer2 = setTimeout(function(){
        Data.shift('messages');
    },8400);

});

Tag.on('tag.close', function(e){
    var self = this;
    var index = e.node.attrs('index');

    clearTimeout(self._timer1);
    self.set('$.messageClass', 'visible animating out fly left');
    clearTimeout(self._timer2);
    self._timer2 = setTimeout(function(){
        Data.splice('messages', index, 1);
    },400);

});

</script>

@endsection
