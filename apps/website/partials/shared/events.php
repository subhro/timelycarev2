@section('scripts')
@parent
<script type="text/javascript">



Event.on('message.show', function(message){
  Data.push('messages', message);
});


Event.on('page.next', function(){
  Event.fire('page.paginate', 1);
});
Event.on('page.prev', function(){
  Event.fire('page.paginate', -1);
});
Event.on('page.refresh', function(){
  Data.set('pagination.page', 1);
  Event.fire('page.paginate', 0);
});
Event.on('page.redirect', function(url){
  window.location = url;
});

Event.on('delay.redirect', function(url){
  setTimeout(function(){
    window.location = url;
  },1000);
});




Event.on('page.paginate', function(direction){


  var url = Data.get('_url');

  var filter = Data.get('filter');
  var search = Data.get('search');

  if(!filter)
  filter = {};
  if(!search)
  search = {};

  var params = {};
  for (var key in filter) { params[key] = filter[key]; }
  for (var key in search) { params[key] = search[key]; }

  params.page = Data.get('pagination.page');
  params.page = parseInt(params.page) + direction;


  if(!params.page<1)
  Data.set('pagination.page', params.page);

  document.body.className = '_requesting';

  setTimeout(function(){
    Api.get(url).params(params).send();
  },1000);


});


Event.on('page.sort', function(e){

  var column = e.node.attrs('column');
  console.log(column);
  var order_by = Data.get('search.order_by');
  var order_direction = Data.get('search.order_direction');

  if(order_by != column)
  order_direction = 'none';

  order_by = column;

  if(order_direction == 'asc'){
    order_direction = 'desc';
    Data.set('_sort_class', 'fa-chevron-up');
  }else{
    order_direction = 'asc';
    Data.set('_sort_class', 'fa-chevron-down');
  }

  Data.set('pagination.page', 1);
  Data.set('search.order_by', order_by);
  Data.set('search.order_direction', order_direction);


  Event.fire('page.paginate', 0);

});


Event.on('api.init', function($request){
  Data.set('loading', true);
});


Event.on('api.success', function($response){

  if(!$response)
  return;

  if(!$response.data)
  $response.data = [];


  if($response.data.options)
  Data.set('options', $response.data.options);


  for(var key in $response.data){
    Data.set(key, $response.data[key]);
  }

  if(!$response.events)
  $response.events = [];

  for(var key in $response.events){
    Event.fire(key, $response.events[key]);
  }

  Data.set('loading', false);

});


Event.on('api.error', function(){
  Data.set('loading', false);
});





</script>
@endsection
