<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <link rel="icon" href="/favicon.png" type="image/x-icon"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

  <title>Timely Care</title>

  <link rel="stylesheet" href="/assets/css/framework.css">
  <link rel="stylesheet" href="/assets/css/app.css">

  @yield('styles')

  <script src="/assets/js/framework.js" charset="utf-8"></script>
  <script src="/assets/js/app.js" charset="utf-8"></script>

</head>

<body class="_loading">
  <input id="sidebar-toggle" type="checkbox" />
  <div id="app"></div>
  @yield('body')
  <div class="_spinner">
    <div class="ui active inverted dimmer">
      <div class="ui text loader">Loading</div>
    </div>
  </div>
  <script id="app.tpl" type="text/template">
    @yield('header')
    @yield('sidebar')
    <div id="content">
      <label for="sidebar-toggle" id="sidebar-overlay"></label>
      <div class="page-content">
        @yield('content')
      </div>
    </div>
    <div id="messages">
      {{#each messages:index}}
      <message type="{{type}}" text="{{text}}" index="{{index}}"></message>
      {{/each}}
    </div>
    @yield('segments')
  </script>

  <script type="text/javascript">
  @if(isset($data) && $data)
    @foreach($data as $key => $value)
      Data.set('<%$key%>', <%json_encode($value)%>);
    @endforeach
  @endif
  </script>

  @yield('markups')
  @yield('scripts')


</body>
</html>
