<?php
$uid = 9;
$limit = 20;

$chats = Chat::where('type', 0)
->where('sender_id', $uid)
->orderBy('timestamp')
->limit($limit)
->get();
$chatRecords = [];

foreach($chats as $chat){
  if(isset($chatRecords['0-'.$chat->receiver_id])){
    continue;
  }
  $chatRecords['0-'.$chat->receiver_id] = $chat->timestamp;
}

$chats = Chat::where('type', 0)
->where('receiver_id', $uid)
->orderBy('timestamp')
->limit($limit)
->get();

  foreach($chats as $chat){
    if(isset($chatRecords['0-'.$chat->sender_id])){
      continue;
    }
    $chatRecords['0-'.$chat->sender_id] = $chat->timestamp;
  }


$grpupIds = Group::where('member_ids', 'like', "%$uid%")
->pluck('id');

$chats = Chat::where('type', 2)
->whereIn('receiver_id', $grpupIds)
->orderBy('timestamp')
->limit($limit)
->get();

foreach($chats as $chat){
  if(isset($chatRecords['2-'.$chat->receiver_id])){
    continue;
  }
  $chatRecords['2-'.$chat->receiver_id] = $chat->timestamp;
}
arsort($chatRecords);
$doctors = [];
foreach($chatRecords as $key => $chatRecord){
  $keys = explode('-',$key);
  $type = $keys[0];
  $userId = $keys[1];
  if($type == '0'){
    $doctor = User::find($userId);
    $doctor->group = 0;
  } else {
    $doctor = Group::find($userId);
    $doctor->group = 1;
  }
  $doctors[] = $doctor;
}
if(!count($doctors)){
  $message = 'No doctors available';
  if(Input::has('q')){
    $message = 'No doctors found';
  }
  $response = [
    'type' => 'error',
    'message' => $message,
    'pagination' => $pagination
  ];
  goto RESPONSE;
}
$response = [
  'type' => 'success',
  'doctors' => $doctors,
  'pagination' => $pagination
];

RESPONSE:
return $response;
