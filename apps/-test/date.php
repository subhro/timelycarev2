<?php
 $format = "Y-m-d";
 $startDate = '2018-05-10';
 $endDate   = '2018-05-17'.'23:59:59';

$period = new DatePeriod(new DateTime($startDate),new DateInterval('P1D'),new DateTime($endDate));
foreach ($period as $key => $value) {
    echo $value->format('Y-m-d');
}
?>
